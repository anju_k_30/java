//4.Given an array arr[] of size N,check if is sorted in non-descending order or not(ascending)

import java.util.*;
class Sorting{
	boolean Sort(int arr[]){

		for(int i=1;i<arr.length;i++){
			if(arr[i]<arr[i-1])
				return false;
		}
		return true;
	}

}
class Client{
	public static void main(String s[]){

	Scanner sc=new Scanner(System.in);
	System.out.println("Enter Array Size");

	int size=sc.nextInt();

	System.out.println("Enter array Elements");
	int arr[]=new int[size];

	for(int i=0;i<arr.length;i++)
		arr[i]=sc.nextInt();

	Sorting obj=new Sorting();
	if(obj.Sort(arr))
		System.out.println("Array is sorted");
	else
		System.out.println("Array is not sorted");
	}
	
}

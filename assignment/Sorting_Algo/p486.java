//1.Given an Integer N and a list arr. SortSot the array using bubble sort algorithm .

import java.util.*;
class Sorting{
	void BubbleSort(int size,int arr[]){

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length-(i+1);j++){
				if(arr[j]>arr[j+1]){
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
	}

}

class Client{
	public static void main(String s[]){

	Scanner sc=new Scanner(System.in);
	System.out.println("Enter Array Size");

	int size=sc.nextInt();

	System.out.println("Enter array Elements");
	int arr[]=new int[size];

	for(int i=0;i<arr.length;i++)
		arr[i]=sc.nextInt();

	Sorting obj=new Sorting();
	obj.BubbleSort(size,arr);

	for(int i=0;i<arr.length;i++)
		System.out.print(arr[i]+" ");

	System.out.println();

	}
}

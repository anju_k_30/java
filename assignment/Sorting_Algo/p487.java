//2.Given an array of size  N containing only 0s,1s and 2s;sort the array in ascending order.

import java.util.*;
class Sorting{
	void Sort(int arr[]){

		int low=0;
		int curr=0;
		int high=arr.length-1;

		while(curr<=high){

			if(arr[curr]==0){
				int temp=arr[low];
				arr[low]=arr[curr];
				arr[curr]=temp;

				curr++;
				low++;
			}
			else if(arr[curr]==1)
				curr++;
			else{
				int temp=arr[curr];
				arr[curr]=arr[high];
				arr[high]=temp;

				high--;
			}

		}
		
	}

}

class Client{
	public static void main(String s[]){

	Scanner sc=new Scanner(System.in);
	System.out.println("Enter Array Size");

	int size=sc.nextInt();

	System.out.println("Enter array Elements like =>0,1 and 2");
	int arr[]=new int[size];

	for(int i=0;i<arr.length;i++)
		arr[i]=sc.nextInt();

	Sorting obj=new Sorting();
	obj.Sort(arr);

	for(int num:arr)
		System.out.print(num+" ");

	System.out.println();

	}
}

//5.Given an array arr[],its starting position l and its ending position r.Also ,sort array using Merge Sort algorithm.

import java.util.*;
class Sorting{
	void MergeSort(int arr[],int start,int end){
		if(start<end){
			int mid=(start+end)/2;
			MergeSort(arr,start,mid);
			MergeSort(arr,mid+1,end);
			merge(arr,start,mid,end);
		}
	}

	void merge(int arr[],int start,int mid,int end){

		int n1=mid-start+1;
		int n2=end-mid;

		int arr1[]=new int[n1];
		int arr2[]=new int[n2];

		for(int i=0;i<n1;i++){
			arr1[i]=arr[start+i];
		}

		for(int i=0;i<n2;i++){
			arr2[i]=arr[mid+1+i];
		}

		int i=0,j=0,k=start;

		while(i<arr1.length && j<arr2.length){
			if(arr1[i]<arr2[j]){
				arr[k]=arr1[i];
				i++;
			}else{
				arr[k]=arr2[j];
				j++;
			}
			k++;
		}

		while(i<arr1.length){
			arr[k]=arr1[i];
			i++;
			k++;
		}

		while(j<arr2.length){
			arr[k]=arr2[j];
			j++;
			k++;
		}
	}



}

class Client{
	public static void main(String s[]){

	Scanner sc=new Scanner(System.in);
	System.out.println("Enter Array Size");

	int size=sc.nextInt();

	System.out.println("Enter array Elements");
	int arr[]=new int[size];

	for(int i=0;i<arr.length;i++)
		arr[i]=sc.nextInt();

	Sorting obj=new Sorting();
	obj.MergeSort(arr,0,arr.length-1);

	for(int num:arr)
		System.out.print(num+" ");

	System.out.println();
	}
}

//3.Given two sorted array arr1[] and arr2[] of sizes n and m in non-descending order.Merge them in sorted order without using any extra space .Modify arr1 so that it containg the first N elements and modify arr2 so that it contains the last M elements

import java.io.*;
import java.util.Arrays;

class Sorting{
	void Sort(int arr1[],int arr2[],int n,int m){
		
		int right=n-1;
		int left=0;

		while(right >= 0 && left < m){
			
			if(arr1[right]>arr2[left]){
			
				int temp=arr1[right];
				arr1[right]=arr2[left];
				arr2[left]=temp;
			
				right--;
				left++;
			}
			else
				break;
		}
		
		Arrays.sort(arr1);
		Arrays.sort(arr2);			
	}



}

class Client{
	public static void main(String s[])throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	
	System.out.println("Enter Array1 Size");
	int size1=Integer.parseInt(br.readLine());
	
	System.out.println("Enter sorted elements in arr1");
	int arr1[]=new int[size1];

	for(int i=0;i<arr1.length;i++)
		arr1[i]=Integer.parseInt(br.readLine());



	System.out.println("Enter Array2 Size");
	int size2=Integer.parseInt(br.readLine());
	
	System.out.println("Enter sorted elements in arr2");
	int arr2[]=new int[size2];

	for(int i=0;i<arr2.length;i++)
		arr2[i]=Integer.parseInt(br.readLine());

	
	
	
	Sorting obj=new Sorting();
	obj.Sort(arr1,arr2,size1,size2);

	System.out.print("arr1:[");
	for(int i=0;i<arr1.length;i++)
		System.out.print(arr1[i]+" ");
	
	System.out.print("]\n");

	System.out.print("arr2:[");
	for(int i=0;i<arr2.length;i++)
		System.out.print(arr2[i]+" ");
	System.out.print("]\n");

	}
}

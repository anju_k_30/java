//10.Given an array nums,return true if the array was originally sorted in non-descending order,then rotated some number of positions(including 0)Otherwise return false.


import java.io.*;
class Sorting{
	boolean sort(int arr[]){

			int sort=0,rotate=0;

			for(int i=1;i<arr.length;i++){
				if(arr[i-1]>arr[i] && arr[0]>=arr[arr.length-1])
					sort++;
			}

			if(sort==1 ||sort==0)
				return true;
			else 
				return false;
		}  
}

class Client{

	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter Array Elements");
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		Sorting obj=new Sorting();
		System.out.println(obj.sort(arr));
			
	
	
	}

		
}
/*
import java.io.*;

class Client {

	// Function to check if an array is
	// Sorted and rotated clockwise
	static boolean checkIfSortRotated(int arr[], int n)
	{
		// Initializing two variables x,y as zero.
		int x = 0, y = 0;

		// Traversing array 0 to last element.
		// n-1 is taken as we used i+1.
		for (int i = 0; i < n - 1; i++) {
			if (arr[i] < arr[i + 1])
				x++;
			else
				y++;
		}

		// If till now both x,y are greater
		// than 1 means array is not sorted.
		// If both any of x,y is zero means
		// array is not rotated.
		if (y == 1) {
			// Checking for last element with first.
			if (arr[n - 1] < arr[0])
				x++;
			else
				y++;

			// Checking for final result.
			if (y == 1)
				return true;
		}
		// If still not true then definitely false.
		return false;
	}

	// Driver code
	public static void main(String[] args)
	{
		int arr[] = { 3,4,5,6, 1, 2 };

		int n = arr.length;

		// Function Call
		boolean x = checkIfSortRotated(arr, n);
		if (x == true)
			System.out.println("YES");
		else
			System.out.println("NO");
	}
}
*/

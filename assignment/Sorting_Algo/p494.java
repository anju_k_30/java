//9.You are given a 0-indexed integer array nums and a target element target.A target index is an index i such that nums2[i]==target.Return a list of the target indices of nums after sorting nums in non-descending order.If there are no target indices,return an empty list.The Returned list must be sorted
// in increasing order.

import java.util.*;
class Sorting{
	List<Integer> Sort(int arr[],int target){

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length-(i+1);j++){
				if(arr[j]>arr[j+1]){
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
	
		List<Integer> list=new ArrayList<Integer>();

		for(int i=0;i<arr.length;i++){
			if(arr[i]==target){
				list.add(i);
			}
		}
	return list;
	}
	
}
class Client{
	public static void main(String s[]){

	Scanner sc=new Scanner(System.in);
	System.out.println("Enter Array Size");

	int size=sc.nextInt();

	System.out.println("Enter array Elements");
	int arr[]=new int[size];

	for(int i=0;i<arr.length;i++)
		arr[i]=sc.nextInt();
	
	System.out.println("Enter Target value");
	int target=sc.nextInt();

	Sorting obj=new Sorting();
	List list=obj.Sort(arr,target);

	System.out.println(list);
	
	
	}
}

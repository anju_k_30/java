//code 5:Wap to print the square of even digits of the given no. 
//ip=942111423
//op=4 16 4 16

class num{
	public static void main(String s[]){
		int n=942111423;
		while(n!=0){
			int no=n%10;
			if(no%2==0)
				System.out.println(no*no);
			n=n/10;
		}
		
	}
}

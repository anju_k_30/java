//code:9:WAP to reverse the given no.
//ip:942111423
//op:324111249

class num{
	public static void main(String []args){
		int n=942111423;
		int rem=0;
		int rev=0;

		while(n!=0){
			rem=n%10;
			rev=(rev*10)+rem;
			n=n/10;
		}
		System.out.println(rev);
	}
}


//code 4:Wap to count the odd digits of the given no. 
//ip=942111423
//op=5

class num{
	public static void main(String s[]){
		int count=0;
		int n=942111423;
		while(n!=0){
			if(n%2==1)
				count++;
			n=n/10;
		}
		System.out.println(count);
	}
}

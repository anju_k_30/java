//code:10:WAP to chech whether the no. is palindrome or not.
//ip:2332
//op:yes palindrome

class num{
	public static void main(String []args){
		int n=2332;
		int ori=n;
		int rem=0;
		int rev=0;

		while(n!=0){
			rem=n%10;
			rev=(rev*10)+rem;
			n=n/10;
		}
		if(ori==rev)
			System.out.println(ori+" is a palindome");
		else
			System.out.println(ori+" is not a palindome");
	}
}


//code1:
/*WAP to print the following pattern

D4 C3 B2 A1
A1 B2 C3 D4
D4 C3 B2 A1
A1 B2 C3 D4
*/

import java.io.*;
class Pattern{
	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter no. of Rows");
		int no=Integer.parseInt(br.readLine());

		
		for(int i=1;i<=no;i++){
			int a=no;
			char ch='A';

			for(int j=1;j<=no;j++){
				if(i%2==0){
					System.out.print(ch++);
					System.out.print(j+" ");

				}else{
					System.out.print(ch--);
					System.out.print(a--+" ");
				}
			}
			System.out.println(" ");
		}
	}
}



















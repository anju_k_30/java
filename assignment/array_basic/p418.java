//3.Large element in an array

import java.io.*;

class arrayd{
	public static void main(String str[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements");
		
		int large=arr[0];
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]>large)
				large=arr[i];
		}
		
		System.out.println("Large is "+large);


	}
}
		
	



//1.Given an integer array and another element. The Task is to find if the given element is present in the array or not

import java.io.*;
class arrayd{
	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Array Size");
		int size=Integer.parseInt(br.readLine());

		System.out.println("Array Elements");
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Array Element to be searched");
		int search=Integer.parseInt(br.readLine());
		
		for(int i=0;i<arr.length;i++){
			if(arr[i]==search)
				System.out.println("Element found at "+i);
		}
		

	}
}

//2.WAP to display the first 10 natural numbers in reverse order

class Demo{
	void no(int num){
		for(int i=num;i>=1;i--){
			System.out.print(i + " ");
		}
	}
	
	void num(int no){
		if(no==0)
			return ;
	
		System.out.print(no +" ");
		num(no-1);
	}

	public static void main(String s[]){
		Demo obj=new Demo();
		//obj.no(10);
		obj.num(10);
	}
}

//2.WAP to print the sum of n natural numbers

class Demo{
	int no(int num){
		int sum=0;
		for(int i=1;i<=num;i++){
			sum=sum+i;
		
		}
		return sum;
	}
	
	int num(int no){
		if(no==1)
			return 1;
		
		return no+num(no-1);
	}

	public static void main(String s[]){
		Demo obj=new Demo();
		System.out.println(obj.no(10));
		System.out.println(obj.num(10));
	}
}

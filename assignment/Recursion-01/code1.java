//1.WAP to print the nos. between 1 to 10

class Demo{
	void no(int num){
		for(int i=1;i<=num;i++){
			System.out.print(i + " ");
		}
	}
	
	void num(int no){
		if(no==0)
			return ;
		
		num(no-1);
		System.out.print(no +" ");
	}

	public static void main(String s[]){
		Demo obj=new Demo();
		//obj.no(10);
		obj.num(10);
	}
}

//code1:WAP to take size of array from user and take interger elements from user .Print sum of odd elements only.

import java.io.*;
class code1{
	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter Array Elements");

		int sum=0;

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2!=0)
				sum=sum+arr[i];
		}
		System.out.println("Sum of odd elements is "+sum);
	}
}



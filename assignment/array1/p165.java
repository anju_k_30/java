//code2:WAP to take size of array from user and take interger elements from user .Print product of even elements only.

import java.io.*;
class code2{
	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter Array Elements");

		int mul=1;

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0)
				mul=mul*arr[i];
		}
		System.out.println("Product of even elements is "+mul);
	}
}



//code5:WAP ,take input from user,Print only elements that are divisible by 5


import java.io.*;
class code5{
	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Output is");

		for(int i=0;i<arr.length;i++){
			if(arr[i]%5==0)
				System.out.println(arr[i]);
		}
	}
}




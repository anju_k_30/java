//code4:WAP ,take 7 character as a input,Print only vowel from the array

import java.io.*;
class code4{
	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		char arr[]=new char[size];
		System.out.println("Enter array elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=(char)(br.read());
			br.skip(1);
		}

		System.out.println("Vowels Are");

		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a')
				System.out.println(arr[i]+" ");

			else if(arr[i]=='e')
				System.out.println(arr[i]+" ");

			else if(arr[i]=='i')
				System.out.println(arr[i]+" ");

			else if(arr[i]=='o')
				System.out.println(arr[i]+" ");

			else if(arr[i]=='u')
				System.out.println(arr[i]+" ");
		}

	}
}




//code4:WAP in which according to month no. print the no. of days in that month

class num{
	public static void main(String []args){
		int x=2;
		if(x<0 || x>13)
			System.out.println("Invalid input");
		
		else if(x==1)
			System.out.println("Jan has 31 Days");
		
		else if(x==2)
			System.out.println("Feb has 28 Days");
		
		else if(x==3)
			System.out.println("March has 31 Days");
		
		else if(x==4)
			System.out.println("April has 30 Days");
		
		else if(x==5)
			System.out.println("May has 31 Days");
		
		else if(x==6)
			System.out.println("June has 30 Days");
		
		else if(x==7)
			System.out.println("July has 31 Days");
		
		else if(x==8)
			System.out.println("Aug has 30 Days");
		
		else if(x==9)
			System.out.println("Sep has 31 Days");
		
		else if(x==10)
			System.out.println("Oct  has 31 Days");
		
		else if(x==11)
			System.out.println("Nov has 30 Days");
		
		else
			System.out.println("Dec has 31 Days");

	}
}

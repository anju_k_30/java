//code8:WAP to find the uncommon elements between two arrays.

import java.io.*;
class code8{
	public static void main(String S[])throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter Array1 Size");
		int size1=Integer.parseInt(br.readLine());

		System.out.println("Enter Array1 Elements");
		int arr1[]=new int[size1];

		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter Array2 Size");
		int size2=Integer.parseInt(br.readLine());

		System.out.println("Enter Array2 Elements");
		int arr2[]=new int[size2];

		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		
		int flag=0;
		System.out.println("Uncommon Elements Are: ");

		for(int i=0;i<arr1.length;i++){
			flag=0;
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					flag=1;
					break;
				}
			}
			if(flag==0){
				System.out.println(arr1[i]);
				flag=1;
			}
		}
		for(int i=0;i<arr2.length;i++){
			flag=0;
			for(int j=0;j<arr1.length;j++){
				if(arr2[i]==arr1[j]){
					flag=1;
					break;
				}
			}
			if(flag==0){
				System.out.println(arr2[i]);
				flag=1;
			}
		}
	}
}

//code4:WAP to take search a specific element from an array and return its index.

import java.io.*;
class code4{
	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		
		System.out.println("Enter Array Elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter Array Element to be Searched");
		int search=Integer.parseInt(br.readLine());

		for(int i=0;i<arr.length;i++){
			if(arr[i]==search)
				System.out.println("Element found at index : "+i);
		}
			
				System.out.println("Element not found");
		
	}
}


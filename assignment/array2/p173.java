//code5:WAP to take size and elements from user of array.Find the min element from the array.

import java.io.*;
class code5{
	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Array Elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int min=arr[0];
		
		for(int i=0;i<arr.length;i++){
			if(min>arr[i])
				min=arr[i];
		}
		System.out.println("min is : "+min);
	

	}
}
		


//code 10:WAP to print the elements whose addition of digits is even 

import java.io.*;
class code10{
	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Elements Whose addition of Digits is");
	
		for(int i=0;i<arr.length;i++){
			int no=arr[i];
			int sum=0;

			while(no!=0){
				sum=sum+no%10;
				no=no/10;
			}
				if(sum%2==0)
					System.out.println(arr[i]);
			
		}
	}
}






//code1:P to create an array of n integer elements from user.Insert the value from users and find the sum of all elements in the array.

import java.io.*;
class code1{
	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		
		System.out.println("Enter array Elements");

		int sum=0;
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			sum=sum+arr[i];
		}
		System.out.println("sum is "+sum);

	}
}

//code3:WAP to find the sum of even and odd integers in a given array of integers

import java.io.*;
class code3{
	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size= Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter Array Elements");

		int sumeven=0,sumodd=0;

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0)
				sumeven=sumeven+arr[i];
			else
				sumodd=sumodd+arr[i];
		}
		System.out.println("Sum of Even= "+sumeven);
		System.out.println("Sum of Odd= "+sumodd);
	}
}





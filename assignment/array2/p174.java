//code6:WAP to take size and elements from user of array.Find the max element from the array.

import java.io.*;
class code6{
	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Array Elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int max=arr[0];
		
		for(int i=0;i<arr.length;i++){
			if(max<arr[i])
				max=arr[i];
		}
		System.out.println("max is : "+max);
	

	}
}
		


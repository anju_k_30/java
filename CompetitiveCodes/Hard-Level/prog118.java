/*41. First Missing Positive
Hard

Given an unsorted integer array nums. Return the smallest positive integer that is not present in nums.

You must implement an algorithm that runs in O(n) time and uses O(1) auxiliary space.

Example 1:

Input: nums = [1,2,0]
Output: 3
Explanation: The numbers in the range [1,2] are all in the array.
*/

import java.util.*;
class Solution {
    public int firstMissingPositive(int[] nums) {

        Arrays.sort(nums);
        int missingNo=1;

        for(int number:nums){
            if(number>0){
                if(missingNo==number){
                    missingNo++;
                }
                else if(number>missingNo){
                    break;
                }
            }
        }

        return missingNo;

    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{1,2,0};

		Solution obj=new Solution();
		System.out.println(obj.firstMissingPositive(arr));
	}
}

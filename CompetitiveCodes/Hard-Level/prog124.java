/*115. Distinct Subsequences
Hard

Given two strings s and t, return the number of distinct subsequences of s which equals t.

The test cases are generated so that the answer fits on a 32-bit signed integer.

 

Example 1:

Input: s = "rabbbit", t = "rabbit"
Output: 3
Explanation:
As shown below, there are 3 ways you can generate "rabbit" from s.
rabbbit
rabbbit
rabbbit

Example 2:

Input: s = "babgbag", t = "bag"
Output: 5
Explanation:
As shown below, there are 5 ways you can generate "bag" from s.
babgbag
babgbag
babgbag
babgbag
babgbag*/

import java.util.*;
class Solution {


      public int numDistinct(String s, String t) {

       Map<String, Integer> memo = new HashMap<>();
        return numDistinctHelper(s, t, 0, 0, memo);
    }

     private int numDistinctHelper(String s, String t, int sIndex, int tIndex, Map<String, Integer> memo) {
        // If reached the end of t, we found a valid subsequence
        if (tIndex == t.length()) {
            return 1;
        }
        // If reached the end of s but not t, no valid subsequence
        if (sIndex == s.length()) {
            return 0;
        }

        // Check if the result is already cached
        String key = sIndex + "," + tIndex;
        if (memo.containsKey(key)) {
            return memo.get(key);
        }

        int count = 0;
        // If the characters match, we can either include this character or skip it
        if (s.charAt(sIndex) == t.charAt(tIndex)) {
            count += numDistinctHelper(s, t, sIndex + 1, tIndex + 1, memo); // Include this character
        }
        // Skip this character in s
        count += numDistinctHelper(s, t, sIndex + 1, tIndex, memo);

        // Cache the result
        memo.put(key, count);
        return count;
    }
}

class User{
	
	 public static void main(String[] args) {
     
	      	Solution solution = new Solution();
        	System.out.println(solution.numDistinct("rabbbit", "rabbit")); // Output: 3
        	System.out.println(solution.numDistinct("babgbag", "bag")); // Output: 5
    }

}

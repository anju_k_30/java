/*632. Smallest Range Covering Elements from K Lists
Hard

You have k lists of sorted integers in non-decreasing order. Find the smallest range that includes at least one number from each of the k lists.

We define the range [a, b] is smaller than range [c, d] if b - a < d - c or a < c if b - a == d - c.

 

Example 1:

Input: nums = [[4,10,15,24,26],[0,9,12,20],[5,18,22,30]]
Output: [20,24]
Explanation: 
List 1: [4, 10, 15, 24,26], 24 is in range [20,24].
List 2: [0, 9, 12, 20], 20 is in range [20,24].
List 3: [5, 18, 22, 30], 22 is in range [20,24].

Example 2:

Input: nums = [[1,2,3],[1,2,3],[1,2,3]]
Output: [1,1]
*/

import java.util.*;

class Pair {
    int first;
    int second;

    public Pair(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }
}

class Solution {
    public int[] smallestRange(List<List<Integer>> nums) {
        int k = nums.size();
        List<Pair> list = new ArrayList<>();
        for(int i = 0; i < k; i++) {
            List<Integer> l = nums.get(i);
            for(int nu : l) {
                list.add(new Pair(nu, i));
            }
        }

        Collections.sort(list, (a, b) -> a.first - b.first);
        int[] ans = new int[2];
        ans[1] = ans[0] = Integer.MAX_VALUE;
        int i = 0, j = 0;
        List<Integer> store = new ArrayList<>();
        HashMap<Integer, Integer> mp = new HashMap<>();
        int totalType = 0;

        while(j < list.size()) {
            int nu = list.get(j).first;
            int type = list.get(j).second;
            store.add(nu);
            int val = mp.getOrDefault(type, 0);
            mp.put(type, val + 1);
            totalType++;
            if(mp.size() < k) {
                j++;
            } else if(mp.size() == k) {
                if(ans[0] == ans[1] && ans[1] == Integer.MAX_VALUE) {
                    ans[0] = store.get(0);
                    ans[1] = store.get(store.size() - 1);
                }
                int dif = store.get(store.size() - 1) - store.get(0);
                if(dif < ans[1] - ans[0]) {
                    ans[0] = store.get(0);
                    ans[1] = store.get(store.size() - 1);
                }

                while(mp.size() == k) {
                    dif = store.get(store.size() - 1) - store.get(0);
                    if(dif < ans[1] - ans[0]) {
                        ans[0] = store.get(0);
                        ans[1] = store.get(store.size() - 1);
                    }
                    store.remove(0);
                    int t = list.get(i).second;
                    int vv = (int) mp.get(t);
                    mp.put(t, vv - 1);
                    if(vv == 1) {
                        mp.remove(t);
                    }
                    i++;
                }

                j++;

            }

        }
        return ans;
    }
}


class User {
    public static void main(String[] args) {
        Solution solution = new Solution();

        List<List<Integer>> nums = new ArrayList<>();
        nums.add(Arrays.asList(4, 10, 15, 24, 26));
        nums.add(Arrays.asList(0, 9, 12, 20));
        nums.add(Arrays.asList(5, 18, 22, 30));

        int[] result = solution.smallestRange(nums);

        System.out.println("Expected Output: [20, 24]");
        System.out.println("Output from smallestRange: [" + result[0] + ", " + result[1] + "]");
    }
}
      



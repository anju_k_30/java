/*123. Best Time to Buy and Sell Stock III
Hard

You are given an array prices where prices[i] is the price of a given stock on the ith day.

Find the maximum profit you can achieve. You may complete at most two transactions.

Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock before you buy again).

 

Example 1:

Input: prices = [3,3,5,0,0,3,1,4]
Output: 6
Explanation: Buy on day 4 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
Then buy on day 7 (price = 1) and sell on day 8 (price = 4), profit = 4-1 = 3.

Example 2:

Input: prices = [1,2,3,4,5]
Output: 4
Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are engaging multiple transactions at the same time. You must sell before buying again.

Example 3:

Input: prices = [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.

 */

class Solution {
    public int maxProfit(int[] prices) {
        int buy1 = Integer.MAX_VALUE, buy2 = Integer.MAX_VALUE;
        int sell1 = 0, sell2 = 0;
        
        for (int price : prices) {
            // First transaction: buy at the lowest price
            buy1 = Math.min(buy1, price);
            // First transaction: sell with maximum profit
            sell1 = Math.max(sell1, price - buy1);
            // Second transaction: buy with remaining profit
            buy2 = Math.min(buy2, price - sell1);
            // Second transaction: sell with maximum profit
            sell2 = Math.max(sell2, price - buy2);
        }
        
        return sell2;
    }
}
class User{
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] prices = {3, 3, 5, 0, 0, 3, 1, 4};
        System.out.println(solution.maxProfit(prices)); // Output should be 6
    }
}

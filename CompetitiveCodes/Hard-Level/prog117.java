/*4. Median of Two Sorted Arrays
Hard

Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.

The overall run time complexity should be O(log (m+n)).

 

Example 1:

Input: nums1 = [1,3], nums2 = [2]
Output: 2.00000
Explanation: merged array = [1,2,3] and median is 2.

Example 2:

Input: nums1 = [1,2], nums2 = [3,4]
Output: 2.50000
Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.
*/

class Solution {
    public double findMedianSortedArrays(int[] num1, int[] num2) {


        int m=num1.length;
        int n=num2.length;
        int i=0,j=0,k=0;

        int a[]=new int[m+n];

        while(i<m && j< n){

            if(num1[i]<num2[j]){
                a[k]=num1[i];
                i++;
            }else{
                a[k]=num2[j];
                j++;

            }
            k++;

        }

        while(i<m){
            a[k]=num1[i];
            i++;
            k++;
        }

        while(j<n){
            a[k]=num2[j];
            j++;
            k++;
        }

        if((m+n)%2!=0){
            return a[(m+n)/2];
        }else{

            int x=(m+n)/2;
            double p=a[x];
            double q=a[x-1];
            return (p+q)/2;
        }
    }
}


class User{
	public static void main(String s[]){
		int arr1[]=new int[]{1,2};
		int arr2[]=new int[]{3,4};

		Solution obj=new Solution();
		System.out.println(obj.findMedianSortedArrays(arr1,arr2));
	}
}
	

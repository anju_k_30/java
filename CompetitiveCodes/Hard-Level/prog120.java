/*84. Largest Rectangle in Histogram
Hard

Given an array of integers heights representing the histogram's bar height where the width of each bar is 1, return the area of the largest rectangle in the histogram.

 

Example 1:

Input: heights = [2,1,5,6,2,3]
Output: 10
Explanation: The above is a histogram where width of each bar is 1.
The largest rectangle is shown in the red area, which has an area = 10 units.
*/

class Solution {
    public int largestRectangleArea(int[] h) {
        int n=h.length;

        if(n==0)
            return 0;

        int maxArea=0;

        int left[]=new int[n];
        int right[]=new int[n];

        left[0]=-1;
        right[n-1]=n;

        for(int i=1;i<n;i++){
            int prev=i-1;
            while(prev>=0 && h[prev]>=h[i]){
                prev=left[prev];
            }
            left[i]=prev;
        }

        for(int i=n-2;i>=0;i--){
            int prev=i+1;
            while(prev<n && h[prev]>=h[i]){
                prev=right[prev];
            }
            right[i]=prev;
        }

        for(int i=0;i<n;i++){
            int width=right[i]-left[i]-1;
            maxArea=Math.max(maxArea,h[i]*width);
        }

        return maxArea;

    }
}

class User{
 public static void main(String[] args) {
        Solution obj = new Solution();
        int[] heights = {2, 1, 5, 6, 2, 3};
        System.out.println("Largest Rectangle Area: " + obj.largestRectangleArea(heights));
    }

}

/*1169. Invalid Transactions

A transaction is possibly invalid if:

    the amount exceeds $1000, or;
    if it occurs within (and including) 60 minutes of another transaction with the same name in a different city.

You are given an array of strings transaction where transactions[i] consists of comma-separated values representing the name, time (in minutes), amount, and city of the transaction.

Return a list of transactions that are possibly invalid. You may return the answer in any order.

 

Example 1:

Input: transactions = ["alice,20,800,mtv","alice,50,100,beijing"]
Output: ["alice,20,800,mtv","alice,50,100,beijing"]
Explanation: The first transaction is invalid because the second transaction occurs within a difference of 60 minutes, have the same name and is in a different city. Similarly the second one is invalid too.*/

import java.util.*;

class Trans {
  public String name;
  public int time;
  public int amount;
  public String city;
  public Trans(String name, int time, int amount, String city) {
    this.name = name;
    this.time = time;
    this.amount = amount;
    this.city = city;
  }
}

class Solution {
  public List<String> invalidTransactions(String[] transactions) {
    List<String> ans = new ArrayList<>();
    Map<String, List<Trans>> nameToTranses = new HashMap<>();

    for (final String t : transactions) {
      Trans trans = getTrans(t);
      nameToTranses.putIfAbsent(trans.name, new ArrayList<>());
      nameToTranses.get(trans.name).add(trans);
    }

    for (final String t : transactions) {
      Trans currTrans = getTrans(t);
      if (currTrans.amount > 1000) {
        ans.add(t);
      } else if (nameToTranses.containsKey(currTrans.name)) {
        // Iterate through all the transactions with the same name, check if
        // they're within 60 minutes in a different city.
        for (Trans trans : nameToTranses.get(currTrans.name))
          if (Math.abs(trans.time - currTrans.time) <= 60 && !trans.city.equals(currTrans.city)) {
            ans.add(t);
            break;
          }
      }
    }

    return ans;
  }

  private Trans getTrans(final String t) {
    String[] s = t.split(","); // [name, time, amount, city]
    return new Trans(s[0], Integer.parseInt(s[1]), Integer.parseInt(s[2]), s[3]);
  }
}

class User {
    
      public static void main(String[] args) {
        String[] transactions = {
            "alice,20,800,mtv",
            "alice,50,100,beijing"
        };

        Solution solution = new Solution();
        List<String> invalidTransactions = solution.invalidTransactions(transactions);

        System.out.println("Invalid transactions:");
        for (String transaction : invalidTransactions) {
            System.out.println(transaction);
        }
      }
    
}


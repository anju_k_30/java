/*Max Area of Island
 
 You are given an m x n binary matrix grid. An island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

The area of an island is the number of cells with a value 1 in the island.

Return the maximum area of an island in grid. If there is no island, return 0.

Example 1:

Input: grid = [[0,0,1,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,1,1,0,1,0,0,0,0,0,0,0,0],[0,1,0,0,1,1,0,0,1,0,1,0,0],[0,1,0,0,1,1,0,0,1,1,1,0,0],[0,0,0,0,0,0,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,0,0,0,0,0,0,1,1,0,0,0,0]]
Output: 6
Explanation: The answer is not 11, because the island must be connected 4-directionally.
*/


class Solution {
    public int maxAreaOfIsland(int[][] grid) {
        
        int maxArea=Integer.MIN_VALUE;
        
        for(int i=0;i<grid.length;i++){
            for(int j=0;j<grid[0].length;j++){
                
                if(grid[i][j]==1){
                    
                    int area=dfs(grid,i,j);
                    
                    maxArea=Math.max(maxArea,area);
                }
            }
        }
        
        if(maxArea==Integer.MIN_VALUE)
            return 0;
        else
            return maxArea;
        
    }
    
    
    public int dfs(int [][]grid,int i,int j){
        
        //checking for invalid condition
        if(i<0 || i>=grid.length || j<0 || j>=grid[0].length || grid[i][j]!=1)
            return 0;
        
        //change value to 0 to avoid repeatation
        
        grid[i][j]=0;
        
        
        return 1 + dfs(grid,i-1,j) + dfs(grid,i,j+1) + dfs(grid,i+1,j) + dfs(grid,i,j-1);
    }
}

class User{
	public static void main(String s[]){
		int arr[][]=new int[][]{{0,0,1,0,0,0,0,1,0,0,0,0,0},{0,0,0,0,0,0,0,1,1,1,0,0,0},{0,1,1,0,1,0,0,0,0,0,0,0,0},{0,1,0,0,1,1,0,0,1,0,1,0,0},{0,1,0,0,1,1,0,0,1,1,1,0,0},{0,0,0,0,0,0,0,0,0,0,1,0,0},{0,0,0,0,0,0,0,1,1,1,0,0,0},{0,0,0,0,0,0,0,1,1,0,0,0,0}};
		Solution obj=new Solution();
		System.out.println(obj.maxAreaOfIsland(arr));
	}
}


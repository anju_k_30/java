/*
Given a boolean 2D array of n x m dimensions, consisting of only 1's and 0's, where each row is sorted. Find the 0-based index of the first row that has the maximum number of 1's.

Example 1:

Input: 
N = 4 , M = 4
Arr[][] = {{0, 1, 1, 1},
           {0, 0, 1, 1},
           {1, 1, 1, 1},
           {0, 0, 0, 0}}
Output: 2
Explanation: Row 2 contains 4 1's (0-based
indexing).
*/

class Solution {
    int rowWithMax1s(int mat[][], int n, int m) {

        int maxOnesCount = 0;
        int rowIndex = -1;

        int row = 0;
        int col = m - 1;

        while (row < n && col >= 0) {
            if (mat[row][col] == 1) {
                // Move to the left in the current row
                col--;
                // Update the maximum count and corresponding row index
                maxOnesCount = m - col - 1;
                rowIndex = row;
            } else {
                // Move down to the next row
                row++;
            }
        }

        return rowIndex;
    }
}

class User{
	public static void main(String s[]){
		int arr[][]=new int [][]{{0,1,1,1},{0,0,1,1},{1,1,1,1},{0,0,0,0}};

		Solution obj=new Solution();
		System.out.println(obj.rowWithMax1s(arr,4,4));
	}
}


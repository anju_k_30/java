/*Given an array of integers Arr of size N and a number K. Return the maximum sum of a subarray of size K.

NOTE*: A subarray is a contiguous part of any given array.

Example 1:

Input:
N = 4, K = 2
Arr = [100, 200, 300, 400]
Output:
700
Explanation:
Arr3  + Arr4 =700,
which is maximum.
*/
//Sliding Window

import java.util.*;
class Solution {
    static long maximumSumSubarray(int K, ArrayList<Integer> Arr, int N) {
        long maxSum = 0;
        long currentSum = 0;

        // Calculate the sum of the first subarray of size K
        for (int i = 0; i < K; i++) {
            currentSum += Arr.get(i);
        }
        maxSum = currentSum;

        // Iterate through the array and find the maximum sum of subarrays of size K
        for (int i = K; i < N; i++) {
            // Subtract the element leaving the window and add the new element entering the window
            currentSum += Arr.get(i) - Arr.get(i - K);
            // Update maxSum if the current sum is greater
            maxSum = max(maxSum, currentSum);
        }

        return maxSum;
    }
    private static long max(long a, long b) {
        return a > b ? a : b;
    }
}

class User{
	public static void main(String s[]){
		  ArrayList<Integer> Arr = new ArrayList<>();
      		  Arr.add(100);
        	  Arr.add(200);
        	  Arr.add(300);
       		  Arr.add(400);
        
        	  int N = Arr.size();
        	  int K = 2;
		  Solution obj=new Solution();
		
		  long result = obj.maximumSumSubarray(K, Arr, N);
        	  System.out.println(result);
	
	}
}

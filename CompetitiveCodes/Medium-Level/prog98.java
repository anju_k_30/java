/*Jump Game:
 * Given an positive integer N and a list of N integers A[]. Each element in the array denotes the maximum length of jump you can cover. Find out if you can make it to the last index if you start at the first index of the list.


Example 1:

Input:
N = 6
A[] = {1, 2, 0, 3, 0, 0}
Output:
1
Explanation:
Jump 1 step from first index to
second index. Then jump 2 steps to reach
4th index, and now jump 2 steps to reach
the end.
*/

class Solution {
    static int canReach(int[] A, int N) {

        if(N == 0 || N == 1) return 1;
        int jumps = 0;
        for(int i = 0; i <= jumps; i++){
            jumps = Math.max(jumps, i+A[i]);
            if(i >= N-1) return 1;
        }

        return 0;
    }
};
class User{
	public static void main(String s[]){
		int arr[]=new int[]{1,2,0,3,0,0};
		Solution obj=new Solution();
		System.out.println(obj.canReach(arr,6));
	}
}


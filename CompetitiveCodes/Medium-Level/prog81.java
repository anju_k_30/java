/*
 3closest sum
 Given an integer array nums of length n and an integer target, find three integers in nums such that the sum is closest to target.

Return the sum of the three integers.

You may assume that each input would have exactly one solution.

Example 1:

Input: nums = [-1,2,1,-4], target = 1
Output: 2
Explanation: The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
*/

import java.util.Arrays;

class Solution {
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);

        int closestSum = Integer.MAX_VALUE;
        int minDiff = Integer.MAX_VALUE;

        for (int i = 0; i < nums.length - 2; i++) {
            int left = i + 1; // Index of the left pointer
            int right = nums.length - 1; // Index of the right pointer

            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right]; // Calculate the sum of the current triplet

                // Check if the current sum is closer to the target
                if (Math.abs(sum - target) < minDiff) {
                    minDiff = Math.abs(sum - target);
                    closestSum = sum;
                }

                // Move the pointers based on the sum
                if (sum < target) {
                    left++; // Increase the left pointer if the sum is smaller than the target
                } else {
                    right--; // Decrease the right pointer if the sum is larger than the target
                }
            }
        }

        return closestSum;
    }

}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{-1,2,1,-4};
		Solution obj=new Solution();
		int target =1;
		System.out.println(obj.threeSumClosest(arr,target));
	}
}

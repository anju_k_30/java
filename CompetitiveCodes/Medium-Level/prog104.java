/*Buy and Sell a Share at most twice

In daily share trading, a buyer buys shares in the morning and sells them on the same day. If the trader is allowed to make at most 2 transactions in a day, the second transaction can only start after the first one is complete (buy->sell->buy->sell). The stock prices throughout the day are represented in the form of an array of prices. 

Given an array price of size n, find out the maximum profit that a share trader could have made.

Example 1:

Input:
n = 6
prices[] = {10,22,5,75,65,80}
Output:
87
Explanation:
Trader earns 87 as sum of 12, 75 Buy at 10, sell at 22, Buy at 5 and sell at 80.*/

class Solution {
    public static int maxProfit(int n, int[] price) {
        if (n <= 1)
            return 0;

        // Initialize arrays to track the maximum profit achievable with at most 2 transactions
        int[] profit1 = new int[n]; // Maximum profit after the first transaction
        int[] profit2 = new int[n]; // Maximum profit after the second transaction

        // Initialize variables to track the minimum price to buy at for the first and second transactions
        int minBuy1 = price[0];
        int minBuy2 = price[0];

        // Calculate the maximum profit after the first transaction
        for (int i = 1; i < n; i++) {
            minBuy1 = Math.min(minBuy1, price[i]);
            profit1[i] = Math.max(profit1[i - 1], price[i] - minBuy1);
        }

        // Calculate the maximum profit after the second transaction
        for (int i = 1; i < n; i++) {
            minBuy2 = Math.min(minBuy2, price[i] - profit1[i]); // Adjusted buying price considering profit from the first transaction
            profit2[i] = Math.max(profit2[i - 1], price[i] - minBuy2);
        }

        // Maximum profit achievable after two transactions will be the maximum profit after the second transaction
        return profit2[n - 1];
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{10,22,5,75,65,80};
		Solution obj=new Solution();
		System.out.println(obj.maxProfit(arr.length,arr));
	}
}

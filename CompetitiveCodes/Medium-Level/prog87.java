/*Given an array of positive numbers, the task is to find the number of possible contiguous subarrays having product less than a given number k.

Example 1:

Input : 
n = 4, k = 10
a[] = {1, 2, 3, 4}
Output : 
7
Explanation:
The contiguous subarrays are {1}, {2}, {3}, {4} 
{1, 2}, {1, 2, 3} and {2, 3}, in all these subarrays
product of elements is less than 10, count of
such subarray is 7.
{2,3,4} will not be a valid subarray, because 
2*3*4=24 which is greater than 10.*/

class Solution {

    public long countSubArrayProductLessThanK(long a[], int n, long k)
    {
        int l=0,r=0;
        long ans=0;
        long pro=a[0];
        while(r<n){
            if(pro<k){
                ans+=(r-l+1);
                r++;
                if(r<n)
                {
                    pro*=a[r];

                }
            }
            else{
                pro/=a[l];
                if(l==r){
                    ++r;
                    if (r<n)
                {
                    pro*=a[r];

                }
                }
                ++l;


            }
        }
        return ans;
    }
}
class User{
	public static void main(String s[]){
		long arr[]=new long[]{1,2,3,4};
		Solution obj=new Solution();
		long k=10;
		System.out.println(obj.countSubArrayProductLessThanK(arr,arr.length,k));
	}
}


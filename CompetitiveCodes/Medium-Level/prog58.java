/*Given an integer array nums of length n where all the integers of nums are in the range [1, n] and each integer appears once or twice, return an array of all the integers that appears twice.

You must write an algorithm that runs in O(n) time and uses only constant extra space.

Example 1:

Input: nums = [4,3,2,7,8,2,3,1]
Output: [2,3]

*/
import java.util.*;
class Solution {
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> al=new ArrayList<Integer>();
        
        Arrays.sort(nums);
         
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1] && (al.isEmpty() || nums[i]!=al.get(al.size()-1))) {
                al.add(nums[i]);
            }
	}

        if(al.isEmpty()){
            al.add(-1);
            return al;
        }else
            return al;
    }
}


class User{
	public static void main(String s[]){
		int arr[]=new int[]{4,3,2,7,8,2,3,1};
		Solution obj=new Solution();
		System.out.println(obj.findDuplicates(arr));
	}
}

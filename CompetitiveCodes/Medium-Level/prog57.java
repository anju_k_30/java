/*Given an integer array nums, return the number of subarrays filled with 0.

A subarray is a contiguous non-empty sequence of elements within an array.

 

Example 1:

Input: nums = [1,3,0,0,2,0,0,4]
Output: 6
Explanation: 
There are 4 occurrences of [0] as a subarray.
There are 2 occurrences of [0,0] as a subarray.
There is no occurrence of a subarray with a size more than 2 filled with 0. Therefore, we return 6.
*/

class Solution {
    public long zeroFilledSubarray(int[] arr) {
        
        long count=0;
        long n=0;
        
        for(int i=0;i<arr.length;i++){
            if(arr[i]==0){
                count++;
            }
            else{
                count=0;
            }
            n=n+count;
        }
        
        return n;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{1,3,0,0,2,0,0,4};
		Solution obj=new Solution();
		System.out.println(obj.zeroFilledSubarray(arr));
	}
}

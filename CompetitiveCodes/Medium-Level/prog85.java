/* Non overlapping Intervals:
 
Given an array of intervals intervals where intervals[i] = [starti, endi], return the minimum number of intervals you need to remove to make the rest of the intervals non-overlapping.

Example 1:

Input: intervals = [[1,2],[2,3],[3,4],[1,3]]
Output: 1
Explanation: [1,3] can be removed and the rest of the intervals are non-overlapping.
*/

import java.util.*;
class Solution {
  public int eraseOverlapIntervals(int[][] intervals) {
    if (intervals.length == 0)
      return 0;

    Arrays.sort(intervals, (a, b) -> a[1] - b[1]);

    int ans = 0;
    int currentEnd = intervals[0][1];

    for (int i = 1; i < intervals.length; ++i)
      if (intervals[i][0] >= currentEnd)
        currentEnd = intervals[i][1];
      else
        ++ans;

    return ans;
  }
}

class User{
	public static void main(String s[]){
		int arr[][]=new int[][]{{1,2},{2,3},{3,4},{1,3}};
		Solution obj=new Solution();
		System.out.println(obj.eraseOverlapIntervals(arr));
	}
}


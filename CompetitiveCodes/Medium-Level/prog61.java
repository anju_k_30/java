///maximum Difference


/*Given array A[] of integers, the task is to complete the function findMaxDiff which finds the maximum absolute difference between nearest left and right smaller element of every element in array.If the element is the leftmost element, nearest smaller element on left side is considered as 0. Similarly if the element is the rightmost elements, smaller element on right side is considered as 0.

Examples:

Input : arr[] = {2, 1, 8}
Output : 1
Left smaller  LS[] {0, 0, 1}
Right smaller RS[] {1, 0, 0}
Maximum Diff of abs(LS[i] - RS[i]) = 1 

Input  : arr[] = {2, 4, 8, 7, 7, 9, 3}
Output : 4
Left smaller   LS[] = {0, 2, 4, 4, 4, 7, 2}
Right smaller  RS[] = {0, 3, 7, 3, 3, 3, 0}
Maximum Diff of abs(LS[i] - RS[i]) = 7 - 3 = 4 

Input : arr[] = {5, 1, 9, 2, 5, 1, 7}
Output : 1

*/

class Solution
{
    int findMaxDiff(int a[], int n)
    {

	 int LS[] = new int[n];
        int RS[] = new int[n];
        // put 1st value and last value in LS ang RS resp...
        LS[0] = 0;
        RS[n - 1] = 0;
        for (int i = 1; i < n; i++) {

            for (int j = i - 1; j >= 0; j--) {
                if (a[j] < a[i]) {
                    LS[i] = a[j];
                    break;
                } else {
                    LS[i] = 0;
                }

            }

        }
        for (int i = 0; i < n - 1; i++) {

            for (int j = i + 1; j < n; j++) {
                if (a[j] < a[i]) {
                    RS[i] = a[j];
                    break;
                } else {
                    RS[i] = 0;
                }

            }

        }
        // find the difference
        int diff = 0;
        int Max = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            if (LS[i] > RS[i]) {
                diff = LS[i] - RS[i];
            } else {
                diff = RS[i] - LS[i];
            }
            if (diff > Max) {
                Max = diff;
            }

        }
        return Max;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{2,1,8};
		Solution obj=new Solution();
		System.out.println(obj.findMaxDiff(arr,3));
	}
}


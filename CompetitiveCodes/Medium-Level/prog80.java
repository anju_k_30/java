/*Search in a matrix:

Given a matrix mat[][] of size N x M, where every row and column is sorted in increasing order, and a number X is given. The task is to find whether element X is present in the matrix or not.


Example 1:

Input:
N = 3, M = 3
mat[][] = 3 30 38 
         44 52 54 
         57 60 69
X = 62
Output:
0
Explanation:
62 is not present in the
matrix, so output is 0
*/

class Solution {
    public static int matSearch(int mat[][], int N, int M, int X) {
        if (mat == null || N == 0 || M == 0) {
            return 0; // Matrix is empty
        }

        int row = 0;
        int col = M - 1; // Start from the top-right corner

        while (row < N && col >= 0) {
            if (mat[row][col] == X) {
                return 1; // Found the target
            } else if (mat[row][col] < X) {
                // Move down (since the current element is smaller than the target)
                row++;
            } else {
                // Move left (since the current element is larger than the target)
                col--;
            }
        }

        return 0; // Target not found
    }
}

class User {
    public static void main(String s[]) {
        int mat[][] = { { 10, 20, 30, 40 },
                        { 15, 25, 35, 45 },
                        { 27, 29, 37, 48 },
                        { 32, 33, 39, 50 } };
        Solution obj = new Solution();
        System.out.println(obj.matSearch(mat, mat.length, mat[0].length, 5));
    }
}

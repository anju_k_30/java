/*1465. Maximum Area of a Piece of Cake After Horizontal and Vertical Cuts
Medium

You are given a rectangular cake of size h x w and two arrays of integers horizontalCuts and verticalCuts where:

    horizontalCuts[i] is the distance from the top of the rectangular cake to the ith horizontal cut and similarly, and
    verticalCuts[j] is the distance from the left of the rectangular cake to the jth vertical cut.

Return the maximum area of a piece of cake after you cut at each horizontal and vertical position provided in the arrays horizontalCuts and verticalCuts. Since the answer can be a large number, return this modulo 109 + 7.

Example 1:

Input: h = 5, w = 4, horizontalCuts = [1,2,4], verticalCuts = [1,3]
Output: 4 
Explanation: The figure above represents the given rectangular cake. Red lines are the horizontal and vertical cuts. After you cut the cake, the green piece of cake has the maximum area.
*/
import java.util.Arrays;
class Solution {
  public int maxArea(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
    final int kMod = 1_000_000_007;
    Arrays.sort(horizontalCuts);
    Arrays.sort(verticalCuts);

    // the maximum gap of each direction
    int maxGapX = Math.max(verticalCuts[0], w - verticalCuts[verticalCuts.length - 1]);
    int maxGapY = Math.max(horizontalCuts[0], h - horizontalCuts[horizontalCuts.length - 1]);

    for (int i = 1; i < verticalCuts.length; ++i)
      maxGapX = Math.max(maxGapX, verticalCuts[i] - verticalCuts[i - 1]);

    for (int i = 1; i < horizontalCuts.length; ++i)
      maxGapY = Math.max(maxGapY, horizontalCuts[i] - horizontalCuts[i - 1]);

    return (int) ((long) maxGapX * maxGapY % kMod);
  }
}
class User{
	public static void main(String s[]){
		int horizontalCuts[]=new int[]{1,2,4};
		int verticalCuts[]=new int[]{1,3};
		int h=5;
		int w=4;
		Solution obj=new Solution();
		System.out.println(obj.maxArea(h,w,horizontalCuts,verticalCuts));
	}
}


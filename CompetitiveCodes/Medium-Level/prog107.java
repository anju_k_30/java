/*
 1423. Maximum Points You Can Obtain from Cards
Medium

There are several cards arranged in a row, and each card has an associated number of points. The points are given in the integer array cardPoints.

In one step, you can take one card from the beginning or from the end of the row. You have to take exactly k cards.

Your score is the sum of the points of the cards you have taken.

Given the integer array cardPoints and the integer k, return the maximum score you can obtain. 

Example 1:

Input: cardPoints = [1,2,3,4,5,6,1], k = 3
Output: 12
Explanation: After the first step, your score will always be 1. However, choosing the rightmost card first will maximize your total score. The optimal strategy is to take the three cards on the right, giving a final score of 1 + 6 + 5 = 12.
*/

import java.util.*;
class Solution {
  public int maxScore(int[] cardPoints, int k) {
    final int n = cardPoints.length;
    final int sum = Arrays.stream(cardPoints).sum();
    int windowSum = 0;
    for (int i = 0; i < n - k; ++i)
      windowSum += cardPoints[i];

    int ans = sum - windowSum;

    for (int i = 0; i < k; ++i) {
      windowSum -= cardPoints[i];
      windowSum += cardPoints[i + n - k];
      ans = Math.max(ans, sum - windowSum);
    }

    return ans;
  }
}

class User{
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] cardPoints = {1, 2, 3, 4, 5, 6, 1};
        int k = 3;

        int maxScore = solution.maxScore(cardPoints, k);

        System.out.println("Maximum score: " + maxScore);
    }
}


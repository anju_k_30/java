/*Maximum Product Subarray(152)
Given an integer array nums, find a subarray that has the largest product, and return the product.

The test cases are generated so that the answer will fit in a 32-bit integer.

Example 1:

Input: nums = [2,3,-2,4]
Output: 6
Explanation: [2,3] has the largest product 6.
*/

class Solution {
    public int maxProduct(int[] arr) {

	int maxPro = Integer.MIN_VALUE;
        int firstPro = 1;
        int secPro = 1;

        for(int i =0;i<arr.length;i++){
            firstPro *= arr[i];
            maxPro = Math.max(maxPro,firstPro);
            if(arr[i]==0)
                firstPro =1;
        }

        for(int i=arr.length-1;i>=0;i--){
            secPro *= arr[i];
            maxPro = Math.max(maxPro,secPro);
            if(arr[i]==0)
                secPro = 1;
        }
        return maxPro;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{2,3,-2,4};
		Solution obj=new Solution();
		System.out.println(obj.maxProduct(arr));
	}
}

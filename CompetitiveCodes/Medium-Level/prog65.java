/*You are given a list of songs where the ith song has a duration of time[i] seconds.

Return the number of pairs of songs for which their total duration in seconds is divisible by 60. Formally, we want the number of indices i, j such that i < j with (time[i] + time[j]) % 60 == 0.

Example 1:

Input: time = [30,20,150,100,40]
Output: 3
Explanation: Three pairs have a total duration divisible by 60:
(time[0] = 30, time[2] = 150): total duration 180
(time[1] = 20, time[3] = 100): total duration 120
(time[1] = 20, time[4] = 40): total duration 60

Example 2:

Input: time = [60,60,60]
Output: 3
Explanation: All three pairs have a total duration of 120, which is divisible by 60.
*/

import java.util.*;
class Solution {
    public int numPairsDivisibleBy60(int[] time) {

        int count = 0;
        Map<Integer,Integer> hm = new HashMap<Integer,Integer>();

       /* for(int i=0; i<time.length; i++){
             for(int j=i+1; j<time.length; j++){
                 if((time[i] + time[j]) % 60 == 0){
                     count++;
                 }
             }
         }

        return count;*/

         for(int i=0; i<time.length; i++){
            if(time[i]>=60){
                time[i] = time[i]%60;
            }
        }
        for(int i=0; i<time.length; i++){

            if(time[i] == 0 && hm.containsKey(time[i])){
                count = count + hm.get(time[i]);
            }
            if(hm.containsKey(60-time[i])) {
                count = count + hm.get(60-time[i]);
            }
            hm.put(time[i],hm.getOrDefault(time[i],0)+1);
        }
        return count;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{30,20,150,100,40};
		Solution obj=new Solution();
		System.out.println(obj.numPairsDivisibleBy60(arr));
	}
}

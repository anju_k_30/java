/*Rotate Image
 *
You are given an n x n 2D matrix representing an image, rotate the image by 90 degrees (clockwise).

You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.
Example 1:

Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
Output: [[7,4,1],[8,5,2],[9,6,3]]
*/

import java.util.*;
class Solution {
    public void rotate(int[][] matrix) {

         int n = matrix.length;

        // Step 1: Transpose the matrix
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                // Swap matrix[i][j] with matrix[j][i]
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }

        // Step 2: Reverse each row of the transposed matrix
        for (int i = 0; i < n; i++) {
            int start = 0;
            int end = n - 1;
            while (start < end) {
                // Swap matrix[i][start] with matrix[i][end]
                int temp = matrix[i][start];
                matrix[i][start] = matrix[i][end];
                matrix[i][end] = temp;
                start++;
                end--;
            }
        }

    }
}
class User{
	public static void main(String s[]){
		int arr[][]=new int[][]{{1,2,3},{4,5,6},{7,8,9}};

		Solution obj=new Solution();
		obj.rotate(arr);

		 for (int i = 0; i < arr.length; i++) {
          	 	for (int j = 0; j < arr[i].length; j++) {
                		System.out.print(arr[i][j] + " ");
            		}
            		System.out.println();
        	}
	}
}


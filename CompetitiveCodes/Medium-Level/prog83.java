/*Transition Point:

Given a sorted array containing only 0s and 1s, find the transition point, i.e., the first index where 1 was observed, and before that, only 0 was observed.

Example 1:

Input:
N = 5
arr[] = {0,0,0,1,1}
Output: 3
Explanation: index 3 is the transition 
point where 1 begins.
*/

class Solution {
		int transitionPoint(int arr[], int n) {
		int l = 0;
		int h = n - 1;
	    int index = -1;
    	while (l <= h) {
			int mid = (l + h) / 2;
        	if (arr[mid] == 1) {
				index = mid;
				h = mid - 1;
			} else {
				l = mid + 1;
			}
		}
		return index;
	}
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{0,0,0,1,1};
		Solution obj=new Solution();
		System.out.println(obj.transitionPoint(arr,5));
	}
}


/*Back to Explore Page
Remove duplicates from an unsorted linked list
EasyAccuracy: 45.95%Submissions: 218K+Points: 2

Internship Alert!
Become an SDE Intern by topping this monthly leaderboard! 
banner

Given an unsorted linked list of N nodes. The task is to remove duplicate elements from this unsorted Linked List. When a value appears in multiple nodes, the node which appeared first should be kept, all others duplicates are to be removed.

Example 1:

Input:
N = 4
value[] = {5,2,2,4}
Output: 5 2 4
Explanation:Given linked list elements are
5->2->2->4, in which 2 is repeated only.
So, we will delete the extra repeated
elements 2 from the linked list and the
resultant linked list will contain 5->2->4
*/

import java.util.HashSet;

class Node {
    int data;
    Node next;

    public Node(int data) {
        this.data = data;
        this.next = null;
    }
}

class Solution {
    public Node removeDuplicates(Node head) {
        Node itr = head;
        Node temp = new Node(-1);
        Node ref = temp;
        HashSet<Integer> hs = new HashSet<>();

        while (itr != null) {
            if (hs.contains(itr.data)) {
                itr = itr.next;
            } else {
                hs.add(itr.data);
                ref.next = itr;
                itr = itr.next;
                ref = ref.next;
            }
        }
        ref.next = null;
        return temp.next;
    }
}

class User {
    public static void main(String[] args) {
        Node head = new Node(5);
        head.next = new Node(2);
        head.next.next = new Node(2);
        head.next.next.next = new Node(4);

        Solution solution = new Solution();
        Node result = solution.removeDuplicates(head);

        // Printing the result
        while (result != null) {
            System.out.print(result.data + " ");
            result = result.next;
        }
    }
}


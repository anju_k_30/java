/*Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of nums except nums[i].

The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

You must write an algorithm that runs in O(n) time and without using the division operation.

 

Example 1:

Input: nums = [1,2,3,4]
Output: [24,12,8,6]
*/

import java.util.*;
class Solution {
    public int[] productExceptSelf(int[] nums) {
        
        int arr1[]=new int[nums.length];
        int arr2[]=new int[nums.length];
        
        arr1[0]=1;
        arr2[nums.length-1]=1;
        
        for(int i=1;i<nums.length;i++){
            arr1[i]=arr1[i-1]*nums[i-1];            
        }
        
        for(int i=nums.length-2;i>=0;i--){
            arr2[i]=arr2[i+1]*nums[i+1];
        }
        
        int result[]=new int[nums.length];
        
        for(int i=0;i<result.length;i++){
            result[i]=arr1[i]*arr2[i];
        }
        
        return result;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{1,2,3,4};
		Solution obj=new Solution();
		System.out.println(Arrays.toString(obj.productExceptSelf(arr)));
	}
}

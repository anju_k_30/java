/*Given an unsorted array Arr of size N of positive integers. One number 'A' from set {1, 2,....,N} is missing and one number 'B' occurs twice in array. Find these two numbers.

Example 1:

Input:
N = 2
Arr[] = {2, 2}
Output: 2 1
Explanation: Repeating number is 2 and 
smallest positive missing number is 1.
*/

import java.util.*;
class Solution {
    int[] findTwoElement(int arr[], int n) {
        int[] temp = new int[n];
        int repeatingNumber = -1;
        int missingNumber = -1;
        int[] temp2=new int[2];

        for (int i = 0; i < n; i++) {
            temp[arr[i] - 1]++;
            if (temp[arr[i] - 1] > 1) {
                repeatingNumber = arr[i];
            }
        }
        for (int i = 0; i < n; i++) {
            if (temp[i] == 0) {
                missingNumber = i + 1;
                break;
            }
        }
        temp2[1]=missingNumber;
        temp2[0]=repeatingNumber;

        return temp2;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{2,2};
		Solution obj=new Solution();
		System.out.println(Arrays.toString(obj.findTwoElement(arr,2)));
	}
}

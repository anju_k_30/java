/*Stickler Thief
MediumAccuracy: 37.98%Submissions: 212K+Points: 4

Internship Alert
New month-> Fresh Chance to top the leaderboard and get SDE Internship! 
banner

Stickler the thief wants to loot money from a society having n houses in a single line. He is a weird person and follows a certain rule when looting the houses. According to the rule, he will never loot two consecutive houses. At the same time, he wants to maximize the amount he loots. The thief knows which house has what amount of money but is unable to come up with an optimal looting strategy. He asks for your help to find the maximum money he can get if he strictly follows the rule. ith house has a[i] amount of money present in it.

Example 1:

Input:
n = 5
a[] = {6,5,5,7,4}
Output: 
15
Explanation: 
Maximum amount he can get by looting 1st, 3rd and 5th house. Which is 6+5+4=15.
*/

class Solution
{

    public int FindMaxSum(int arr[], int n)
    {
        if(n == 0){
            return 0;
        }
        if(n == 1){
            return arr[1];
        }
        if(n == 2){
            return Math.max(arr[0],arr[1]);
        }
        int arr1[] = new int[n];
        arr1[0] = arr[0];
        arr1[1] = Math.max(arr[0],arr[1]);

        for(int i=2 ; i<n ; i++){
            arr1[i] = Math.max(arr1[i-2]+arr[i],arr1[i-1]);
        }
        return arr1[n-1];
    }
}
class User {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] arr = {6,5,5,7,4};

        int maxSum = solution.FindMaxSum(arr, arr.length);
        System.out.println("Maximum sum of non-adjacent elements: " + maxSum);
    }
}


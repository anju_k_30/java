/*78. Subsets
Medium

Given an integer array nums of unique elements, return all possible subsets (the power set).

The solution set must not contain duplicate subsets. Return the solution in any order.

 

Example 1:

Input: nums = [1,2,3]
Output: [[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
*/

import java.util.*;
class Solution {
    public List<List<Integer>> subsets(int[] arr) {

        List<List<Integer>> al = new ArrayList<>();
        al.add(new ArrayList<Integer>());

        for(int i : arr){
            int n = al.size();
            for(int j=0 ; j<n ; ++j){
                ArrayList<Integer> al1 = new ArrayList(al.get(j));
                al1.add(i);
                al.add(al1);
            }
        }
        return al;
    }
}
class User {
    
	public static void main(String[] args) {
        Solution solution = new Solution();

        int[] arr = {1, 2, 3};

        List<List<Integer>> subsets = solution.subsets(arr);

        System.out.println("Subsets of the array:");
        for (List<Integer> subset : subsets) {
            System.out.println(subset);
        }
    }
}


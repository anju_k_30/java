/*
 * Given an array Arr of size N, print the second largest distinct element from an array. If the second largest element doesn't exist then return -1.

Example 1:

Input:
N = 6
Arr[] = {12, 35, 1, 10, 34, 1}
Output: 34
Explanation: The largest element of the
array is 35 and the second largest element
is 34.
*/


class Solution {
    int print2largest(int arr[], int n) {


       /*Arrays.sort(arr); // Sorting the array

        int secondLargest = Integer.MIN_VALUE;

        for (int i = n - 2; i >= 0; i--) {
            if (arr[i] != arr[n - 1]) {
                secondLargest = arr[i];
                break;
            }
        }
        */

        int largest = Integer.MIN_VALUE;
        int secondLargest = Integer.MIN_VALUE;

        for (int number : arr) {
            if (number > largest) {
                secondLargest = largest;
                largest = number;
            } else if (number > secondLargest && number != largest) {
                secondLargest = number;
            }
        }

        if (secondLargest == Integer.MIN_VALUE) {
            secondLargest = -1; // Update secondLargest to -1 if all numbers are the same
        }
        return secondLargest;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{12, 35, 1, 10, 34, 1};
		Solution obj=new Solution();
		System.out.println(obj.print2largest(arr,5));
	}
}


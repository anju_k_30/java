
/*Given two sorted arrays arr1 and arr2 of size N and M respectively and an element K. The task is to find the element that would be at the kth position of the final sorted array.
 

Example 1:

Input:
arr1[] = {2, 3, 6, 7, 9}
arr2[] = {1, 4, 8, 10}
k = 5
Output:
6
Explanation:
The final sorted array would be -
1, 2, 3, 4, 6, 7, 8, 9, 10
The 5th element of this array is 6.
*/

class Solution {
    public long kthElement( int arr1[], int arr2[], int n, int m, int k) {
        int i =0;
        int j =0;
        int ret = 0;
        int cnt =0;

        while(i<n && j<m){
            if(arr1[i] <= arr2[j]){
                ret = arr1[i];
                i++;
            }else{
                ret = arr2[j];
                j++;
            }
            cnt++;
            if(cnt == k){
                return ret;
            }
        }

        while(i<n){
            cnt++;
            if(cnt == k){
                return arr1[i];

            }
            i++;
        }

        while(j<m){
            cnt++;
            if(cnt == k){
                return arr2[j];
            }
            j++;
        }

        return -1;

    }
}

class User {
    public static void main(String[] args) {
       
        int[] arr1 = {1, 3, 5, 7, 9};
        int[] arr2 = {2, 4, 6, 8, 10};
        
        int n = arr1.length;
        int m = arr2.length;
        
        int k = 5; 

        Solution solution = new Solution();
        
        long kth = solution.kthElement(arr1, arr2, n, m, k);
        
     
        if (kth != -1) {
            System.out.println("The " + k + "th element is: " + kth);
        } else {
            System.out.println("The " + k + "th element doesn't exist.");
        }
    }
}


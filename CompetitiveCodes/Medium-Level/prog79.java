/*Set Matrix to Zeros:
 * Given an m x n integer matrix matrix, if an element is 0, set its entire row and column to 0's.

You must do it in place.

Example 1:

Input: matrix = [[1,1,1],[1,0,1],[1,1,1]]
Output: [[1,0,1],[0,0,0],[1,0,1]]
*/

import java.util.*;
class Solution {
    public void setZeroes(int[][] matrix) {

        Set<Integer> zeroRows = new HashSet<>();
        Set<Integer> zeroCols = new HashSet<>();

        // Find zero rows and columns
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] == 0) {
                    zeroRows.add(i);
                    zeroCols.add(j);
                }
            }
        }

        // Zero out rows
        for (int row : zeroRows) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[row][j] = 0;
            }
        }

        // Zero out columns
        for (int col : zeroCols) {
            for (int i = 0; i < matrix.length; i++) {
                matrix[i][col] = 0;
            }
        }

    }
}
class User{
	public static void main(String s[]){
		int arr[][]=new int[][]{{1,1,1},{1,0,1},{1,1,1}};
		Solution obj=new Solution();
		obj.setZeroes(arr);
		for (int[] row : arr) {
         		System.out.println(Arrays.toString(row));
		}
        
	}
}

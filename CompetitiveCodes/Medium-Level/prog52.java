/*Given an integer array nums and an integer k, return the kth largest element in the array.

Note that it is the kth largest element in the sorted order, not the kth distinct element.

Can you solve it without sorting?

 

Example 1:

Input: nums = [3,2,1,5,6,4], k = 2
Output: 5
*/

import java.util.*;
class Solution {
    public int findKthLargest(int[] nums, int k) {
        
        Arrays.sort(nums);
        
        return nums[nums.length-k];
    }
}

class User{
	public static void main(String[] args) {
        	int[] arr = {3, 1, 4, 1, 5, 9, 2, 6};
       		int k = 3;
		Solution obj=new Solution();
        	int result = obj.findKthLargest(arr, k);
        	System.out.println("The " + k + "th largest element is: " + result);
    }
}

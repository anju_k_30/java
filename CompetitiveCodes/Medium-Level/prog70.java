/*Word Search:
 * Given an m x n grid of characters board and a string word, return true if word exists in the grid.

The word can be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or vertically neighboring. The same letter cell may not be used more than once.

Example 1:

Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
Output: true
*/

class Solution {
    boolean searchNeighbours(int i,int j,int row,int col,int wordSize, char[][] board,String word){
        if((wordSize >= word.length()) || (word.length() == 1 && word.charAt(wordSize)==board[i][j])){
         return true;
        }

        if(i<0 || i>=row || j<0 || j>=col || board[i][j]=='.' || word.charAt(wordSize)!=board[i][j]){
            return false;
        }

        board[i][j]='.';
        boolean temp = false;

        temp = temp || searchNeighbours(i,j-1,row,col,wordSize+1,board,word);
         temp = temp || searchNeighbours(i,j+1,row,col,wordSize+1,board,word);
          temp = temp || searchNeighbours(i-1,j,row,col,wordSize+1,board,word);
           temp = temp || searchNeighbours(i+1,j,row,col,wordSize+1,board,word);

        board[i][j]=word.charAt(wordSize);

        return temp;

    }
    public boolean exist(char[][] board, String word) {
        int n = board.length;
        if(n==0){
            return false;
        }
        int m = board[0].length;

        for(int i =0;i<n;i++){

            for(int j=0;j<m;j++){

                if(word.charAt(0)==board[i][j]){
                    if(searchNeighbours(i,j,n,m,0,board,word)) return true;
                }
            }
        }
        return false;


    }
}

class User {
    public static void main(String[] args) {
        char[][] board = {
            {'A','B','C','E'},
            {'S','F','C','S'},
            {'A','D','E','E'}
        };

        String word = "ABCCED";

        Solution solution = new Solution();
        boolean exists = solution.exist(board, word);

        if (exists) {
            System.out.println("The word exists in the board.");
        } else {
            System.out.println("The word does not exist in the board.");
        }
    }
}

/*Trapping Rain Water:
 * Given an array arr[] of N non-negative integers representing the height of blocks. If width of each block is 1, compute how much water can be trapped between the blocks during the rainy season.


Example 1:

Input:
N = 6
arr[] = {3,0,0,2,0,4}
Output:
10
Explanation:

Example 2:

Input:
N = 4
arr[] = {7,4,0,9}
Output:
10
Explanation:
Water trapped by above
block of height 4 is 3 units and above
block of height 0 is 7 units. So, the
total unit of water trapped is 10 units.
*/

class Solution {

    static long trappingWater(int arr[], int n) {
        long trappedWater = 0;

        if (n <= 2)
            return trappedWater;

        int left = 0;
        int right = n - 1;
        int leftMax = 0;
        int rightMax = 0;


        while (left < right) {

            leftMax = Math.max(leftMax, arr[left]);

            rightMax = Math.max(rightMax, arr[right]);


            if (leftMax < rightMax) {
                trappedWater += Math.max(0, leftMax - arr[left]);
                left++;
            } else {
                trappedWater += Math.max(0, rightMax - arr[right]);
                right--;
            }
        }

        return trappedWater;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{3,0,0,2,0,4};
		Solution obj=new Solution();
		System.out.println(obj.trappingWater(arr,6));
	}
}


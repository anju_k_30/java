/*k-diff pairs in an Array(532)
 * Given an array of integers nums and an integer k, return the number of unique k-diff pairs in the array.

A k-diff pair is an integer pair (nums[i], nums[j]), where the following are true:

    0 <= i, j < nums.length
    i != j
    |nums[i] - nums[j]| == k

Notice that |val| denotes the absolute value of val.
Example 1:

Input: nums = [3,1,4,1,5], k = 2
Output: 2
Explanation: There are two 2-diff pairs in the array, (1, 3) and (3, 5).
Although we have two 1s in the input, we should only return the number of unique pairs.
*/

import java.util.*;
class Solution {
    public int findPairs(int[] nums, int k) {

        Map<Integer,Integer> map=new LinkedHashMap<>();

        for(int i=0;i<nums.length;i++)
        {
            if(!map.containsKey(nums[i]))
            {
                map.put(nums[i],1);
            }
            else
            {
                map.put(nums[i],map.get(nums[i])+1);
            }
        }
        int count=0;
        for(Integer key:map.keySet())
        {
            if(k==0 && map.get(key)>1)
            {
                count++;
            }
            else if((k>0) && map.containsKey(key-k))
            {
                count++;
            }
        }
        return count;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{3,1,4,1,5};
		Solution obj=new Solution();
		int k=2;
		System.out.println(obj.findPairs(arr,k));
	}
}

/*Merge Intervals:
 * Given an integer array nums and an integer k, return the number of non-empty subarrays that have a sum divisible by k.
A subarray is a contiguous part of an array.

Example 1:

Input: nums = [4,5,0,-2,-3,1], k = 5
Output: 7
Explanation: There are 7 subarrays with a sum divisible by k = 5:
[4, 5, 0, -2, -3, 1], [5], [5, 0], [5, 0, -2, -3], [0], [0, -2, -3], [-2, -3]
*/

class Solution {
    
    public int subarraysDivByK(int[] nums, int k) {

        int [] remainderArray = new int[k];

        remainderArray[0] = 1;
        int sum = 0;
        int count = 0;

        for(int a : nums){

            sum = (sum + a) % k;
            if(sum < 0) {
                sum += k;
            }
            count = count + remainderArray[sum];
            remainderArray[sum]++;
        }
        return count;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{4,5,0,-2,-3,1};
		Solution obj=new Solution();
		int k=5;
		System.out.println(obj.subarraysDivByK(arr,k));
	}
}


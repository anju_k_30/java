/*Given an array arr[] and an integer K where K is smaller than size of array, the task is to find the Kth smallest element in the given array. It is given that all array elements are distinct.

Note :-  l and r denotes the starting and ending index of the array.

Example 1:

Input:
N = 6
arr[] = 7 10 4 3 20 15
K = 3
L=0 R=5

Output : 7
Explanation :
3rd smallest element in the given 
array is 7.
*/


import java.util.*;
class Solution{
    public static int kthSmallest(int[] arr, int l, int r, int k) 
    { 
        Arrays.sort(arr);
        return arr[k-1];
    } 
}

class User{
	public static void main(String[] args) {
        	int[] arr = {7,10,4,3,20,15};
       		int k = 3;
		Solution obj=new Solution();
        	int result = obj.kthSmallest(arr,0,5, k);
        	System.out.println("The " + k + "th smallest element is: " + result);
    }
}

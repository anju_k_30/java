/*Minimum Platforms:
 * Given arrival and departure times of all trains that reach a railway station. Find the minimum number of platforms required for the railway station so that no train is kept waiting.
Consider that all the trains arrive on the same day and leave on the same day. Arrival and departure time can never be the same for a train but we can have arrival time of one train equal to departure time of the other. At any given instance of time, same platform can not be used for both departure of a train and arrival of another train. In such cases, we need different platforms.


Example 1:

Input: n = 6
arr[] = {0900, 0940, 0950, 1100, 1500, 1800}
dep[] = {0910, 1200, 1120, 1130, 1900, 2000}
Output: 3
Explanation:
Minimum 3 platforms are required to
safely arrive and depart all trains*/

import java.util.Arrays;

class Solution {
   static int findPlatform(int arr[], int dep[], int n) {
        
        Arrays.sort(arr);
        Arrays.sort(dep);

        int platformsNeeded = 1; // At least one platform is needed
        int maxPlatforms = 1;
        int i = 1; 
        int j = 0; 

        while (i < n && j < n) {
            // If next train arrives before the departure of current train
            if (arr[i] <= dep[j]) {
                platformsNeeded++;
                i++;
            }
            // If next train arrives after the departure of current train
            else {
                platformsNeeded--;
                j++;
            }
            
            maxPlatforms = Math.max(maxPlatforms, platformsNeeded);
        }

        return maxPlatforms;
    }
}

class User{
    public static void main(String[] args) {
        int[] arrival = {900, 940, 950, 1100, 1500, 1800};
        int[] departure = {910, 1200, 1120, 1130, 1900, 2000};
        int n = arrival.length;
	Solution obj=new Solution();

        int result = obj.findPlatform(arrival, departure, n);
        System.out.println("Minimum platforms required: " + result);
    }
}


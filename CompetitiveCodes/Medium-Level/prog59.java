/*Given an array of integers nums containing n + 1 integers where each integer is in the range [1, n] inclusive.

There is only one repeated number in nums, return this repeated number.

You must solve the problem without modifying the array nums and uses only constant extra space.


Example 1:

Input: nums = [1,3,4,2,2]
Output: 2
*/

import java.util.*;

class Solution {
    public int findDuplicate(int[] nums) {

        List<Integer> al=new ArrayList<Integer>();

        Arrays.sort(nums);

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1]) {
                return nums[i];
            }
        }

        return -1;
    }
}

class User{
        public static void main(String s[]){
                int arr[]=new int[]{1,3,4,2,2};
                Solution obj=new Solution();
                System.out.println(obj.findDuplicate(arr));
        }
}


/*
 * Given an integer array nums, rotate the array to the right by k steps, where k is non-negative.

Example 1:

Input: nums = [1,2,3,4,5,6,7], k = 3
Output: [5,6,7,1,2,3,4]
Explanation:
rotate 1 steps to the right: [7,1,2,3,4,5,6]
rotate 2 steps to the right: [6,7,1,2,3,4,5]
rotate 3 steps to the right: [5,6,7,1,2,3,4]
*/

import java.util.*;
class Solution {
    public void rotate(int[] nums, int k) {

        int n = nums.length;
        k = k % n; // Handle cases where k is greater than n

        reverse(nums, 0, n - 1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, n - 1);
    }
     static void reverse(int arr[],int low, int high){

            while(low<high){

                int temp=arr[low];

                arr[low]=arr[high];

                arr[high]=temp;

                low++;

                high--;

            }

        }
 }
class User{
	public static void main(String s[]){
		int arr[]=new int[]{1,2,3,4,5,6,7};
		Solution obj=new Solution();

		obj.rotate(arr,3);

		System.out.println(Arrays.toString(arr));
					
	}
}


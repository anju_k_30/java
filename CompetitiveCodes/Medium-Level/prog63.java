//Maximum of all subarrays of size k

/*Given an array arr[] of size N and an integer K. Find the maximum for each and every contiguous subarray of size K.

Example 1:

Input:
N = 9, K = 3
arr[] = 1 2 3 1 4 5 2 3 6
Output: 
3 3 4 5 5 5 6 
Explanation: 
1st contiguous subarray = {1 2 3} Max = 3
2nd contiguous subarray = {2 3 1} Max = 3
3rd contiguous subarray = {3 1 4} Max = 4
4th contiguous subarray = {1 4 5} Max = 5
5th contiguous subarray = {4 5 2} Max = 5
6th contiguous subarray = {5 2 3} Max = 5
7th contiguous subarray = {2 3 6} Max = 6
*/

import java.util.*;
class Solution
{
    //Function to find maximum of each subarray of size k.
    static ArrayList <Integer> max_of_subarrays(int arr[], int n, int k)
    {
       
         if(n==0 || k<=0 ||k>n){
            return new ArrayList<Integer>();
        }
        ArrayList <Integer> al=new ArrayList<Integer>();
        int j, max;

        for (int i = 0; i <= n-k; i++) {

            max = arr[i];

            for (j = 1; j < k; j++) {
                if (arr[i + j] > max)
                    max = arr[i + j];
            }
            al.add(max);
        }
        return al;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{1,2,3,1,4,5,2,3,6};
		Solution obj=new Solution();
		int k=3;		//(size of subarray)
		System.out.println(obj.max_of_subarrays(arr,arr.length,k));
	}
}

/*Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.
 
Example 1:

Input: nums = [3,2,3]
Output: [3]
*/

import java.util.*;
class Solution {
    public List<Integer> majorityElement(int[] nums) {

        Map<Integer, Integer> hashMap = new HashMap<>();
        List<Integer> res = new ArrayList<>();

        for(Integer ele : nums) {
            if(res.contains(ele)) continue;

            if(hashMap.containsKey(ele)) {
                hashMap.put(ele, hashMap.get(ele) + 1);
            } else {
                hashMap.put(ele, 1);
            }

            if(hashMap.get(ele) > nums.length/3 && !res.contains(ele)) {
                res.add(ele);
            }
        }

        return res;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{3,2,3};
		Solution obj=new Solution();
		System.out.println(obj.majorityElement(arr));
	}
}

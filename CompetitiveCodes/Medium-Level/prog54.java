/*
 * Given an array nums with n objects colored red, white, or blue, sort them in-place so that objects of the same color are adjacent, with the colors in the order red, white, and blue.

We will use the integers 0, 1, and 2 to represent the color red, white, and blue, respectively.

You must solve this problem without using the library's sort function.



Example 1:

Input: nums = [2,0,2,1,1,0]
Output: [0,0,1,1,2,2]
*/

import java.util.*;
class Solution {
    public void sortColors(int[] arr) {
        
        int j=0;
        int min=0;
        int max=arr.length-1;
        
        while(j<=max){
            if(arr[j]==0){
                swap(arr,j,min);
                j++;
                min++;
            }
            else if(arr[j]==2){
                swap(arr,j,max);
                max--;
                
            }else
                j++;
        }
    }
    
    void swap(int []arr,int i,int j){
        int temp=arr[i];
        arr[i]=arr[j];
        arr[j]=temp;
    }
        
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{2,0,2,1,1,0};
		Solution obj=new Solution();
		obj.sortColors(arr);
		System.out.println(Arrays.toString(arr));
	}
}


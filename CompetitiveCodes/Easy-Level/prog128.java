/*83. Remove Duplicates from Sorted List
Easy

Given the head of a sorted linked list, delete all duplicates such that each element appears only once. Return the linked list sorted as well.

 

Example 1:

Input: head = [1,1,2]
Output: [1,2]
*/

import java.util.*;
class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }

class Solution {
    public ListNode deleteDuplicates(ListNode head) {

        HashSet<Integer> hs = new HashSet<>();
        ListNode curr=head;
        ListNode prev=null;

        while(curr!=null){
            int data = curr.val;
            if (hs.contains(data))
                prev.next=curr.next;

            else{
                hs.add(data);
                prev=curr;
            }

            curr=curr.next;
        }

        return head;
    }
}

class User {
    public static void main(String[] args) {
        Solution solution = new Solution();

        // Create the linked list
        ListNode head = new ListNode(1);
        head.next = new ListNode(1);
        head.next.next = new ListNode(2);
        
        

        ListNode rev = solution.deleteDuplicates(head);

        //print
        while (rev != null) {
            System.out.print(rev.val + " ");
            rev = rev.next;
        }
    }
}


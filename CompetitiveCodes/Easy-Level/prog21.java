//Wave Array 
//Given a sorted array arr[] of distinct integers. Sort the array into a wave-like array(In Place).
//In other words, arrange the elements into a sequence such that arr[1] >= arr[2] <= arr[3] >= arr[4] <= arr[5].....

import java.util.Arrays;
class Solution {
    
    public static void swap(int arr[],int i,int j){
        
        int temp=arr[i];
        arr[i]=arr[j];
        arr[j]=temp;
        
    }
    public static void convertToWave(int n, int[] a) {
       
       for(int i=0;i<n-1;i=i+2){
           swap(a,i,i+1);
           
       }
    }
}
    
class Client{
        public static void main(String s[]){
                Solution obj=new Solution();

                int arr[]=new int[]{2,4,7,8,9,10};
                obj.convertToWave(6,arr);

		System.out.println(Arrays.toString(arr));
        }
}

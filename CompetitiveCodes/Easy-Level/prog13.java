// Next Greater Element
//The next greater element of soe elements x in an array is the first elemeny that is to the right of x in the same array
//num1=[4,1,2] 		num2=[1,3,4,2]
//output=[-1,3,-1]

import java.util.*;
class Solution {
	 static int[] leaders(int[] nums1, int[] nums2){
            ArrayList<Integer> al= new ArrayList<>();
            for(int i=0;i<nums2.length;i++){
                al.add(nums2[i]);
            }
            for(int i=0;i<nums1.length;i++){
                int flag=0;
                for(int j=al.indexOf(nums1[i]);j<nums2.length;j++){
                        if(nums2[j]>nums1[i]){
                                nums1[i]=nums2[j];
                                flag=1;
                                break;
                        }
                }
                if(flag==0){
                         nums1[i]=-1;
                }
            }
        return nums1;
        }
}
class Client{
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        
	System.out.println("Enter size of array");
        int size1=sc.nextInt();
        int[] nums1=new int[size1];
        
	System.out.println("Enter size of array 2");
	int size2=sc.nextInt();
	int[] nums2=new int[size2];
	
	System.out.println("Enter element");
        for(int i=0;i<size1;i++){
            nums1[i]=sc.nextInt();
        }

	System.out.println("Enter element for array 2");
	for(int i=0;i<size2;i++){
		nums2[i]=sc.nextInt();
	}

        Solution obj=new Solution();
        
	System.out.println("Output: ");
        int[] output=obj.leaders(nums1,nums2);
	System.out.print("[ ");
	for(int i=0;i<size1;i++){
		System.out.print(output[i]+" ");
        } 
	System.out.println("]");
        
	}
}




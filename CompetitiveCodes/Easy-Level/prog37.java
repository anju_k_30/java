//Given two arrays of A and B respectively of sizes N1 and N2, the task is to calculate the product of the maximum element of the first array and minimum element of the second array.

class Solution{

    // Function for finding maximum and value pair
    public static long find_multiplication (int arr[], int brr[], int n, int m) {

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;


        for(int i=0 ; i<arr.length ; i++){
            if(arr[i] > max){
                max = arr[i];
            }
        }
        for(int i=0 ; i<brr.length ; i++){
            if(brr[i] < min){
                min = brr[i];
            }
        }
        long product = max * min;
        return product;
    }


}

class User {
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr1[]=new int[]{5,7,9,3,6,2};
		int arr2[]=new int[]{1,2,6,-1,0,9};
		System.out.println(obj.find_multiplication(arr1,arr2,6,6));
	}
}

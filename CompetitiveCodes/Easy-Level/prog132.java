/*Given a linked list of 0s, 1s and 2s, sort it.

Given a linked list of N nodes where nodes can contain values 0s, 1s, and 2s only. The task is to segregate 0s, 1s, and 2s linked list such that all zeros segregate to head side, 2s at the end of the linked list, and 1s in the mid of 0s and 2s.

Example 1:

Input:
N = 8
value[] = {1,2,2,1,2,0,2,2}
Output: 0 1 1 2 2 2 2 2
Explanation: All the 0s are segregated
to the left end of the linked list,
2s to the right end of the list, and
1s in between.
*/
class Node {
   int data;
   Node next;

  Node(int data) {
      this.data = data;
  }
}


class Solution
{
    
    Node sort(Node head)
    {
         int[] count = new int[3]; // Initialize count of 0s, 1s, and 2s to 0

        // Count the occurrences of 0s, 1s, and 2s in the linked list
        Node current = head;
        while (current != null) {
            count[current.data]++;
            current = current.next;
        }

        // Update the linked list with the sorted values
        current = head;
        for (int i = 0; i < 3; i++) {
            while (count[i] > 0) {
                current.data = i;
                current = current.next;
                count[i]--;
            }
        }

        return head;
    }
}

class User {
    public static void main(String[] args) {
        Solution solution = new Solution();

        // Create the linked list
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(2);
        head.next.next.next = new Node(1);
        head.next.next.next.next = new Node(2);
        head.next.next.next.next.next = new Node(0);
        head.next.next.next.next.next.next = new Node(2);
        head.next.next.next.next.next.next.next = new Node(2);


        Node rev=solution.sort(head);

	 while (rev != null) {
            System.out.print(rev.data + " ");
            rev = rev.next;
        }


    }
}

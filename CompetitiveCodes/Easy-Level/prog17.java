//Remove Dupilcate from the sorted array.Return the count of unique elements.


class Solution {
    public int removeDuplicates(int[] nums) {
        
        int count=0;
        for(int i=0;i<nums.length;i++){
                    
            if(i<nums.length-1 && nums[i]==nums[i+1]){
                continue;
            }
            else{
                nums[count]=nums[i];
                count++;
            }
        }
        return count;
    }
}

class Client{
	public static void main(String s[]){
		Solution obj=new Solution();
		
		int arr[]=new int[]{1,1,2};
		System.out.println(obj.removeDuplicates(arr));
	}
}

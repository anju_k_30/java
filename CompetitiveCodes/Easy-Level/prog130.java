/*Delete nodes having greater value on right

Given a singly linked list, remove all the nodes in the list which have any node on their right whose value is greater. (Not just immediate Right , but entire List on the Right)

Example 1:

Input:
LinkedList = 12->15->10->11->5->6->2->3
Output: 15 11 6 3
Explanation: Since, 12, 10, 5 and 2 are
the elements which have greater elements
on the following nodes. So, after deleting
them, the linked list would like be 15,
11, 6, 3.*/

class Node {
   int data;
   Node next;

  Node(int data) {
      this.data = data;
  }
}

class Solution {
    Node compute(Node head) {
        if (head == null || head.next == null)
            return head;

        // Reverse the linked list
        head = reverse(head);

        Node current = head;
        Node maxNode = head;

        while (current != null && current.next != null) {
            if (current.next.data < maxNode.data) {
                current.next = current.next.next;
            } else {
                current = current.next;
                maxNode = current;
            }
        }

        // Reverse the linked list again to bring it back to original order
        return reverse(head);
    }

    Node reverse(Node head) {
        Node prev = null;
        Node current = head;
        Node next;

        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }

        return prev;
    }
}
class User {
    public static void main(String[] args) {
        Solution solution = new Solution();

        // Create the linked list
        Node head = new Node(12);
        head.next = new Node(15);
        head.next.next = new Node(10);
        head.next.next.next = new Node(11);
        head.next.next.next.next = new Node(5);
        head.next.next.next.next.next = new Node(6);
        head.next.next.next.next.next.next = new Node(2);
        head.next.next.next.next.next.next.next = new Node(3);



        Node rev = solution.compute(head);

        //print
        while (rev != null) {
            System.out.print(rev.data + " ");
            rev = rev.next;
        }
    }
}


//Given an array arr of distinct elements of size N, the task is to rearrange the elements of the array in a zig-zag fashion so that the converted array should be in the below form: 

//    arr[0] < arr[1]  > arr[2] < arr[3] > arr[4] < . . . . arr[n-2] < arr[n-1] > arr[n]. 


import java.util.Arrays;
class Solution{
    public void zigZag(int a[], int n){
        
        for(int i=0;i<n-1;i++){
            if(i%2==0){
                
                if(a[i]>a[i+1]){
                    int temp=a[i];
                    a[i]=a[i+1];
                    a[i+1]=temp;
                }
            }
            else if(a[i]<a[i+1]){
                int temp=a[i];
                a[i]=a[i+1];
                a[i+1]=temp;
            }
            
        }
            
    }
}
class Client{
        public static void main(String s[]){
                Solution obj=new Solution();

                int arr[]=new int[]{4,3,7,8,6,2,1};
                obj.zigZag(arr,6);

                System.out.println(Arrays.toString(arr));
        }
}


//Even and odd
//arrange the no in array such a way that even no gets even position and odd no gets odd position
//[3,6,12,1,5,8] expected=>[6,3,12,1,8,5]

import java.util.Arrays;

class Solution {
    static int[] reArrange(int[] arr, int N) {
        int num1=1;
        int num2=0;
        
        while(num2<arr.length){
            if(arr[num2]%2==0){
                num2=num2+2;
            }else if(arr[num1]%2==1){
                num1=num1+2;
            }
            else{
                int temp=arr[num1];
                arr[num1]=arr[num2];
                arr[num2]=temp;
            }
            
        }
	return arr;
    }
};
class Client{
	public static void main(String s[]){
		int arr[]=new int[]{3,6,12,1,5,8};
		Solution obj=new Solution();
		System.out.println(Arrays.toString(obj.reArrange(arr,6)));
	}
}

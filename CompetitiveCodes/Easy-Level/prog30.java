/*
 Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".*/


import java.util.Arrays;

class Solution {
    public String longestCommonPrefix(String[] strs) {
        // Check for empty input array
        if (strs == null || strs.length == 0) {
            return "";
        }

        Arrays.sort(strs);
        String s1 = strs[0];
        String s2 = strs[strs.length - 1];

        int index = 0;
        while (index < s1.length() && index < s2.length()) {
            if (s1.charAt(index) == s2.charAt(index)) {
                index++;
            } else {
                break;
            }
        }

        return index == 0 ? "" : s1.substring(0, index);
    }
}

class User{
	public static void main(String s[]){
		Solution obj=new Solution();
		String arr[]=new String[]{"flower","flow","flight"};
		System.out.println(obj.longestCommonPrefix(arr));
	}
}

//Find the smallest and second smallest element in an array.
//arr=[2,1,0,5,6]  =>[0,1]

import java.util.Arrays;
class Compute 
{
    public long[] minAnd2ndMin(long a[], long n)  
    {
       long min=Integer.MAX_VALUE;
       long temp=Integer.MAX_VALUE;
       for(int i=0;i<a.length;i++){
           if(a[i]<min){
               min=a[i];
           }  
           }
         for(int i=0;i<a.length;i++){
             if(a[i]<temp && a[i] != min)
                temp=a[i];
         }
       
    
    long arr[]=new long[2];
    if(temp != Integer.MAX_VALUE){
    arr[0]=min;
    arr[1]=temp;
    
    }else{
        arr[0]=-1;
    }
    return arr;
    }
}

class User {
	public static void main(String s[]){
		Compute obj=new Compute();
		long arr[]=new long[]{2,1,0,5,6};
		System.out.println(Arrays.toString(obj.minAnd2ndMin(arr,5)));
	}
}

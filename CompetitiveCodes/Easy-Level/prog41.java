//You are given an array a, of n elements. Find the minimum index based distance between two distinct elements of the array, x and y. Return -1, if either x or y does not exist in the array.

import java.io.*;
import java.util.*; 

/*class Solution {
    int minDist(int a[], int n, int x, int y) {
        int distance=0;
        
        for(int i=0;i<n;i++){
            for(int j=i+1;j<n;j++){
                if(a[i]==x && a[j]==y ||a[i]==y && a[j]==x){
                    return Math.abs(j - i);
                }
            }
        }
        
        return -1;
    }
}*/
class Solution {
    int minDist(int a[], int n, int x, int y) {
        int distance = Integer.MAX_VALUE;
        int xIndex = -1;
        int yIndex = -1;

        for (int i = 0; i < n; i++) {
            if (a[i] == x) {
                xIndex = i;
                if (yIndex != -1)
                    distance = Math.min(distance, Math.abs(yIndex - xIndex));
            } else if (a[i] == y) {
                yIndex = i;
                if (xIndex != -1)
                    distance = Math.min(distance, Math.abs(yIndex - xIndex));
            }
        }

        if (distance == Integer.MAX_VALUE) {
            // Either x or y is not present in the array
            return -1;
        }

        return distance;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{86,39,90,67,84,66,62};
		Solution obj=new Solution();
		System.out.println(obj.minDist(arr,7,86,66));
	}
}

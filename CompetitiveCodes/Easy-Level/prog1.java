//leetcode-136
//Given a non empty array of integers nums,every element appeares twice except for one.Find that single one.TC-O(N).
//arr=[2,2,1] output=1

class ArrayDemo{
	int Find(int arr[]){

		for(int i=0;i<arr.length;i++){
			
			boolean twice=false;

			for(int j=0;j<arr.length;j++){
				if(i!=j && arr[i]==arr[j]){
					twice=true;
					break;
				}
			}
			if(!twice)
				return arr[i];
			
		}
		return -1;
	}
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{2,2,1};
		
		ArrayDemo obj=new ArrayDemo();
		System.out.println(obj.Find(arr));
	}
}

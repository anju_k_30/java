//Count pairs with given sum
//arr=[1,5,7,1] =>2

import java.util.*;
class Solution {
    int getPairsCount(int[] arr, int n, int sum) {
        int count=0;
        Map<Integer,Integer>map=new HashMap<>();

        for(int i=0;i<arr.length;i++){
            int target=sum-arr[i];

            if(map.containsKey(target)){
                count=count+map.get(target);
            }
            map.put(arr[i],map.getOrDefault(arr[i],0)+1);

        }
        return count;
    }
}
class User{
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{1,5,7,1};

		System.out.println(obj.getPairsCount(arr,arr.length,6));
	}
}

// Alternate positive and negative number


import java.io.*;
import java.util.*;

class Solution {
    void rearrange(int arr[], int n) {
	   ArrayList<Integer> al1 = new ArrayList<Integer>();
        ArrayList<Integer> al2 = new ArrayList<Integer>();
        
        for(int i=0 ; i<arr.length ; i++){
            if(arr[i] >= 0){
                al1.add(arr[i]);
            }
            else{
                al2.add(arr[i]);
            }
        }
        int i=0;
        int j=0;
        int k=0;
        
        while(i<al1.size() && j<al2.size()){
            arr[k++] = al1.get(i++);
            arr[k++] = al2.get(j++);
        }
        while(i < al1.size()){
            arr[k++] = al1.get(i++);
        }
        while(j < al2.size()){
            arr[k++] = al2.get(j++);
        }
    }

}
class Demo{
        public static void main(String args[])throws IOException{

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int nums[]=new int[size];

        System.out.println("Enter Element :");
        for(int i=0;i<size;i++){
            nums[i]=Integer.parseInt(br.readLine());
        }

        Solution obj=new Solution();
        obj.rearrange(nums,size);

        }
}





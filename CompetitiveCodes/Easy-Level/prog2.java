//First Repeating Element
// Function to return the position of the first repeating element.
// arr=[1,5,3,4,3,5,6]
// output=2(5 is repeated first)

//Gives TLE
class ArrayDemo{
	int repeat(int[] arr, int n) {
        for(int i=0;i<arr.length;i++){
            for (int j = i + 1; j < n; j++) {
                if (arr[i] == arr[j]) {
                    return i+1;
                }
            }
        }
    	return -1;   
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{1,5,3,4,3,5,6};
		ArrayDemo obj=new ArrayDemo();
		//obj.repeat(arr,7);
		System.out.println(obj.repeat(arr,7));
	}
}

//Leader in an array
//leader is the greater than or equal to all the elements to its right side.Rightmost element is always greater.
//arr[16,17,4,3,5,2] =>[17,5,2]

import java.util.*;

class Solution{
    //Function to find the leaders in the array.
    static ArrayList<Integer> leaders(int arr[], int n){

        ArrayList<Integer> obj=new ArrayList<>();

        int max=-1;

        for(int i=n-1;i>=0;i--){
            if(max<=arr[i]){
                max=arr[i];
                obj.add(max);
            }
        }

        Collections.reverse(obj);
        return obj;
    }
}

class User{
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{16,17,4,3,5,2};
		System.out.println(obj.leaders(arr,arr.length));
	}
}


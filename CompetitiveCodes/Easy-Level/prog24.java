//Element with left side smaller and right side greater

import java.io.*;
import java.util.*;

class Solution{
    public int findElement(int arr[], int n){
        for(int i=1;i<arr.length-1;i++){
            int num = arr[i];
            int j=0; int k=arr.length-1;int flag=0;int hlag=0;
            while(j<i){
             if(arr[j]>arr[i]){
                 flag=1;
                 break;
             }
             j++;
            }
            while(k>i){
                if(arr[k]<arr[i]){
                    hlag=1;
                    break;
                }
                k--;
            }
            if(flag==0&&hlag==0){
                return arr[i];
            }
        }
        return -1;
       / 
    } 
}
class Demo{
        public static void main(String args[])throws IOException{

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int nums[]=new int[size];

        System.out.println("Enter Element :");
        for(int i=0;i<size;i++){
            nums[i]=Integer.parseInt(br.readLine());
        }

        Solution obj=new Solution();
        int num= obj.findElement(nums,size);

        System.out.println(num);

        }
    }

//Given a sorted array with possibly duplicate elements. The task is to find indexes of first and last occurrences of an element X in the given array.
//Note: If the element is not present in the array return {-1,-1} as pair.

import java.util.*;
class pair {
    long first;
    long second;

    public pair(long first, long second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "(" + first + ", " + second + ")";
    }
}

class Solution {

    public pair indexes(long arr[], long x)
    {
        long first = -1;
        long second = -1;

        for(int i=0 ; i<arr.length ; i++){
            if(arr[i] == x){
                first = i;
                break;
            }
        }

        for(int i=arr.length-1 ; i>=0 ; i--){
            if(arr[i] == x){
                second = i;
                break;
            }
        }
        return new pair(first,second);
    }
}

class User {
	public static void main(String s[]){
		Solution obj=new Solution();
		long arr[]=new long[]{1,3,5,5,5,5,67,123,125};
		System.out.println(obj.indexes(arr,5));
	}
}

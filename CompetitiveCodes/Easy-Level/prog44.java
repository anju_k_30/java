/*
 * Given an unsorted array Arr of size N. Find the subarray Arr[s...e] such that sorting this subarray makes the whole array sorted.

Example 1:

Input:
N = 11
Arr[] ={10,12,20,30,25,40,32,31,35,50,60}
Output: 3 8
Explanation: Subarray starting from index 3 and ending at index 8 is required subarray.
Initial array: 10 12 20 30 25 40 32 31 35 50 60 
Final array: 10 12 20 25 30 31 32 35 40 50 60

*/
import java.util.*;
class Solution {
    int[] printUnsorted(int[] arr1, int n) {
       
                int arr2[]=new int[arr1.length];

                for(int i=0;i<arr2.length;i++){
                        arr2[i]=arr1[i];
                }
                Arrays.sort(arr2);

                int arr3[]=new int[2];

                int flag=0;
                for(int i=0;i<arr1.length;i++){
                        if(arr2[i]!=arr1[i] && flag==0){
                                arr3[0]=i;
                                flag=1;
                        }
                        if(arr2[i]!=arr1[i] && flag==1){
                                arr3[1]=i;
                        }
                }
      

                return arr3;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{10,12,20,30,25,40,32,31,35,50,60};
		Solution obj=new Solution();
		
		System.out.println(Arrays.toString(obj.printUnsorted(arr,11)));
	}
}

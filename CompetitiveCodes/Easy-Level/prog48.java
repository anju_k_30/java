/*Write a program to find the transpose of a square matrix of size N*N. Transpose of a matrix is obtained by changing rows to columns and columns to rows.

Example 1:

Input:
N = 4
mat[][] = {{1, 1, 1, 1},
           {2, 2, 2, 2}
           {3, 3, 3, 3}
           {4, 4, 4, 4}}
Output: 
{{1, 2, 3, 4},  
 {1, 2, 3, 4}  
 {1, 2, 3, 4}
 {1, 2, 3, 4}} 
*/

class Solution
{
    public void transpose(int n,int arr[][])
    {
        for(int i=0 ; i<arr.length ; i++){
            for(int j=i+1 ; j<arr.length ; j++){
                int temp = arr[i][j];
                arr[i][j] = arr[j][i];
                arr[j][i] = temp;
            }
        }
    }
}


class User{
	public static void main(String s[]){
		int arr[][]=new int[][]{{1,1,1,1},{2,2,2,2},{3,3,3,3},{4,4,4,4}};
		Solution obj=new Solution();
		obj.transpose(4,arr);
		
		for (int i = 0; i < arr.length; i++) {
            		for (int j = 0; j < arr[i].length; j++) {
               			 System.out.print(arr[i][j] + " ");
            		}
            	System.out.println();
        	}
	}
}

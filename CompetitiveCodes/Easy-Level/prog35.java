/*
Given two arrays: a1[0..n-1] of size n and a2[0..m-1] of size m. Task is to check whether a2[] is a subset of a1[] or not. Both the arrays can be sorted or unsorted. There can be duplicate elements.*/

import java.util.*;
class Compute {
    public String isSubset( long a1[], long a2[], long n, long m) {

        HashMap<Long,Integer> map = new HashMap<>();


        for(int i=0 ; i<a1.length ; i++){
         if(map.containsKey(a1[i])){
             map.put(a1[i],map.get(a1[i])+1);
         }
         else{
             map.put(a1[i],1);
         }
        }
         for(int i=0 ; i<a2.length ; i++){
            if(map.containsKey(a2[i])) {
                if(map.get(a2[i]) == 1){
                    map.remove(a2[i]);
                }
                else{
                    map.put(a2[i],map.get(a2[i])-1);
                }
            }
            else{
                return "No";
            }
         }
         return "Yes";
    }
}

class User {
	public static void main(String s[]){
		Compute obj=new Compute();
		long arr1[]=new long[]{11,7,1,13,21,3,7,3};
		long arr2[]=new long[]{11,3,7,1,7};
		System.out.println(obj.isSubset(arr1,arr2,8,5));
		
	}
}

//Two-Sum=>Return indices of the two numbers such that they add up to target
//arr=[2,3,6,4] sum=5   =>[0,1]

import java.util.Arrays;
class Solution {
    public int[] twoSum(int[] arr, int target) {
        int[] values=new int[2];

        for(int i=0;i<arr.length;i++){
            for(int j=i+1;j<arr.length;j++){
                if(arr[i]+arr[j]==target){
                    values[0]=i;
                    values[1]=j;
                    return values;
                }
            }
        }
        return values;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{2,3,6,4};
		Solution obj=new Solution();
		System.out.println(Arrays.toString(obj.twoSum(arr,5)));
	}
}
	

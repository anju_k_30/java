//Key-Pair=>Determine whether or not there exists two elements in array whose sum is exactly x
//arr =[1,5,3,4,5,6] and x=6 output=>true

import java.util.Arrays;

class ArrayDemo {
    boolean Sum(int arr[], int n, int x) {
        Arrays.sort(arr);

        int left = 0;
        int right = arr.length - 1;
        int currentSum;
        
        while (left < right) {
            currentSum = arr[left] + arr[right];

            if (currentSum == x) {
                return true; 
            } else if (currentSum < x) {
                left++;
            } else {
                right--;
            }
        }

        return false; 
    }
}
class User{
	public static void main(String s[]){

		int arr[]=new int[]{1,5,3,4,3,5,6};
		ArrayDemo obj=new ArrayDemo();
		System.out.println(obj.Sum(arr,7,6));

	}
}

/*Given are the heights of certain Buildings which lie adjacent to each other. Sunlight starts falling from the left side of the buildings. If there is a building of a certain Height, all the buildings to the right side of it having lesser heights cannot see the sun. The task is to find the total number of such buildings that receive sunlight.*/

class Solution {

    public static int longest(int arr[],int n)
    {
        int count=0;
        int max=Integer.MIN_VALUE;
        
        for(int i=0;i<n;i++){
            if(arr[i]>=max){
                max=arr[i];
                count++;
            }
        }
        return count;
    }
}
class User{
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{6,2,8,4,11,13};
		System.out.println(obj.longest(arr,6));			//4(6,8,11,13)

	}
}

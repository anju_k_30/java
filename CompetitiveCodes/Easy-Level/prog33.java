//Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
//Return the running sum of nums.

import java.util.Arrays;
class Solution {
    public int[] runningSum(int[] arr) {
      for(int i=1 ; i<arr.length ; i++){

		arr[i] = arr[i-1 ]+arr[i];

	}
     	return arr;

       }
}
class User {
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{1,2,3,4};
		System.out.println(Arrays.toString(obj.runningSum(arr)));
	}
}

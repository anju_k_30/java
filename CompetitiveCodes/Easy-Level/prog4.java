//Finding Single element from an array where every element appears twice except one,find it.
//arr=[2,2,1] output ==>1

class ArrayDemo {
    public int Find(int[] arr) {
        int find=0;
        for(int i=0;i<arr.length;i++){
            find=find^arr[i];
        }
        return find;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{2,2,1};
		
		ArrayDemo obj=new ArrayDemo();
		System.out.println(obj.Find(arr));
	}
}

/*Given an array nums[] of size n, construct a Product Array P (of same size n) such that P[i] is equal to the product of all the elements of nums except nums[i].

*/
import java.util.Arrays;
class Solution
{
	public static long[] productExceptSelf(int nums[], int n)
	{
        long arr1[]=new long[n];
        long arr2[]=new long[n];

        arr1[0]=1;
        arr2[n-1]=1;

        for(int i=1;i<n;i++){
            arr1[i]=arr1[i-1]*nums[i-1];
        }

        for(int i=n-2;i>=0;i--){
            arr2[i]=arr2[i+1]*nums[i+1];
        }

        long[] result = new long[n];

        for (int i = 0; i < n; i++) {
            result[i] = arr1[i] * arr2[i];
        }
        return result;
	}
}
class User{
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{10,3,5,6,2};
		long arr1[]=new long[arr.length];
		arr1=obj.productExceptSelf(arr,5);
		System.out.println(Arrays.toString(arr1));
	}
}

//Given an array nums containing n distinct numbers in the range [0, n], return the only number in the range that is missing from the array.

class Solution {
    public int missingNumber(int[] nums) {

        int sum=0;

        for(int i=0;i<nums.length;i++){
            sum=sum+nums[i];
        }

        int actualsum = (nums.length * (nums.length + 1)) / 2;

        int val = actualsum - sum;
        return val;
    }
}
class User{
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{3,0,1};
		System.out.println(obj.missingNumber(arr));
	}
}


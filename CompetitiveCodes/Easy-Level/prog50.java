/*
 * banner

Given a sorted array of size N and an integer K, find the position(0-based indexing) at which K is present in the array using binary search.

Example 1:

Input:
N = 5
arr[] = {1 2 3 4 5}
K = 4
Output: 3
Explanation: 4 appears at index 3.*/

class Solution {
    int binarysearch(int arr[], int n, int k) {

        int start = 0;
        int end = arr.length-1;

        while(start <= end){
            int mid = (start + end)/2;
            if(arr[mid] == k){
                return mid;
            }
            if(arr[mid] < k){
                start = mid + 1;
            }
            if(arr[mid] > k){
                end = mid - 1;
            }
        }
        return -1;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{1,2,3,4,5};
		Solution obj=new Solution();
		System.out.println(obj.binarysearch(arr,5,4));
	}
}

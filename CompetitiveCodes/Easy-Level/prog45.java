/*
 * Given an array of integers (A[])  and a number x, find the smallest subarray with sum greater than the given value. If such a subarray do not exist return 0 in that case.

Example 1:

Input:
A[] = {1, 4, 45, 6, 0, 19}
x  =  51
Output: 3
Explanation:
Minimum length subarray is
{4, 45, 6}
*/

import java.util.*;
class Solution {

    public static int smallestSubWithSum(int arr[], int n, int x) {
	
	 int start =0,end=1;
	 int sum = arr[start];
	 int result = Integer.MAX_VALUE;

	 if(sum>x)
        	return 1;

    	if(end<n)
        	sum +=arr[end];

	while(start<n && end <n){

        	if(sum>x){
           		 result = Math.min(result,end-start+1);
           		 sum = sum-arr[start];
			 start++;
      		  }else{
            		end++;
            		if(end<n){
                		sum = sum + arr[end];
            		}
        	}
    	}	

    	if(result == Integer.MAX_VALUE){
        	return 0;
    	}
    	return result;
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{1, 4, 45, 6, 0, 19};
		Solution obj=new Solution();
		System.out.println(obj.smallestSubWithSum(arr,6,51));
	}
}


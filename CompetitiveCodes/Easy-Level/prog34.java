/*You are given a 0-indexed 1-dimensional (1D) integer array original, and two integers, m and n. You are tasked with creating a 2-dimensional (2D) array with m rows and n columns using all the elements from original.

The elements from indices 0 to n - 1 (inclusive) of original should form the first row of the constructed 2D array, the elements from indices n to 2 * n - 1 (inclusive) should form the second row of the constructed 2D array, and so on.

Return an m x n 2D array constructed according to the above procedure, or an empty 2D array if it is impossible.*/

import java.util.Arrays;
class Solution {
    public int[][] construct2DArray(int[] original, int m, int n) {

        if(original.length != n*m){
            return new int [0][0];
        }
        int arr[][] = new int[m][n];
        
        for(int i=0 ; i<original.length ; i++){
            int temp1 = i/n;
            int temp2 = i%n;
            arr[temp1][temp2] = original[i];

        }
        return arr;
    }
}
class User {
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{1,2,3,4};
		System.out.println(Arrays.deepToString(obj.construct2DArray(arr, 2, 2)));
	}
}

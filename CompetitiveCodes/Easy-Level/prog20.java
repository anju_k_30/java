//check if array is sorted and rotated.(1750)

class Solution {
    public boolean check(int[] nums) {

        int count=0;

        for(int i=0;i<nums.length;i++){
            if(nums[i]>nums[(i+1)%nums.length]){
                count++;
            }
            if(count>1){
                return false;
            }
        }
        return true;
    }
}

class Client{
	public static void main(String s[]){
		Solution obj=new Solution();

                int arr[]=new int[]{3,4,5,1,2};
                System.out.println(obj.check(arr));
        }
}

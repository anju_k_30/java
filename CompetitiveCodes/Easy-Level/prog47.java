/*
 * banner

Given an Integer N and a list arr. Sort the array using bubble sort algorithm.
Example 1:

Input:
N = 5
arr[] = {4, 1, 3, 9, 7}
Output:
1 3 4 7 9
*/

import java.util.*;
class Solution
{
	public static void bubbleSort(int arr[], int n)
    {
       boolean swapped;
       for(int i=0 ; i<arr.length ; i++){
           swapped = false;
           for(int j=0 ; j<arr.length-i-1 ; j++){
               if(arr[j] > arr[j+1]){
                   int temp = arr[j];
                   arr[j] = arr[j+1];
                   arr[j+1] = temp;
                   swapped = true;
               }
           }
           if(swapped == false){
               break;
           }
        }
    }
}

class User{
	public static void main(String s[]){
		int arr[]=new int[]{4,1,3,9,7};
		Solution obj=new Solution();
		obj.bubbleSort(arr,5);
		System.out.println(Arrays.toString(arr));
	}
}

/*Remove duplicate elements from sorted Array
EasyAccuracy: 38.18%Submissions: 215K+Points: 2

Internship Alert
New month-> Fresh Chance to top the leaderboard and get SDE Internship! 
banner

Given a sorted array a[] of size n, delete all the duplicated elements from a[] & modify the array such that only distinct elements should be present there.

Note:
1. Don't use set or HashMap to solve the problem.
2. You must return the modified array size where only distinct elements are present in the array, the driver code will print all the elements of the modified array.

Example 1:

Input:
N = 5
Array = {2, 2, 2, 2, 2}
Output: 
1
Explanation: After removing all the duplicates only one instance of 2 will remain i.e. {2} so modify array will contains 2 at first position and you should return 1 after modify the array.
*/

class Solution {
    int remove_duplicate(int arr[],int N){
        int i = 0;
        int j = 1;
        while(j < N){
            if(arr[i] != arr[j]){
                i++;
                arr[i] = arr[j];
            }
            j++;
        }
        return i+1;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{2,2,2,2,2};
		Solution obj=new Solution();
		System.out.println(obj.remove_duplicate(arr,5));
	}
}


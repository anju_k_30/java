/*Maximize sum(arr[i]*i) of an Array
EasyAccuracy: 18.93%Submissions: 208K+Points: 2

Internship Alert
New month-> Fresh Chance to top the leaderboard and get SDE Internship! 
banner

Given an array A of N integers. Your task is to write a program to find the maximum value of ∑arr[i]*i, where i = 0, 1, 2,., n 1.
You are allowed to rearrange the elements of the array.
Note: Since output could be large, hence module 109+7 and then print answer.

 

Example 1:

Input : Arr[] = {5, 3, 2, 4, 1}
Output : 40
Explanation:
If we arrange the array as 1 2 3 4 5 then 
we can see that the minimum index will multiply
with minimum number and maximum index will 
multiply with maximum number. 
So 1*0+2*1+3*2+4*3+5*4=0+2+6+12+20 = 40 mod(109+7) = 40

*/

import java.util.*;
class Solution{

    int Maximize(int arr[], int n)
    {

        Arrays.sort(arr);

        long result = 0;
        for (int i = 0; i < arr.length; i++) {
            result = (result + (long)arr[i] * i) % 1000000007;
        }

        return (int) result;
    }

}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{5,3,2,4,1};

		Solution obj=new Solution();

		System.out.println(obj.Maximize(arr,arr.length));
	}
}

//Approach2
////You are given an array a, of n elements. Find the minimum index based distance between two distinct elements of the array, x and y. Return -1, if either x or y does not exist in the array.


class Solution {
    int minDist(int a[], int n, int x, int y) {

        int min = Integer.MAX_VALUE;
        int count = 0;
        int flag = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] == x) {

                for (int j = 0; j < n; j++) {

                    if (a[j] == y) {
                        flag = 1;

                        if(i>j){
                        count = i - j;
                        }
                        else{
                            count=j-i;
                        }
                        if (count < min) {
                            min = count;
                        }
                    }

                }
            }
        }
        if (flag == 1) {
            return min;
        } else {

            return -1;
        }
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{86,39,90,67,84,66,62};
		Solution obj=new Solution();
		System.out.println(obj.minDist(arr,7,86,66));
	}
}

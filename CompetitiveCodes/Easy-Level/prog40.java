/*
 * Given a 0-indexed integer array nums, return the number of distinct quadruplets (a, b, c, d) such that:

    nums[a] + nums[b] + nums[c] == nums[d], and
    a < b < c < d
*/

class Solution {
    public int countQuadruplets(int[] nums) {

        int count=0;

        for(int i=0;i<nums.length-3;i++){
            for(int j=i+1;j<nums.length-2;j++){
                for(int k=j+1;k<nums.length-1;k++){
                    for(int l=k+1;l<nums.length;l++){
                        if(nums[i]+nums[j]+nums[k]==nums[l]){
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{1,2,3,6};
		Solution obj=new Solution();
		System.out.println(obj.countQuadruplets(arr));
	}
}


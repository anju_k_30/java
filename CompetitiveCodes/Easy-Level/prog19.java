//Chocolate Distribution problem:

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

class Solution
{
    public long findMinDiff (ArrayList<Integer> a, int n, int m)
    {
        Collections.sort(a);
        
        long min=Long.MAX_VALUE;
    
        for(int i=0; i+m-1<n ;i++){
            
            int j=i+m-1;
            
            if(a.get(j)-a.get(i)<min){
                min=a.get(j)-a.get(i);
            }
        }
        return min;
    }
}

class Client{
        public static void main(String s[]){
                Solution obj=new Solution();

 		ArrayList<Integer> arr = new ArrayList<>(Arrays.asList(3, 4, 1, 9, 56, 7, 9, 12));
                System.out.println(obj.findMinDiff(arr,8,5));
        }
}


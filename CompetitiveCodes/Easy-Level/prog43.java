/*
 * Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must be unique and you may return the result in any order.

Example 1:

Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2]
*/

import java.util.*;
class Solution {
    public int[] intersection(int[] arr1, int[] arr2) {
        
                ArrayList<Integer> al=new ArrayList<>();
                ArrayList<Integer> al2=new ArrayList<>();

                for(int i=0;i<arr1.length;i++){
                        al.add(arr1[i]);
                }
        
                int count=0;
                for(int i=0;i<arr2.length;i++){
                        if(al.contains(arr2[i]) && !al2.contains(arr2[i])){
                                al2.add(arr2[i]);
                                count++;
                        }
                }
        
                int arr3[]=new int[count];
                for(int i=0;i<arr3.length;i++){
                        arr3[i]=al2.get(i);
                }

                return arr3;
    }
}
class User{
	public static void main(String s[]){
		int arr1[]=new int[]{1,2,2,1};
		int arr2[]=new int[]{2,2};

		Solution obj=new Solution();
		System.out.println(Arrays.toString(obj.intersection(arr1,arr2)));
	}
}

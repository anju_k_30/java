//Three Great Candidates/Max Product of 3 Numbers.

import java.util.Arrays;

class Solution {
    public int maximumProduct(int[] nums) {
        Arrays.sort(nums);

        int n = nums.length;

        int option1 = nums[n - 1] * nums[n - 2] * nums[n - 3];
        int option2 = nums[0] * nums[1] * nums[n - 1];

        return Math.max(option1, option2);
    }
}

class Client{
        public static void main(String s[]){
                Solution obj=new Solution();

                int arr[]=new int[]{1,2,3};
                System.out.println(obj.maximumProduct(arr));
        }
}


//Approach2:/*Given an array nums[] of size n, construct a Product Array P (of same size n) such that P[i] is equal to the product of all the elements of nums except nums[i].

import java.util.Arrays;
class Solution{
	int []productArrays(int arr[],int n){
		int product=1;

		for(int i=0;i<arr.length;i++){
			product=product*arr[i];
		}

		for(int i=0;i<arr.length;i++){
			arr[i]=(arr[i]!=0)?(product/arr[i]):0;
		}
		return arr;
	}
}
class User{
        public static void main(String s[]){
                Solution obj=new Solution();
                int arr[]=new int[]{10,3,5,6,2};
                System.out.println(Arrays.toString(obj.productArrays(arr,5)));
        }
}



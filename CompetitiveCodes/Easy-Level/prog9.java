//Majority Element
//It is an element that apperas more than n/2 times.
//arr=[2,2,3]  =>2

class Solution {
    public int majorityElement(int[] nums) {
        int count=0;
        int value=0;

        for(int i=0;i<nums.length;i++){
            if(count==0){
                value=nums[i];
            }
            if(value==nums[i]){
                count++;
            }
            else{
                count--;
            }
        }
        return value;
    }
}

class User{
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{2,2,3};
		System.out.println(obj.majorityElement(arr));
	}
}

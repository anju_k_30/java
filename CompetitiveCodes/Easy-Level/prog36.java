//Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.
//Note that you must do this in-place without making a copy of the array.

import java.util.Arrays;
class Solution {
    public void moveZeroes(int[] arr) {

        int j = 0;
        for(int i=0 ; i<arr.length ; i++){
            if(arr[i] != 0){
                arr[j] = arr[i];
                j++;
            }
 
        }
        for(int i=j ; i<arr.length ; i++){
            arr[i] = 0;
        }
    }
}

class User {
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{0,1,0,3,12};
		obj.moveZeroes(arr);
		System.out.println(Arrays.toString(arr));
	}
}

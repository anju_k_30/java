//(88).You are given two integer arrays nums1 and nums2,sorted in non-descending order, and two integers m and n,representing the no. of elements in nums1 and num2 resp. merge nums1 and nums2 into a single array sorted in non-descending order.
//The final sorted array should not be returned by the function,but instead be stored inside the array num1.To accommodate this,nums1 has a length of m+n,where the first m elements denote the elements that should be merged,and the last n elements are set to 0 and should be ignores.nums2 has a length of n.

import java.io.*;
class Sorting{
	void merge(int nums1[],int nums2[],int m,int n){
		int i=m-1;
		int j=n-1;

		int k=m+n-1;
		
		while(i>=0 && j>=0){
			if(nums1[i]>nums2[j]){
				nums1[k--]=nums1[i--];
			}else
				nums1[k--]=nums2[j--];
		}
		
		while(j>=0){
			nums1[k--]=nums2[j--];
		}

	}
}

class Client{
	public static void main(String s[])throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	
	System.out.println("Enter Array1 Size");
	int size1=Integer.parseInt(br.readLine());

	System.out.println("Enter Array2 Size");
	int size2=Integer.parseInt(br.readLine());
	
	System.out.println("Enter sorted elements in arr1");
	int arr1[]=new int[size1+size2];
	int arr2[]=new int[size2];

	for(int i=0;i<size1;i++)
		arr1[i]=Integer.parseInt(br.readLine());

	
	System.out.println("Enter sorted elements in arr2");

	for(int i=0;i<arr2.length;i++)
		arr2[i]=Integer.parseInt(br.readLine());

	
	Sorting obj=new Sorting();
	obj.merge(arr1,arr2,size1,size2);

	System.out.print("arr1:[");
	for(int i=0;i<arr1.length;i++)
		System.out.print(arr1[i]+" ");
	
	System.out.print("]\n");
	}
}

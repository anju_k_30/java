//Remove Element
//Remove all the occurances of val(given by user)in array in-place.Return no. of elements which are not equal to val
//arr =[3,2,2,3] and val=2   output=2

class Solution {
    public int removeElement(int[] nums, int val) {
        int itr=0;

         for(int i=0;i<nums.length;i++){
             if(nums[i]!=val){
                 nums[itr++]=nums[i];
              }
         }
        return itr;
    }
}
class User{
	public static void main(String s[]){
		int arr[]=new int[]{0,1,2,2,3,0,4,2};
		Solution obj=new Solution();
		System.out.println(obj.removeElement(arr,2));
	}
}

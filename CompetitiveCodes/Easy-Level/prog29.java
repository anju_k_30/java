/*
 * Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.*/

import java.util.Arrays;
class Solution {
    public int[] sortedSquares(int[] nums) {
        for(int i=0;i<nums.length;i++){
            nums[i]=nums[i]*nums[i];
        }

        Arrays.sort(nums);
        return nums;
    }
}
class User{
	public static void main(String s[]){
		Solution obj=new Solution();
		int arr[]=new int[]{-4,-1,0,3,10};
		System.out.println(Arrays.toString(obj.sortedSquares(arr)));
	}
}

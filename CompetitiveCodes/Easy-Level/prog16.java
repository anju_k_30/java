//Equilibrium Point
//Sum of elements to the left of pivot index is equal to sum of elements to the right of pivot element
//[1,7,3,6,5,6]		(1+7+3=11 and 5+6=11) return 1-based index

class Solution {
    public int pivotIndex(int[] arr) {
        
        int leftSum = 0;
        int rightSum = 0;
        int sum = 0;
        
        for(int i=0 ; i<arr.length ; i++){
            sum = sum + arr[i];
        }
    
        for(int i=0 ; i<arr.length ; i++){
            rightSum = sum - arr[i];
            if(leftSum == rightSum){
                return i+1;
            }
            leftSum = leftSum + arr[i];
            sum = sum - arr[i];
        }
        return -1;
    }
}
class Client{
	public static void main(String s[]){
		int arr[]=new int[]{1,7,3,6,5,6};
		Solution obj=new Solution();
		System.out.println(obj.pivotIndex(arr));
	}
}

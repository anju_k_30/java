//Max Consecutive ones.return the count 
//arr=>[1,1,0,1,1,1]   output:3


class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        int count=0;
        int maxcount=0;

        for(int i=0;i<nums.length;i++){
            if(nums[i]==1){
                count++;
                if(count>maxcount)
                    maxcount=count;
            }
            else{
                count=0;
            }
        }
        return maxcount;
    }
}

class Client{
	public static void main(String s[]){
		Solution obj=new Solution();

		int arr[]=new int[]{1,1,0,1,1,1};
		System.out.println(obj.findMaxConsecutiveOnes(arr));
	}
}

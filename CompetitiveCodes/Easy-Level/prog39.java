//You are given a large integer represented as an integer array digits, where each digits[i] is the ith digit of the integer. The digits are ordered from most significant to least significant in left-to-right order. The large integer does not contain any leading 0's.
//Increment the large integer by one and return the resulting array of digits.

import java.util.Arrays;
class Solution {
    public int[] plusOne(int[] digits) {

        int n = digits.length;
        int carry = 1;

        for (int i = n - 1; i >= 0; i--) {
            int sum = digits[i] + carry;
            digits[i] = sum % 10;
            carry = sum / 10;


            if (carry == 0) {
                break;
            }
        }


        if (carry > 0) {
            int[] result = new int[n + 1];
            result[0] = carry;
            System.arraycopy(digits, 0, result, 1, n);
            return result;
        }
        return digits;
    }
}

class User{
	public static void main(String s[]){
		//int arr[]=new int[]{1,2,3};
		int arr[]=new int[]{9,9,9};
		Solution obj=new Solution();
		System.out.println(Arrays.toString(obj.plusOne(arr)));
	}
}

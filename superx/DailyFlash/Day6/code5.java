//5.WAP to replace vowels to # in a given string

import java.io.*;
import java.util.*;
class OccuranceCount{
	static String Replace(String s){

		char arr[]=s.toCharArray();

		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'||arr[i]=='A'||arr[i]=='e'||arr[i]=='E'||arr[i]=='i'||arr[i]=='I'||arr[i]=='o'||arr[i]=='O'||arr[i]=='u'||arr[i]=='U')
				arr[i]='#';
		
		}
		
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		return Arrays.toString(arr);
		
	}
		public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter String");
		String word=br.readLine();
			
		System.out.println(Replace(word));

	}
}


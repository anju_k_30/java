//3.WAP to check whether the given no. is perfect or not.

import java.io.*;
class Num{
	static int Perfect(int num){
		int per=0;
		for(int i=1;i<=num/2;i++){
			if(num%i==0)
				per=per+i;
		}
		return per;
	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter no");
		int no=Integer.parseInt(br.readLine());

		if(Perfect(no)==no)
			System.out.println("Its Perfect no");
		else
			System.out.println("Its not Perfect no");

	}
}


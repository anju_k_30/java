//2.WAP to print the following pattern
//
//1
//7    26
//63   124  215
//342  511  728  999

import java.util.*;
class Pattern{
	static void pattern(int row){
		int x=1;
		for(int i=0;i<row;i++){
			for(int j=0;j<=i;j++){
				if(i==0)
					System.out.print(x++);
				else{
					System.out.print((x*x*x-1) +"	");
					x++;
				}
					
			}
			System.out.println();	
			
		}
		
	}

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

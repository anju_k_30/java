//4.WAP to check whether the given no. in a given range
//1-10==>1 2
//(sum of factorial of each digit==>145=1+24+120==145)

import java.io.*;
class Num{
	static void strong(int start,int end){

		for(int j=start;j<=end;j++){
			int x=j;
			int sum=0;
				while(x>0){
					int fact=1;
					int rem=x%10;
					for(int i=rem;i>=1;i--){
						fact=fact*i;
					}
					sum=sum+fact;
					x=x/10;
				}
			if(j==sum){
				System.out.print(j+"	");
			}
		}
	System.out.println();	
	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter start and end");
		int start=Integer.parseInt(br.readLine());
		int end=Integer.parseInt(br.readLine());

		strong(start,end);
	}
}


//1.WAP to print the following pattern

//A  B  C  D
//1  3  5  7
//A  B  C  D
//9  11 13 15
//A  B  C  D

import java.util.*;
class Pattern{
	static void pattern(int row,int col){
		
		int x=1;
		
		for(int i=0;i<row;i++){
			int ch=65;
			for(int j=0;j<col;j++){
				if(i%2!=0){
					System.out.print(x +"	");
					x=x+2;
				}
				else{	
					System.out.print((char)ch +"	");
					ch++;
				}
			}
			System.out.println();
		}
			
	}

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter no of rows and columns");

		int row=sc.nextInt();
		int col=sc.nextInt();
		pattern(row,col);
	}
}

//5.WAP to count the size of given string

import java.io.*;
class Count{
	static int count(String s){
		
		char arr[]=s.toCharArray();

		int count=0;
		for(int i=0;i<arr.length;i++){
			count++;
		}
		return count;
	}
		public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter word");
		String word=br.readLine();
		
		System.out.println(count(word));
	}
}


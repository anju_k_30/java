//1.WAP to print the following pattern
//
//1 2 3 4
//2 3 4 5
//3 4 5 6
//4 5 6 7

import java.util.*;
class Pattern{
	static void pattern(int row){
		
		int x=1;
		for(int i=0;i<row;i++){
			x=x+i;
			for(int j=0;j<row;j++){
				System.out.print(x++ +	"	");
			}
			System.out.println();
			x=1;
			
		}
		
	}

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

//4.WAP to print the odd nos in the given range .

import java.io.*;
class Even_Odd{
	static void Odd_No(int no1,int no2){
		
		for(int i=no1;i<=no2;i++){
			if(i%2!=0)
				System.out.println(i);
		}
	}

	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter start");
		int no1=Integer.parseInt(br.readLine());
		
		System.out.println("Enter end");
		int no2=Integer.parseInt(br.readLine());
		
		Odd_No(no1,no2);
	}
}


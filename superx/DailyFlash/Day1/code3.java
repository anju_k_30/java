//3.WAP to check whether the given no is even or odd

import java.io.*;
class Even_Odd{
	static int check(int no){
		if(no%2==0)
			return 0;
		else
			return -1;
	}

	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no");
		int no=Integer.parseInt(br.readLine());

		if((check(no))==0)
			System.out.println("Its Even no");
		else	
			System.out.println("Its Odd no");
	}
}


//2.WAP to print the following pattern
//
//1 
//1 2
//2 3 4
//4 5 6 7

import java.util.*;
class Pattern{
	static void pattern(int row){
		
		int x=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(x++ +	"	");
			}
			System.out.println();
			x=x-1;
			
		}
		
	}

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

//1.WAP to print the following pattern
//
//1  3  5  7
//2  4  6  8
//9  11 13 15
//10 12 14 16

import java.util.*;
class Pattern{
	static void pattern(int row){
		
		int x=1;
		int y=2;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2!=0){
					System.out.print(x +"	");
					x=x+2;
				}
				else{	
					System.out.print(y +"	");
					y=y+2;
				}
			}
			System.out.println();
		}
			
	}

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

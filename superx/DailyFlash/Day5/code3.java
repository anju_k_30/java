//3.WAP to print the factorial of digits in a given range.

import java.io.*;
class Num{
	static void Factorial(int start,int end){
		for(int i=start;i<=end;i++){
			int fact=1;
			for(int j=i;j>1;j--){
				fact=fact*j;
			}
			System.out.println(fact);
		}
	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter start");
		int start=Integer.parseInt(br.readLine());
		System.out.println("Enter end");
		int end=Integer.parseInt(br.readLine());

		Factorial(start,end);
	}
}


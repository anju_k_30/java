//2.WAP to print the following pattern
//
//a
//A B
//a b c
//A B C D

import java.util.*;
class Pattern{
	static void pattern(int row){
		int x=65;
		for(int i=0;i<row;i++){
			x=65;
			for(int j=0;j<=i;j++){
				if(i%2==0){
					System.out.print((char)(x+32) +	"	");
					x++;
				}else{	
					System.out.print((char)x +	" 	");
					x++;
				}
			}
			System.out.println();	
			
		}
		
	}

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

//4.WAP to check whether the given no. is a strong no. or not

import java.io.*;
class Num{
	static int strong(int no){
		int x=no;
		int sum=0;
		while(x>0){
			int fact=1;
			int rem=x%10;
			for(int i=rem;i>1;i--){
				fact=fact*i;
			}
			sum=sum+fact;
			x=x/10;
		}
		return sum;	
	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter no");
		int no=Integer.parseInt(br.readLine());

		if(strong(no)==no)
			System.out.println("Its Strong no");
		else
			System.out.println("Its not Strong no");
	}
}


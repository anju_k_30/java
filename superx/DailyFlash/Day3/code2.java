//2.WAP to print the following pattern
//
//1 
//2 1
//3 2 1
//4 3 2 1

import java.util.*;
class Pattern{
	static void pattern(int row){
		
		for(int i=1;i<=row;i++){
			int x=i;
			for(int j=1;j<=i;j++){
				System.out.print(x-- +	"	");
			}
			System.out.println();	
			
		}
		
	}

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

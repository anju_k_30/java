//4.WAP to print each reverse no in the givein range

import java.io.*;
class Num{
	static void Reverse(int start,int end){
		
	
		for(int j=start;j<=end;j++){
			int i=j;
			int rev=0;
			while(i!=0){
				int rem=i%10;
				rev=rev*10+rem;
				i=i/10;
			}
			System.out.println(rev);
		}
	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter start");
		int start=Integer.parseInt(br.readLine());
		
		System.out.println("Enter end");
		int end=Integer.parseInt(br.readLine());

		Reverse(start,end);
	}
}


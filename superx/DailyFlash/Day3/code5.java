//5.WAP to check whether the string contains characters other than letters

import java.io.*;
class Check{
	static int Letters(String s){
		
		char arr[]=s.toCharArray();

		for(int i=0;i<arr.length;i++){
			if(!((arr[i]>= 'a' && arr[i] <= 'z') || (arr[i] >= 'A' && arr[i] <= 'Z'))) 
				return 0;
		}
		return 1;
	}
		public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter word");
		String word=br.readLine();
		
		if((Letters(word))==0)
			System.out.println("It contains letter");
		else
			System.out.println("It do not contain letter");

	}
}


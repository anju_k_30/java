//3.WAP to check whether the given no is palindrome no. or not

import java.io.*;
class Num{
	static int check(int no){
		int num=no;
		int rev=0;
		while(num>0){
			int rem=num%10;
			rev=rev*10+rem;
			num=num/10;
		}
		if(rev==no)
			return 0;
		else 
			return -1;

	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no");
		int no=Integer.parseInt(br.readLine());

		if((check(no)==0))
			System.out.println("Its Palindrome no");
		else	
			System.out.println("Its not palindrome no");
	}
}


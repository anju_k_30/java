//1.WAP to print the following pattern
//
//A B C D
//D C B A
//A B C D
//D C B A

import java.util.*;
class Pattern{
	static void pattern(int row){
		
		int x=65;
		for(int i=0;i<row;i++){
			if(i%2==0){
				x=65;	
				for(int j=0;j<row;j++){

					System.out.print((char)x++ +	"	");
				}

			}else{
				for(int j=0;j<row;j++){
					System.out.print((char)x-- +	"	");
			
				}
			}
		
			System.out.println();
			x=x-1;
		}		
			
		}
		
	

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

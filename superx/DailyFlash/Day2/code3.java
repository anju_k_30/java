//3.WAP to check whether the given no is prime or composite

import java.io.*;
class Num{
	static int check(int no){
		int count=2;
		if(no <= 1) {
     			return -1;
        	}

		for(int i=2;i<=no/2;i++){
			if(no%i==0)
				count++;
		}
		return count;
	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no");
		int no=Integer.parseInt(br.readLine());

		if((check(no))==-1)
			System.out.println("One is neither composite nor prime");
		else if((check(no)>2))
			System.out.println("Its Composite no");
		else	
			System.out.println("Its Prime no");
	}
}


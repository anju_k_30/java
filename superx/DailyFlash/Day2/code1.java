//1.WAP to print the following pattern
//
//A B C D
//B C D E
//C D E F
//D E F G

import java.util.*;
class Pattern{
	static void pattern(int row){
		
		int x=65;;
		for(int i=0;i<row;i++){
			x=x+i;
			for(int j=0;j<row;j++){
				System.out.print((char)x++ +	"	");
			}
			System.out.println();
			x=65;
			
		}
		
	}

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

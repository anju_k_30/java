//4.WAP to print the composite no in the gievn range

import java.io.*;
class Num{
	static void Composite(int start,int end){
		
	
		for(int j=start;j<=end;j++){
			int count=2;
			for(int i=2;i<=j/2;i++){
				if(j%i==0)
					count++;
			}
			if(count>2)
				System.out.println(j);
		}
	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter start");
		int start=Integer.parseInt(br.readLine());
		
		System.out.println("Enter end");
		int end=Integer.parseInt(br.readLine());

		Composite(start,end);
	}
}


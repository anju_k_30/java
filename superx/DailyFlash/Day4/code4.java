//4.WAP to print the sum of digits in a given range

import java.io.*;
class Num{
	static void sum(int start,int end){
		int sum=0;
		for(int i=start;i<=end;i++){
			sum=sum+i;
		}
			System.out.println(sum);
		
	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter start");
		int start=Integer.parseInt(br.readLine());
		
		System.out.println("Enter end");
		int end=Integer.parseInt(br.readLine());

		sum(start,end);
	}
}


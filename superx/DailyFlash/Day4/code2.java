//2.WAP to print the following pattern
//
//A
//B A
//C B A
//D C B A

import java.util.*;
class Pattern{
	static void pattern(int row){
		int x=65;
		for(int i=0;i<row;i++){
			x=65+i;
			for(int j=0;j<=i;j++){
				System.out.print((char)x-- +	"	");
			}
			System.out.println();	
			
		}
		
	}

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

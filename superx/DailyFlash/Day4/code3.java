//3.WAP to find the factors of a given no.

import java.io.*;
class Num{
	static void Factors(int no){
		int i=1;
		while(i<=no){
			if(no%i==0)
				System.out.println(i);
			i++;
		}

	}
	
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no");
		int no=Integer.parseInt(br.readLine());

		Factors(no);
	}
}


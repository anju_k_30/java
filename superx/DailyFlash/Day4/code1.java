//1.WAP to print the following pattern
//
//A B C D
//# # # #
//A B C D
//# # # #

import java.util.*;
class Pattern{
	static void pattern(int row){
		
		int x=65;
		for(int i=0;i<row;i++){
			x=65;	
			for(int j=0;j<row;j++){
				if(i%2==0)
					System.out.print((char)x++ +	"	");
				else 
					System.out.print("#	");
				}
		
			System.out.println();
			x=x-1;
		}		
			
	}
		
	

public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

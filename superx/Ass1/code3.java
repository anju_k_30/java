//0
//3  8
//15 24 35
//48 63 80 99

class Triangle{
	public static void main(String s[]){
		
		int rows=4;
		int iteration=(rows*(rows+1))/2;
		
		int x=1;
		int counter=0;
		int ln=1;

		for(int i=1;i<=iteration;i++){
			
			counter++;
			System.out.print(x*x-1 +"	");
			x++;

			if(counter==ln){
				System.out.println();
				ln++;
				counter=0;
			}
		}
	}

}

//WAP to print prime no in a range, take a no. range from user

import java.io.*;
class Demo{
	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Range");
		int begin=Integer.parseInt(br.readLine());
		int end=Integer.parseInt(br.readLine());
		
		int count=0;

		System.out.println("Prime Numbers are:");

		for(int i=begin;i<=end;i++){
			count=0;
			for(int j=1;j<=i;j++){
				if(i%j==0){
					count++;
				}
			}
			if(count==2)
				System.out.println(i);
		}
	
	}
}

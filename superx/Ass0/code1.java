//1.WAP to print the factorial of even numbers in a given no.

import java.io.*;

class Demo{

	static int factorial(int no){
		int fact=1;
		for(int i=no;i>1;i--){
			fact=fact*i;
		}
		return fact;
	}

	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number");

		int num=Integer.parseInt(br.readLine());

		int x=num;

		while(x>0){
			int rem = x % 10;
			if(rem % 2==0)
				System.out.print(factorial(rem)+" ");
			x=x/10;
		}
		System.out.println();
	}
}




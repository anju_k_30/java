//2.WAP to find whether the no. is an armstrong number or not

import java.io.*;

class Demo {
    static boolean isArmstrong(int num, int count) {
        int sum = 0;
        int originalNum = num;

        while (num > 0) {
            int digit = num % 10;
            int power = 1;

         
            for (int i = 1; i <= count; i++) {
                power =power* digit;
            }

            sum =sum+power;
            num = num / 10;
        }

        return sum == originalNum;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter Number");

        int num = Integer.parseInt(br.readLine());

        int x = num;
        int count = 0;

        while (x > 0) {
            count++;
            x = x / 10;
        }

        if (isArmstrong(num, count)) {
            System.out.print("It is an Armstrong number\n");
        } else {
            System.out.print("It is Not an Armstrong number\n");
        }

    }
}


//3.
//
//D C B A
//e f g h
//F E D C
//g h i j

import java.util.*;
class Pattern{
	static void pattern(int row){
		int letter1=65;

		for(int i=1;i<=row;i++){
			letter1=letter1+row-1;

			for(int j=1;j<=row;j++){
				if(i%2!=0){
					System.out.print((char)(letter1--) +"	");
				}
				else{	
					letter1=letter1+row;
					System.out.print((char)(letter1++) +"	");
				}
						
			}
			System.out.println();
		}
	}
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		pattern(row);
	}
}

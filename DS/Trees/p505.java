//Count Node

import java.util.*;

class Node{
	int data;
	Node left;
	Node right;

	Node(int data){
		this.data=data;
		left=right=null;
	}
}

class BinaryTree{
	Scanner sc=new Scanner(System.in);

	Node constructBT(){
		
		System.out.println("Enter data for Node");
		int data=sc.nextInt();

		Node newNode=new Node(data);

		System.out.println("Do you want to enter node at left side of the :"+ newNode.data);
		char leftNode=sc.next().charAt(0);
		
		if(leftNode =='y' || leftNode =='Y'){
			newNode.left=constructBT();
		}

		System.out.println("Do you want to enter node at right side of the :"+ newNode.data);
		char rightNode=sc.next().charAt(0);
		
		if(rightNode =='y' || rightNode =='Y'){
			newNode.right=constructBT();
		}

		return newNode;

	}

	void preOrderBT(Node root){

		if(root==null)
			return;
		
		System.out.print(root.data + " ");
		preOrderBT(root.left);
		preOrderBT(root.right);
	}

	/*void inOrderBT(Node root){

		if(root==null)
			return;
		
		inOrderBT(root.left);
		System.out.print(root.data + " ");
		inOrderBT(root.right);
	}

	void postOrderBT(Node root){

		if(root==null)
			return;
		
		postOrderBT(root.left);
		postOrderBT(root.right);
		System.out.print(root.data + " ");
	
	}*/
	
	int countNodes(Node root){
		if(root==null)
			return 0;

		int leftSTNodes=countNodes(root.left);
		int rightSTNodes=countNodes(root.right);

		return leftSTNodes+rightSTNodes+1;
	}

	public static void main(String s[]){

		Node root=null;

		BinaryTree bt=new BinaryTree();

		root=bt.constructBT();

		System.out.print("PreOrder Tree : ");
		bt.preOrderBT(root);
		System.out.println();

		System.out.println("Total Nodes = " + bt.countNodes(root));
		
		/*System.out.print("InOrder Tree : ");
		bt.inOrderBT(root);
		System.out.println();

		System.out.print("PostOrder Tree : ");
		bt.postOrderBT(root);
		System.out.println();*/
		
		

	
	}
}



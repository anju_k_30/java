//Construct Binary Tree from given Array(preOrder)
//eg:nodes array=[ 1 2 4 -1 -1 5 -1 -1 3 -1 6 -1 -1 ]
//-1 indicates null

class Node{
	int data;
	Node left;
	Node right;

	Node(int data){
		this.data=data;
		left=right=null;
	}
}

class BinaryTree{

	int index=-1;

	Node constructBT(int nodesarr[]){
	
		index++;
		if(nodesarr[index]==-1)
			return null;

		Node newNode=new Node(nodesarr[index]);
		newNode.left=constructBT(nodesarr);
		newNode.right=constructBT(nodesarr);

		return newNode;	

	}
	
	void preOrderBT(Node root){

		if(root==null)
			return;
		
		System.out.print(root.data + " ");
		preOrderBT(root.left);
		preOrderBT(root.right);
	}

	void inOrderBT(Node root){

		if(root==null)
			return;
		
		inOrderBT(root.left);
		System.out.print(root.data + " ");
		inOrderBT(root.right);
	}

	void postOrderBT(Node root){

		if(root==null)
			return;
		
		postOrderBT(root.left);
		postOrderBT(root.right);
		System.out.print(root.data + " ");
	
	}

	public static void main(String s[]){
		
		Node root=null;
		
		int nodesarr[]={1,2,4,-1,-1,5,-1,-1,3,-1,6,-1,-1};

		BinaryTree bt =new BinaryTree();
		root=bt.constructBT(nodesarr);

		bt.preOrderBT(root);
		System.out.println();

		bt.inOrderBT(root);
		System.out.println();

		bt.postOrderBT(root);
		System.out.println();


	}}

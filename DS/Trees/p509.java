//Level Order Traversal Binary Tree
//Approach : II

//add root and null,check data is not null:print & add childrens,if data is null put \n and add null

import java.util.*;

class Node{
	int data;
	Node left;
	Node right;

	Node(int data){
		this.data=data;
		left=right=null;
	}
}

class BinaryTree{
	Scanner sc=new Scanner(System.in);

	Node constructBT(){
		
		System.out.println("Enter data for Node");
		int data=sc.nextInt();

		Node newNode=new Node(data);

		System.out.println("Do you want to enter node at left side of the :"+ newNode.data);
		char leftNode=sc.next().charAt(0);
		
		if(leftNode =='y' || leftNode =='Y'){
			newNode.left=constructBT();
		}

		System.out.println("Do you want to enter node at right side of the :"+ newNode.data);
		char rightNode=sc.next().charAt(0);
		
		if(rightNode =='y' || rightNode =='Y'){
			newNode.right=constructBT();
		}

		return newNode;

	}

	void levelOrder(Node root){
		
		Queue<Node>queue=new LinkedList<>();
		queue.add(root);
		queue.add(null);

		while(!queue.isEmpty()){
			Node tempNode=queue.remove();
			if(tempNode==null){
				System.out.println();
				if(queue.isEmpty())
					break;
				else
					queue.add(null);
			}

			else{
				System.out.print(tempNode.data+" ");
				if(tempNode.left!=null)
					queue.add(tempNode.left);
				if(tempNode.right!=null)
					queue.add(tempNode.right);
			}
		}

	}

	public static void main(String s[]){

		Node root=null;

		BinaryTree bt=new BinaryTree();

		root=bt.constructBT();

		bt.levelOrder(root);
		System.out.println();
	
	}
}



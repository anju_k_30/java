//Square root

import java.io.*;

class SquareRoot{

	static int sqrt(int num){

		int val=0;
		for(int i=1;i<=num;i++){
			if(i*i<=num)
				val=i;
		}
		return val;
	}

	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int num=Integer.parseInt(br.readLine());
		int sol=sqrt(num);
		System.out.println(sol);
	}
}

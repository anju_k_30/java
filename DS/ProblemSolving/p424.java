import java.io.*;

class SquareRoot{
	static int sqrt(int num){
		
		int start=1;
		int end=num;
		int ans=0;
		int itr=0;

		while(start<=end){
			
			itr++;
			int mid=(start+end)/10;
			int sr=mid*mid;

			if(sr==num){
				System.out.println(itr);
				return mid;
			}

			if(sr>num){
				end=mid-1;
			}

			if(sr<num){
				start=mid+1;
				ans=mid;
			}
		}
		System.out.println(itr);
		return ans;
	}

	public static void main(String s[])throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int num=Integer.parseInt(br.readLine());
		int sol=sqrt(num);
		System.out.println(sol);
	}
}


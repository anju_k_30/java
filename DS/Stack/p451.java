//reverse a string using stack

import java.util.*;
import java.util.Stack;

class ReverseString{

	String revString(String str){

       	        char stackArr[]=new char[str.length()];

		Stack<Character> s = new Stack<Character>();
		
		for(int i=0;i<str.length();i++){
			s.push(str.charAt(i));
		}

		//System.out.println(s);
		
		int i=0;
		
		while(!s.empty()){
			stackArr[i]=s.pop();
			i++;
		}

		return new String(stackArr);
	}

}

class Client{

	public static void main(String s[]){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter String to Reverse");
		String str=sc.next();


		ReverseString obj=new ReverseString();
		String rev=obj.revString(str);


		System.out.println(rev);
	}
}

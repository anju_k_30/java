//Two stacks using an single array

import java.util.*;

class TwoStacks{

	int stackArr[];
	int top1;
	int top2;
	int maxSize;

	TwoStacks(int size){
		
		this.maxSize=size;
		this.stackArr=new int[size];
		this.top1=-1;
		this.top2=size;
	}

	void push1(int data){
		
		if(top2-top1>1){
			top1++;
			stackArr[top1]=data;
		}
		else{
			System.out.println("Stack overFlow");
		}
	}

	void push2(int data){
		
		if(top2-top1>1){
			top2--;
			stackArr[top2]=data;
		}
		else{
			System.out.println("Stack overFlow");
		}
	}

	int pop1(){

		if(top1==-1){
			System.out.println("Stack 1 is empty");
			return -1;
		}
		else{
			int ret=stackArr[top1];
			top1--;
			return ret;
		}
	}
	
	int pop2(){

		if(top2==maxSize){
			System.out.println("Stack 2 is empty");
			return -1;
		}
		else{
			int ret=stackArr[top2];
			top2++;
			return ret;
		}
	}

	void printStacks(){

		System.out.println("Stack 1");
		if(top1==-1)
			System.out.println("Stack 1 is empty");
		else{
  			for(int i=0;i<=top1;i++)
			System.out.println(stackArr[i] +" ");
		}

		System.out.println("\nStack 2");
		if(top2 == maxSize)
			System.out.println("Stack 2 is empty");
		else{	
			for(int i=maxSize-1;i>=top2;i--)
				System.out.print(stackArr[i] +" ");
		}
    		System.out.println();
	}
}


class Client{

	public static void main(String args[]){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter Array Size");
		int size=sc.nextInt();

		TwoStacks obj=new TwoStacks(size);

		char ch;
		do{
			System.out.println("1.push1");
			System.out.println("2.push2");
			System.out.println("3.pop1");
			System.out.println("4.pop2");
			System.out.println("5.printStack 1 and 2");


			System.out.println("Enter your Choice");
			int choice=sc.nextInt();

			switch(choice){

				case 1:
					{
						System.out.println("Enter Element of stack1");
						int data=sc.nextInt();
						obj.push1(data);
					}
					break;

				case 2:
					{
						System.out.println("Enter Element of stack2");
						int data=sc.nextInt();
						obj.push2(data);
					}
					break;

				case 3:
					{
						int ret=obj.pop1();
						if(ret!=-1)
							System.out.println(ret +" popped from stack1");
					}
					break;

				case 4:
					{
						int ret=obj.pop2();
						if(ret!=-1)
							System.out.println(ret +" popped from stack2");
					}
					break;

				case 5:
					obj.printStacks();
					break;

				default:
					System.out.println("Wrong Choice");
					break;
			}
			System.out.println("Do you want to continue?");
			ch=sc.next().charAt(0);		
		}

		while(ch=='y' ||ch=='Y');
	}
}


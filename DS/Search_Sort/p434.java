class BubbleSort{
	int count=0;
	void sort(int arr[],int N){
		if(N==1)
			return;

		for(int j=0;j<arr.length-1;j++){
			count++;
			if(arr[j]>arr[j+1]){
				int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}

		sort(arr,N-1);
	}

	public static void main(String s[]){
		int arr[]=new int[]{7,3,9,4,2,5,6};
		BubbleSort obj=new BubbleSort();
		obj.sort(arr,arr.length);

		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+"  ");

		System.out.println("\n"+obj.count);
	}
}

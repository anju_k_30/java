//Selection Sort

class Selection{

	public static void main(String s[]){

		int arr[]=new int[]{9,2,7,3,1,8,4,6};
		
		for(int i=0;i<arr.length-1;i++){
			int minInd=i;
			for(int j=i+1;j<arr.length;j++){
				if(arr[j]<arr[minInd]){
					minInd=j;
				}
			}
			int temp=arr[i];
			arr[i]=arr[minInd];
			arr[minInd]=temp;

		}
		for(int k=0;k<arr.length;k++)
			System.out.print(arr[k]+"  ");

	}

}
			

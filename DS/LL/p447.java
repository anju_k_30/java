//Middle node of Linked List

class Node{

	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}

class LinkedList{

	Node head=null;

	//addnode
	void addNode(int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
	}

	//printLL
	void printll(){
		if(head==null)
			System.out.println("Empty LL");
		else{
			Node temp=head;
			while(temp.next!=null){
				System.out.print(temp.data +"=>");
				temp=temp.next;
			}
			System.out.println(temp.data);
		}
	}
	//countNode
	int countNode(){
		Node temp=head;
		int count=0;
		if(head==null)
			return 0;
		else{
			while(temp!=null){
				count++;
				temp=temp.next;
			}
			
		}
		return count;
	}
	
	//middle
	int middle(){
		int length=countNode();
		Node temp=head;
		int count=0;

		while(count<length/2){
			temp=temp.next;
			count++;
		}
		return temp.data;
	}
}
class Client{
	public static void main(String s[]){

		LinkedList ll=new LinkedList();
		ll.addNode(10);
		ll.addNode(20);
		ll.addNode(30);
		ll.addNode(40);
		ll.addNode(50);

		ll.printll();
		System.out.println(ll.middle());
		
		ll.addNode(60);
		ll.printll();
		System.out.println(ll.middle());
		
	
	}
}

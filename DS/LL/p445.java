//in place reverse using while loop

class Node{

	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}

class LinkedList{

	Node head=null;

	//addnode
	void addNode(int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
	}

	//printLL
	void printll(){
		if(head==null)
			System.out.println("Empty LL");
		else{
			Node temp=head;
			while(temp.next!=null){
				System.out.print(temp.data +"=>");
				temp=temp.next;
			}
			System.out.println(temp.data);
		}
	}

	//reverse with iterative approach
	void reverseItr(){
		if(head==null)
			return;
		Node prev=null;
		Node curr=head;
		Node forward=null;

		while(curr!=null){
			forward=curr.next;
			curr.next=prev;
			prev=curr;
			curr=forward;
		}
		head=prev;
	}
}

class Client{
	public static void main(String s[]){

		LinkedList ll=new LinkedList();
		ll.addNode(10);
		ll.addNode(20);
		ll.addNode(30);
		ll.addNode(40);
		ll.addNode(50);

		ll.printll();
		ll.reverseItr();
		ll.printll();

	}
}

import java.util.*;

class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}

class LinkedList{

	Node head=null;

	//addfirst
	void addFirst(int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			newNode.next=head;
			head=newNode;
		}
	}

	//printLL
	void printll(){
		if(head==null)
			System.out.println("Empty LL");
		else{
			Node temp=head;
			while(temp.next!=null){
				System.out.print(temp.data +"=>");
				temp=temp.next;
			}
			System.out.println(temp.data);
		}
	}

	//addlast
	void addLast(int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
	}

	//countNode
	int countNode(){
		Node temp=head;
		int count=0;
		while(temp!=null){
			count++;
			temp=temp.next;
		}
		return count;
	}

	//addatpos
	void addAtPos(int pos,int data){
		
		if(pos<=0||pos>=countNode()+2){
			System.out.println("Invalid pos");
			return;
		}
		if(pos==1)
			addFirst(data);
		else if(pos==countNode()+1)
			addLast(data);
		else{
			Node newNode=new Node(data);
			Node temp=head;
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			newNode.next=temp.next;
			temp.next=newNode;
		}
	}


	//deletefirst
	void delFirst(){
		if(head==null){
			System.out.println("Empty LL");
			return;
		}
		if(countNode()==1)
			head=null;
		else{
			head=head.next;
		}
	}


	//deletelast
	void delLast(){
		if(head==null){
			System.out.println("Empty LL");
			return;
		}
		if(countNode()==1)
			head=null;
		else{
			Node temp=head;
			while(temp.next.next!=null){
				temp=temp.next;
			}
			temp.next=null;
		}
	}

	//deleteatpos
	void delAtPos(int pos){

		if(pos<=0||pos>=countNode()+1){
			System.out.println("Invalid pos");
			return;
		}
		if(pos==1)
			delFirst();
		else if(pos==countNode())
			delLast();
		else{
			Node temp=head;
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			temp.next=temp.next.next;
		}
	}
}
class Client{
	public static void main(String s[]){
		LinkedList ll=new LinkedList();
		Scanner sc=new Scanner(System.in);

		char ch;

		do{
			System.out.println("Singly LinkedList");
			System.out.println("1.addfirst");
			System.out.println("2.addlast");
			System.out.println("3.addatpos");
			System.out.println("4.delfirst");
			System.out.println("5.dellast");
			System.out.println("6.delatpos");
			System.out.println("7.countnode");
			System.out.println("8.printLL");

			System.out.println("Enter your choice");

			int choice=sc.nextInt();
			switch(choice){
				case 1:
					{
					       System.out.println("Enter data");
					       int data=sc.nextInt();
					       ll.addFirst(data);
					}break;

				case 2:
					{
					       System.out.println("Enter data");
					       int data=sc.nextInt();
					       ll.addLast(data);
					}break;

				case 3:
					{
					       System.out.println("Enter data");
					       int data=sc.nextInt();
					       System.out.println("Enter pos");
					       int pos=sc.nextInt();
					       ll.addAtPos(pos,data);
					}break;

				case 4:
					ll.delFirst();
					break;

				case 5:
					ll.delLast();
					break;

				case 6:
					{
					       System.out.println("Enter pos");
					       int pos=sc.nextInt();
					       ll.delAtPos(pos);
					}break;

				case 7:
					{
						int count=ll.countNode();
						System.out.println(count);
					}
					break;

				case 8:
					ll.printll();
					break;
				
				default:
					System.out.println("Wrong choice");
					break;
			}
			System.out.println("Do you want to continue?");
			ch=sc.next().charAt(0);
		}
		while(ch=='Y'||ch=='y');
	}
}

//Middle node of Linked List

class Node{

	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}

class LinkedList{

	Node head=null;

	//addnode
	void addNode(int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
	}

	//printLL
	void printll(){
		if(head==null)
			System.out.println("Empty LL");
		else{
			Node temp=head;
			while(temp.next!=null){
				System.out.print(temp.data +"=>");
				temp=temp.next;
			}
			System.out.println(temp.data);
		}
	}
	//middle node using slow and fast reference
	
	int middle(){
	//	if(head==null)
	//		return;

		Node slow=head;
		Node fast=head.next;

		while(fast!=null){
			fast=fast.next;
			if(fast!=null)
				fast=fast.next;
			slow=slow.next;
		}
		return slow.data;

	}
}
class Client{
	public static void main(String s[]){

		LinkedList ll=new LinkedList();
		ll.addNode(10);
		ll.addNode(20);
		ll.addNode(30);
		ll.addNode(40);
		ll.addNode(50);

		ll.printll();
		System.out.println(ll.middle());
		
		ll.addNode(60);
		ll.printll();
		System.out.println(ll.middle());
		
	
	}
}

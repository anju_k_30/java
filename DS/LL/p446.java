//in place reverse using recursion

class Node{

	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}

class LinkedList{

	Node head=null;

	//addnode
	void addNode(int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
	}

	//printLL
	void printll(){
		if(head==null)
			System.out.println("Empty LL");
		else{
			Node temp=head;
			while(temp.next!=null){
				System.out.print(temp.data +"=>");
				temp=temp.next;
			}
			System.out.println(temp.data);
		}
	}
	
	//reverse with recursion
	void reverseRec(Node prev,Node curr){
		
		if(curr==null){
			head=prev;
			return;
		}else{
			Node forward=curr.next;
			curr.next=prev;
			prev=curr;
			curr=forward;
		}
		reverseRec(prev,curr);
	}
}

class Client{
	public static void main(String s[]){

		LinkedList ll=new LinkedList();
		ll.addNode(10);
		ll.addNode(20);
		ll.addNode(30);
		ll.addNode(40);
		ll.addNode(50);

		ll.printll();
		Node prev=null;
		ll.reverseRec(prev,ll.head);
		ll.printll();

	}
}

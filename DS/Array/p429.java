//Print the sum of every single subarray using prefix sum technique


class ArrayDemo{
	public static void main(String s[]){
		int arr[]=new int[]{2,4,1,3};

		int prefixarray[]=new int[arr.length];

		prefixarray[0]=arr[0];

		for(int i=1;i<arr.length;i++)
			prefixarray[i]=prefixarray[i-1]+arr[i];

		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				int sum=0;

				if(i==0)
					sum=prefixarray[j];
				else
					sum=prefixarray[j]-prefixarray[i-1];
				System.out.println(sum);
			}
		}
	}
}
					

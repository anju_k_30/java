//Bufferreader

import java.io.*;

class InputOutput{
	public static void main(String s[])throws IOException{

		InputStreamReader obj= new InputStreamReader(System.in);
		BufferedReader ob2= new BufferedReader(obj);

		System.out.println("Enter Name");
		String name=ob2.readLine();

		System.out.println(name);

		System.out.println("Enter Age");
		int age=Integer.parseInt(ob2.readLine());

		System.out.println(age);
	}
}


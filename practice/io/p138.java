//code3:

import java .util.*;

class Tokendemo{
	public static void main(String s[]){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Player info");
		String str=sc.nextLine();			//to read whole line

		StringTokenizer st=new StringTokenizer(str," ");	//token i.e parts are created using 2 parameters(string and delimator)

		System.out.println(st.countTokens());			//counts

		while(st.hasMoreTokens()){				//next tokens line
			System.out.println(st.nextToken());
		}
	}
}


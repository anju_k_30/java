//1.myStrLen()

import java.io.*;
class Demo{
	static int mystrlen(String str){
		char arr[]=str.toCharArray();

		int count=0;
		for(int i=0;i<arr.length;i++){
			count++;
		}
		return count;
	}

	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter 2 Strings");

		String str1=br.readLine();
		String str2=br.readLine();
		
		if(mystrlen(str1)==mystrlen(str2))
			System.out.println("Length equal");
		else
			System.out.println("Length not equal");

	}
}
		

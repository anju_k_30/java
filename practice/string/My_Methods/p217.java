//
import java.io.*;
class Demo{
	 int mystrlen(String str){
		char arr[]=str.toCharArray();

		int count=0;
		for(int i=0;i<arr.length;i++)
			count++;
		
		return count;
	}

	int compareStr(String str1,String str2){

		char a1[]=str1.toCharArray();
		char a2[]=str2.toCharArray();

		for(int i=0;i<a1.length;i++){
			if(a1[i]!=a2[i])
				return a1[i]-a2[i];
		}
		return 0;
	}

	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter 2 Strings");

		String str1=br.readLine();
		String str2=br.readLine();
		
		Demo obj=new Demo();

		if(obj.mystrlen(str1)==obj.mystrlen(str2)){
			
			int val=obj.compareStr(str1,str2);
			
			if(val!=0)
				System.out.println(val);
			else
				System.out.println("Equal");
		}else
			System.out.println("Diff Strings");

	}
}
		

//1.myStrLen(): 

import java.util.Scanner;
class Demo{
	static int mystrlen(String str){
		char arr[]=str.toCharArray();

		int count=0;
		for(int i=0;i<arr.length;i++){
			count++;
		}
		return count;
	}

	public static void main(String s[]){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter 2 Strings");

		String str1=sc.next();
		String str2=sc.next();
		
		if(mystrlen(str1)==mystrlen(str2))
			System.out.println("Length equal");
		else
			System.out.println("Length not equal");

	}
}
		

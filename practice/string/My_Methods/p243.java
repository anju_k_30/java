//3.charAt()

import java.io.*;
class Demo{
	char MycharAt(String str1,int pos){
		char arr1[]=str1.toCharArray();
		return arr1[pos];
	
	}

	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String and Index");

		String str1=br.readLine();
		int pos=Integer.parseInt(br.readLine());

		
		Demo obj=new Demo();
		
		char ch=obj.MycharAt(str1,pos);
		System.out.println("At given Index charcter is "+ch);
		
	
	}
}
	

//StringBuffer Methods

//4.reverse()

class Demo{
	public static void main(String s[]){
		StringBuffer str=new StringBuffer("abcdefghijklmnopqrstuvwxyz");
		
		System.out.println(str.reverse());	

		String str1="Core2Web";
		StringBuffer sb2=new StringBuffer(str1);
//		str1=sb2.reverse();		error: incompatible types: StringBuffer cannot be converted to String
		
		str1=sb2.reverse().toString();
		System.out.println(str1);

	}
}

class Demo{
	public static void main(String s[]){
		String str1="Anjali";			//1000
		String str2="Kumbhar";			//2000
		
		System.out.println(str1);
		System.out.println(str2);
		
		str1.concat(str2);

		System.out.println(str1);
		System.out.println(str2);

		System.out.println(str1+str2);

		String str3="AnjaliKumbhar";		//3000
	//	String str4=str1+str2;		
		String str4=str1.concat(str2);		//4000
		//(internally calls new string)

		System.out.println(System.identityHashCode(str3));	//3000(SCP)
		System.out.println(System.identityHashCode(str4));	//4000
	}
}


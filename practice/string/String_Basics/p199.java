class StringDemo{
	public static void main(String s[]){
		String s1="C2W";		//stringconstantpool
		String s2=new String("C2W");	//heap
		char s3[]={'C','2','W'};	//heap

		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);

		String s4="C2W";		//stringconstantpool
		String s5=new String("C2W");	//heap
		
		System.out.println(System.identityHashCode(s1));	//1000
		System.out.println(System.identityHashCode(s2));	//2000
		System.out.println(System.identityHashCode(s3));	//3000
		System.out.println(System.identityHashCode(s4));	//1000
		System.out.println(System.identityHashCode(s5));	//4000

	}
}


class Demo{
	public static void main(String s[]){
		String str1="Anju";
		String str2="Anju";
		String str3=new String("Anju");
		String str4=new String("Anju");
		String str5=new String("Om");
		String str6="om";

		String s1="Anjali";
		String s2=s1;
		String s3=new String(s2);

		System.out.println(System.identityHashCode(str1));	//1000
		System.out.println(System.identityHashCode(str2));	//1000
		System.out.println(System.identityHashCode(str3));	//2000
		System.out.println(System.identityHashCode(str4));	//3000
		System.out.println(System.identityHashCode(str5));	//4000
		System.out.println(System.identityHashCode(str6));	//5000

		System.out.println(System.identityHashCode(s1));	//100
		System.out.println(System.identityHashCode(s2));	//100
		System.out.println(System.identityHashCode(s3));	//200
	
	}
}


class Demo{
	public static void main(String s[]){
		String str1="Anjali";		//scp
		String str2=str1.concat("Anjali");	//heap
		
		System.out.println(str1);	//Anjali
		System.out.println(System.identityHashCode(str1));	//1000

		System.out.println(str2);	//AnjaliAnjali
		System.out.println(System.identityHashCode(str2));	//2000
	

		String str="Shashi";		//scp
		str=str.concat("Bagal");	//heap

		System.out.println(str);	//heap-->ShashiBagal
		System.out.println(System.identityHashCode(str));	//3000
	}
}

		

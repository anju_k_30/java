class Demo{
	public static void main(String s[]){
		int arr1[]={10,200,300};
		int arr2[]={10,200,300};		//			Integer arr[]={10,200,300};

		System.out.println(System.identityHashCode(arr1[0]));	//1000		//1000
		System.out.println(System.identityHashCode(arr1[1]));	//2000		//2000
		System.out.println(System.identityHashCode(arr1[2]));	//3000		//3000
		
		System.out.println(System.identityHashCode(arr2[0]));	//1000		//1000
		System.out.println(System.identityHashCode(arr2[1]));	//4000		//4000
		System.out.println(System.identityHashCode(arr2[2]));	//5000		//5000
		
		System.out.println(System.identityHashCode(arr2[0]));	//1000		//1000
		System.out.println(System.identityHashCode(arr2[1]));	//6000		//4000
		System.out.println(System.identityHashCode(arr2[2]));	//7000		//5000
		}
}
////Different identityhashcodes because it first calls valueof() method and due to it new obj gets created
                //Primitive type:->JVM calls :->valueof() method 
                //but this don't happens with class 


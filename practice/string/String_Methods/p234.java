//12.startsWith(String prefix,int offset)		returntype:boolean

class Demo{
	public static void main(String []args){
		String str="Core2Web";
		System.out.println(str.startsWith("c",0));	//false
		System.out.println(str.startsWith("C",0));	//true
		System.out.println(str.startsWith("or",1));	//true

	}
}


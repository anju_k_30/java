//13.endsWith(String suffix)		returntype:boolean

class Demo{
	public static void main(String []args){
		String str="Core2Web";
		System.out.println(str.endsWith("c"));	//false
		System.out.println(str.endsWith("b"));	//true
		System.out.println(str.endsWith("Web"));	//true

	}
}


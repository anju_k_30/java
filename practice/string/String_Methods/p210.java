//4.compareTo()

class Demo{
	public static void main(String s[]){
		String str1="Core2";
		String str2="Web";

		System.out.println(str1.compareTo(str2));	//-20
		
		String str3="anju";
		String str4="Anju";
		String str5="anju";
		String str6="anjali";

		
		System.out.println(str3.compareTo(str4));	//32
		System.out.println(str3.compareTo(str5));	//0
		System.out.println(str3.compareTo(str6));	//20
	}
}



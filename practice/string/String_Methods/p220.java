//9:lastIndexOf()

class Demo{
	public static void main(String s[]){
		String str1="anjali";
		System.out.println(str1.lastIndexOf('a',0));	//0
		System.out.println(str1.lastIndexOf('a',1));	//0
		System.out.println(str1.lastIndexOf('a',8));	//3
	
		String str2="shashi";
		System.out.println(str2.lastIndexOf('h',0));	//-1
		System.out.println(str2.lastIndexOf('s',0));	//-1
		System.out.println(str2.lastIndexOf('s',5));	//3
		System.out.println(str2.lastIndexOf('h',5));	//3
	

	}
}

//5.compareToIgnoreCase()

class Demo{
	public static void main(String s[]){
	
		String str3="anju";
		String str4="Anju";
		String str6="ANJU";
		
		String str="SHASHI";
		String str1="Shashikant";

		System.out.println(str3.compareToIgnoreCase(str4));	//0
		System.out.println(str3.compareToIgnoreCase(str6));	//0
		System.out.println(str4.compareToIgnoreCase(str6));	//0
		System.out.println(str.compareToIgnoreCase(str1));	//-4
	}
}



class Demo{
	public static void main(String s[]){
		//StringBuffer str1="Anjali";	error: incompatible types: String cannot be converted to StringBuffer
		
		StringBuffer str1=new StringBuffer("Anjali");
		
		System.out.println(str1);				//Anjali
		System.out.println(System.identityHashCode(str1));	//1000
		
		str1.append("Kumbhar");					
			
		System.out.println(str1);				//AnjaliKumbhar
		System.out.println(System.identityHashCode(str1));	//1000	
	
		System.out.println(str1.capacity());			//16+6
	}
}

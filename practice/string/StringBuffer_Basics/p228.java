class Demo{
	public static void main(String s[]){
		String str1="Shashi";
		String str2=new String("Bagal");
		StringBuffer str3=new StringBuffer("Core2Web");
		
		//String str4=str1.concat(str3);	
		//error: method concat in class String cannot be applied to given types;
		//parameter for concat should be of String class only

		StringBuffer str5=str3.append(str2);

		System.out.println(str1);	//Shashi
		System.out.println(str2);	//Bagal
		System.out.println(str3);	//Core2WebBagal
		System.out.println(str5);	//Core2WebBagal
	}
}


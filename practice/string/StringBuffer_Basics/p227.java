class Demo{
	public static void main(String s[]){
		String str1="Anjali";
		String str2=new String("Kumbhar");
		StringBuffer str3=new StringBuffer("Core2Web");
		
	//	String str4=str1.append(str3);											error: cannot find symbol-->(append() method is not present in String class)
		
	//	String str5=str3.append(str1);
	//	error: incompatible types: StringBuffer cannot be converted to String
	
		StringBuffer str6=str3.append(str1);	//-->(internally=>new String("Core2WebAnjali");)

		System.out.println(str1);	//Anjali
		System.out.println(str2);	//Kumbhar
		System.out.println(str3);	//Core2WebAnjali
		System.out.println(str6);	//Core2WebAnjali
	}
}


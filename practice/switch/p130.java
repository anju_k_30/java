//Switch statement:Break works on for while loops also on switch and do while.

class SwitchDemo{

	public static void main(String s[]){

		int x=3;

		switch(x){
			
			case 1:
				System.out.println("one");
			        break;
			
			case 2:
				System.out.println("two");
                               	break;
		
			case 3:
				System.out.println("three");
                                break;
			
			case 4:
				System.out.println("four");
                                break;
			
			case 5:
				System.out.println("five");
                                break;
			
			default:
				System.out.println("No Match");		//least priority
				break;

		}
		System.out.println("After Switch");
	}
}

//without break:three
		//four
		// five
		// no match 
		// after switch



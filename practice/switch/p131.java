//Switch statement:Break works on for while loops also on switch and do while.

class SwitchDemo{

	public static void main(String s[]){

		int x=5;

		switch(x){
			
			case 1:
				System.out.println("one");
			        break;
			
			case 2:
				System.out.println("two");
                               	break;
		
			case 5:
				System.out.println("five");
                                break;
			
		/*	case 5:
				System.out.println("2-five");		//error: duplicate case label

                                break;
			
			case 1+1:
				System.out.println("2-two");		//error: duplicate case label

                                break;
		*/
			default:
				System.out.println("No Match");		//least priority
				break;

		}
		System.out.println("After Switch");
	}
}



//Switch statement:Break works on for while loops also on switch and do while.

class SwitchDemo{

	public static void main(String s[]){

		int ch=65;

		switch(ch){
			
			case 'A':
				System.out.println("char-A");
			        break;
			
		/*	case 65:					//duplicate case label

				System.out.println("int-65");
                               	break;
		
			case 'B':
				System.out.println("char-B");
                                break;
		*/	
			case 66:					//duplicate case label

				System.out.println("int=66");
                                break;
			
			default:
				System.out.println("No Match");		//least priority
				break;

		}
		System.out.println("After Switch");
	}
}


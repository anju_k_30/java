//Switch statement:Break works on for while loops also on switch and do while.

class SwitchDemo{

	public static void main(String s[]){

		int x=3;
		int a=1;
		int b=2;

		switch(x){
			
			case a:
				System.out.println("char-A");		//error: constant expression required

			        break;
			
			case b:
				System.out.println("int-65");		//error: constant expression required

                               	break;
		
			case a+b:
				System.out.println("char-B");		//error: constant expression required

                                break;
		
			case a+a+b:	//a+b+b				

				System.out.println("int=66");		//error: constant expression required

                                break;
			
			default:
				System.out.println("No Match");		//least priority
				break;

		}
		System.out.println("After Switch");
	}
}


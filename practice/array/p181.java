//For Each Loop

class Example{
	public static void main(String S[]){
		int arr[]={10,30,20,40};

		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}

		for(int x:arr){
			System.out.println(x);
		}
	}
}


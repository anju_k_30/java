import java.io.*;

class MultiArray{
	public static void main(String s[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of row");
		int row=Integer.parseInt(br.readLine());
		
		System.out.println("Enter size of col");
		int col=Integer.parseInt(br.readLine());

		int arr[][]=new int [row][col];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}
}


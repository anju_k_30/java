//code 5:

import java.io.*;	//if not wriiten then 4 error(2-bufferedreader,1-inputstreamreader and 1-IOExceptioni)

class Example{
	public static void main(String S[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int arr[]=new int[5];

		System.out.println("Enter array elements");

		for(int i=0;i<5;i++){
			 arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<5;i++){
			System.out.println(arr[i]);
		}
	}
}

//if input is given as 'A' then error==>Exception in thread "main" java.lang.NumberFormatException: For input string: "a"


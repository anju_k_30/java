class InternalArray{
	public static void main(String s[]){
		int arr[]=new int[]{10,20,30};
		int arr2[]=new int[]{10,20,'A'};
		int arr3[]=new int[]{10,200,300};
		char arr4[]=new char[]{'A','B','C'};
	//	float arr5[]=new float[]{10,10.2,10.5f};	//here 10.2 only is considered double
		float arr5[]=new float[]{10,10.2f,10.5f};
	
		
		System.out.println(arr);
		System.out.println(arr2);
		System.out.println(arr3);
		System.out.println(arr4);
		System.out.println(arr5);
		System.out.println();

		System.out.println(System.identityHashCode(arr));
		System.out.println(System.identityHashCode(arr2));
		System.out.println(System.identityHashCode(arr3));
		System.out.println(System.identityHashCode(arr4));
		System.out.println(System.identityHashCode(arr5));
		System.out.println();
		
		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr3[0]));
		System.out.println(System.identityHashCode(arr5[0]));
		System.out.println(System.identityHashCode(arr4[0]));
		System.out.println();

		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr2[1]));
		System.out.println(System.identityHashCode(arr3[1]));
		System.out.println(System.identityHashCode(arr5[1]));
		System.out.println(System.identityHashCode(arr4[1]));
		System.out.println();

		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr2[2]));
		System.out.println(System.identityHashCode(arr3[2]));
		System.out.println(System.identityHashCode(arr5[2]));
		System.out.println(System.identityHashCode(arr4[2]));
		System.out.println();
	}
}





//code2:

class arraydemo{
	public static void main(String s[]){

		int arr1[]=new int[4];

		 arr1[0]=10;
		 arr1[1]=20;
		 arr1[2]=30;
		 arr1[3]=40;

		char arr2[]={'A','B'};
		float arr3[]={10.5f,20.5f};
		boolean arr4[]={true,false,true};

		System.out.println(arr1[0]);
		System.out.println(arr1[1]);
		System.out.println(arr1[2]);
		System.out.println(arr1[3]);
		
		System.out.println(arr2[0]);
		System.out.println(arr2[1]);
		
		System.out.println(arr3[0]);
		System.out.println(arr3[1]);
		
	//	System.out.println(arr3[2]);	runtime error:Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 2 out of bounds for length 2

		
		System.out.println(arr4[0]);
		System.out.println(arr4[1]);
		System.out.println(arr4[2]);
	}
}

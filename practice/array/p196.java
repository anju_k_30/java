//Jagged Array Initialization

import java.io.*;

class Jagged{
	public static void main(String s[])throws IOException{
	
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

 	int arr[][]=new int[3][];

//	arr[0]={1,2,3};		//Due to this Object of 1D array is not created
//	arr[1]={4,5,};
//	arr[2]={7};
	
	arr[0]=new int[]{1,2,3};
	arr[1]=new int[]{4,5,};
	arr[2]=new int[]{7};
	
	for(int i=0;i<arr.length;i++){
		for(int j=0;j<arr[i].length;j++){
			arr[i][j]=Integer.parseInt(br.readLine());
		}
	}

	for(int a[]:arr){
		for(int y:a){
			System.out.print(y+" ");
		}
	System.out.println();
	}
	}
}







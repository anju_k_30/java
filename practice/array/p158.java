//code4:using scanner class

import java.util.*;

class Example{
	public static void main(String s[]){
	
		Scanner sc=new Scanner(System.in);
		int arr[]=new int[5];

		System.out.println("Enter array elements");
		for(int i=0;i<5;i++)
			 arr[i]=sc.nextInt();

		for(int i=0;i<5;i++)
			System.out.println(arr[i]);
	}
}

//if A is given as input then ==>Exception in thread "main" java.util.InputMismatchException


//Jagged Array

class JaggedArray{
	public static void main(String s[]){
		int arr[][]={{10,20,30},{40,50},{50}};

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
		for(int x[]:arr){
			for(int y:x){
			System.out.print(y+" ");
			}
		System.out.println();
		}
	}
}


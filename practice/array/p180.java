//1 byte: -128 to 127

class Example{
	Integer a=new Integer(10);

	public static void main(String []args){
		int x=10;
		int y=10;
		Integer z=10;

		Example obj=new Example();

		System.out.println(System.identityHashCode(obj.a));//different

		System.out.println(System.identityHashCode(x));//same
		System.out.println(System.identityHashCode(y));//same
		System.out.println(System.identityHashCode(z));//same
	}
}


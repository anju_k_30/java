
class DArray{
	public static void main(String s[]){
		int arr[][]={{10,10},{10,10}};

		
		System.out.print(arr[1][1]);	//10
		System.out.println(arr[0]);	//[I@
		System.out.println(arr[1]);	//[I@
		System.out.println(arr);	//[[I@

		System.out.println(System.identityHashCode(arr[0][0]));		//same code for all
		System.out.println(System.identityHashCode(arr[0][1]));
		System.out.println(System.identityHashCode(arr[1][0]));
		System.out.println(System.identityHashCode(arr[1][1]));

	}
}


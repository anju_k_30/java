class TwoDarray{
	public static void main(String s[]){
		int oneDarr[]=new int [2];

		int arr[][]=new int [2][3];
		int arr1[][]=new int [3][];
//		int arr2[][]=new int [][3];	:error

		arr[0][0]=10;
		arr[0][1]=20;
		arr[0][2]=30;
		arr[1][0]=40;
		arr[1][1]=50;
		arr[1][2]=60;

		System.out.println(oneDarr.length);	//2(no.of elements)
		System.out.println(arr.length);		//2(no.of rows)


		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}
}


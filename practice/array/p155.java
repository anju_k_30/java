//code1:

class Demo{
	public static void main(String s[]){
		int arr1[];

		//int arr2[5];			size is decided by jvm(memory management)

		int arr3[]=new int[5];
		
	//	int arr4[]=new int[];		error: array dimension missing

		int arr5[]= new int[] {1,2,3,4,5};

//		int arr6[]=new int[5]{1,2,3,4,5};	error: array creation with both dimension expression and initialization is illegal


		int arr7[]={10,20,30};
		int arr8[]=new int[] {10,20,330};


	}

}	

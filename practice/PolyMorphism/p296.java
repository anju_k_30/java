class Parent{
	char fun(){
		System.out.println("Parent");
		return 'A';
	}
}
class Child extends Parent{
	/*int fun(){				//error: fun() in Child cannot override fun() in Parent

		System.out.println("Child");
		return 10;
	}*/
}

class Client{
	public static void main(String s[]){
		Parent obj=new Child();
		obj.fun();
	}
}

class Parent{
	public void fun(){				//Access specifier :public
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
//	void fun(){					//Access specifier :default
//							error: fun() in Child cannot override fun() in Parent
//							attempting to assign weaker access privileges; was public

	public void fun(){
		System.out.println("Child fun");
	}

}
class Client{
	public static void main(String args[]){
		Parent obj =new Child();
		obj.fun();
	}
}


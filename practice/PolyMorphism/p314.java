class Parent{
	void fun(){				//Access specifier :public
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
//	private void fun(){					//Access specifier :default
								//attempting to assign weaker access privileges; was package
	public void fun(){
		System.out.println("Child fun");
	}

}
class Client{
	public static void main(String args[]){
		Parent obj =new Child();
		obj.fun();
	}
}


class Parent{
	private void fun(){				//Access specifier :public
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
	void fun(){					//Access specifer :default
		System.out.println("Child fun");
	}

}
class Client{
	public static void main(String args[]){
		Parent obj =new Child();
	//	obj.fun();				//error: fun() has private access in Parent

	}
}


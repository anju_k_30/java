//Overloading/Compile time polymorphism/Early Binding

class Demo{					//Method Signature
	void fun(int x){			//fun(int)
		System.out.println(x);
	}

//	void fun(int x){			//error:error: method fun(int) is already defined in class Demo
//						even if the return type is different still error.
	void fun(float x){			//fun(float)
		System.out.println(x);
	}

	void fun(Demo obj){			//fun(Demo)
		System.out.println("In Demo Para");
		System.out.println(obj);	
	}

	public static void main(String s[]){
		Demo obj=new Demo();
		obj.fun(10);
		obj.fun(10.5f);

		Demo obj1=new Demo();
		obj1.fun(obj);
	}
}

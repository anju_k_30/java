class Parent{
 	void fun(){				
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
// 	static void fun(){				//error: fun() in Child cannot override fun() in Parent
						//overridding method is static
	void fun(){
		System.out.println("Child fun");
	}

}
class Client{
	public static void main(String args[]){
		Parent obj =new Child();
		obj.fun();
		
	}
}


//Method Hiding

class Parent{
 	static void fun(){				
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
 	static void fun(){				//error: fun() in Child cannot override fun() in Parent
							//overridding method is static
		System.out.println("Child fun");
	}

}
class Client{
	public static void main(String args[]){
		Parent obj =new Child();
		obj.fun();				//(Method Hiding)  Parent fun
		
		Child obj1=new Child();
		obj1.fun();				//Child fun

		Parent obj2=new Parent();
		obj2.fun();				//Parent fun
	}
}


//III.Je method override kryche ahe te same pahijel(method signature)

class Parent{
	Parent(){
		System.out.println("Parent");
	}
	void fun(int x){
		System.out.println("In Parent Fun");	
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child");
	}
	void fun(){
		System.out.println("In Child Fun");	
	}
}
class Demo{
	public static void main(String S[]){
		Parent obj=new Child();			
		//obj.fun();				//error: method fun in class Parent cannot be applied to given types;
		obj.fun(10);
	}
}
		

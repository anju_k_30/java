class Parent{
	StringBuffer fun(){
		System.out.println("Parent Fun");
		return new StringBuffer("Shashi");
	}
}

class Child extends Parent{
	/*String fun(){					//error: fun() in Child cannot override fun() in Parent
		System.out.println("Child Fun");
		return "Shashi";
	}*/
}
class Client{
	public static void main(String s[]){
		Parent obj=new Child();
		obj.fun();
	}
}

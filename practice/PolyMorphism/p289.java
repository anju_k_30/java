//II.

class Parent{
	Parent(){
		System.out.println("Parent");
	}
	void fun(){
		System.out.println("In Parent Fun");	//this method gets called
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child");
	}
	void fun(int x){
		System.out.println("In Child Fun");	
	}
}
class Demo{
	public static void main(String S[]){
		Parent obj=new Child();			
		obj.fun();
	}
}
		

class Demo{
	void fun(Object obj){
		System.out.println("Object");
	}
	void fun(String str){
		System.out.println("String");
	}
}
class Client{
	public static void main(String s[]){
		Demo obj=new Demo();
		obj.fun("C2W1");
		obj.fun(new StringBuffer("C2W2"));
		obj.fun(null);
	}
}

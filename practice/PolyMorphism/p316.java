class Parent{
	static void fun(){				
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
// 	void fun(){				//error: fun() in Child cannot override fun() in Parent
						//overridden method is static
	void gun(){
		System.out.println("Child fun");
	}

}
class Client{
	public static void main(String args[]){
	//	Parent obj =new Child();
	//	obj.fun();
		Child obj=new Child();
		obj.gun();
	}
}


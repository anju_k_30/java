class Parent{
	Parent(){
		System.out.println("Parent");
	}

	void fun(){
		System.out.println("In Fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child");
	}

	void gun(){
		System.out.println("In Gun");
	}
}
class Demo{
	public static void main(String s[]){
		Child obj=new Child();
		obj.fun();
		obj.gun();
		
		Parent obj1=new Parent();
		obj1.fun();
		//obj1.gun();		error:cannot find Symbol
	}
}


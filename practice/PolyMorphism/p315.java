class Parent{
	final void fun(){				//Access specifier :public
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
	//void fun(){					//error: fun() in Child cannot override fun() in Parent(compile time)
							//overridden method is final

	void gun(){
		System.out.println("Child fun");
	}

}
class Client{
	public static void main(String args[]){
		//Parent obj =new Child();
		//obj.fun();
		Child obj1=new Child();
		obj1.gun();
	}
}


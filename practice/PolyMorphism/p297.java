//Co-Varient relationship as a return type.

class Parent{
	Object fun(){
		System.out.println("Parent fun");
		Object obj=new Object();
		return obj;
		//return new Object();
	}
}

class Child extends Parent{
	String fun(){
		System.out.println("Child Fun");
		return "Shashi";
	}
}

class Client{
	public static void main(String s[]){
		Parent obj=new Child();
		obj.fun();
	}
}


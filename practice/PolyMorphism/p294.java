class Demo{
	void fun(String str){
		System.out.println("String");
	}
	void fun(StringBuffer str1){
		System.out.println("StringBuffer");
	}

}
class Client{
	public static void main(String s[]){
		Demo obj=new Demo();
		obj.fun("C2W");
		obj.fun(new StringBuffer("Core2Web"));
		//obj.fun(null);				//error: reference to fun is ambiguous

	}
}

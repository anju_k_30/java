//Child chya reference ne Parent chya method la call kru shakt nhi

/*
 At compile time, compiler checks only left side .So if that method is present regarding to the left side reference then no error else compiler gives 
 compile time error that is "Cannot find Symbol"

 Parent obj=new Child();

 Also if that method is present then compiler thinks that method is of parent class i.e o/p will be "In Parent Fun".But actually at run time object of 
 child class gets created and method of child class is called.(more preference to the child is given).

 */
class Parent{
	Parent(){
		System.out.println("Parent");
	}
	void fun(){
		System.out.println("In Parent Fun");
	}

}

class Child extends Parent{
	Child(){
		System.out.println("Child");
	}
	void fun(){
		System.out.println("In Child Fun");
	}
}

class Demo{
	public static void main(String s[]){
		Parent obj=new Child();
	//	Child obj1=new Parent();		error: incompatible types: Parent cannot be converted to Child
	//	obj1.fun();
		obj.fun();
	}
}


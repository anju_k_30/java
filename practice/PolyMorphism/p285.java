//OverRiding/Late Binding/Run time Polymorphism

//static methods can't be overridden
//if method signature is changed then we can't override that method

class Parent{
	Parent(){
		System.out.println("Parent Constructor");
	}

	void Property(){
		System.out.println("Home,car,gold");
	}
	void Marry(){
		System.out.println("xyz");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child Constructor");
	}

	void Marry(){
		System.out.println("pqr");
	}
}
class Demo{
	public static void main(String args[]){
		Child obj=new Child();
		obj.Property();
		obj.Marry();
	}
}





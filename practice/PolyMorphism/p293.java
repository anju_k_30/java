class Demo{
	void fun(int x,float y){
		System.out.println("Int-Float Para");
	}
	void fun(float x,int y){
		System.out.println("Float-Int Para");
	}
}
class Client{
	public static void main(String s[]){
		Demo obj=new Demo();
		obj.fun(10,100f);
		//obj.fun(10,10);			//error: reference to fun is ambiguous

	}
}

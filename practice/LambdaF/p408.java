//Lambda Function works on only Functional Interface

interface C2W{
	void lang();
}

class User{
	public static void main(String s[]){
		C2W obj=()->{
			System.out.println("abc");
		};
		obj.lang();
	}
}

//Lambda Function works on only Functional Interface

interface C2W{
	void lang();
}

class User{
	public static void main(String s[]){
		C2W obj=new C2W(){
			public void lang(){
				System.out.println("xyz");
			}
		};
		obj.lang();
	}
}

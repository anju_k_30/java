//code13: Logical

class Operator{
	public static void main(String []args){
		int x=5;
		int y=7;

		//int ans=x&&y;		...error:bad operand type for binary operator &&

		//int ans=x<y && y>x;	...error:incompatible types
		
		//boolean ans =x<y && y;  ..error:bad operand --||--

		boolean ans1=x<y && y<x;
		boolean ans2=x<y || y<x;

		System.out.println(ans1);
		System.out.println(ans2);

	}
}

//code17: negation (~)	//works on binary 

class Operator{
	public static void main(String []args){
		int x=5;	
		int y=2;
		int z=6;		
		int a=195;

		System.out.println(~x);	//-6
		System.out.println(~y);	//-3
		System.out.println(~z);	//-7
		System.out.println(~a);	//-196
					
	}
}
		//when we take 2nd compliment and adds one bit at LSB variable of negative no. then we get these ans (-6,-3,-7...)i.e negation of +ve no.
		
		/*int x=-3;
		 +ve binary-->0000 0011
		 1's complm-->1111 1100
		 	+     0000 0001
		 2's complm-->1111 1101


		 int x=2;
		 0000 0010
	negatn-->1111 1101


	negation of any +ve no. represents any negative no.(here 2 and -3 has same ans)

	
	(6 and -7 has-->1111 1001)
	*/

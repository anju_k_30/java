//code16: Bitwise 	//works on binary 

class Operator{
	public static void main(String []args){
		int x=8;	//1000
		int y=10;	//1010

		System.out.println(x<<2);	//32
		System.out.println(y>>2);	//2
	}
}
		/* 8-->1000
		  10-->1010
		
		  left shift by 2(x<<2)
		  0010 0000

		  right shift by 2(y>>2)
		  0000 0010

		*/

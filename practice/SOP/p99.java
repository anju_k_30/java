//code 4

class Core2web{
	public static void main(String []args){
		int x=10;
		{
//			int x=20;
			System.out.println(x);	//error: variable x is already defined in method main(String[])

		}
		{	
//			int x=30;
			System.out.println(x);	//error: variable x is already defined in method main(String[])

		}
			System.out.println(x);	//10
	}
}

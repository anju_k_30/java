//code 11:

class Demo{
	
	//int x=10;		...error:non-static variable x cannot be referenced from a static context
	static int x=10;

	public static void main(String [] args){
		int y=20;

		System.out.println(x);
		System.out.println(y);
	}
}


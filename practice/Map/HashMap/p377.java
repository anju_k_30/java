import java.util.*;
class HashMapMethods{
	public static void main(String s[]){
		HashMap hm=new HashMap();
		
		//1.put(k,v);
		hm.put("Java",".java");
		hm.put("Python",".py");
		hm.put("Dart",".dart");
		hm.put("CPP",".cpp");

		System.out.println(hm);

		//2.V get(Object);
		System.out.println(hm.get("Python"));

		//3.Set KeySet();
		System.out.println(hm.keySet());

		//4.Collection values();
		System.out.println(hm.values());

		//5.Set entrySet();
		System.out.println(hm.entrySet());
	}
}
/*
op:
{Java=.java, CPP=.cpp, Python=.py, Dart=.dart}
.py
[Java, CPP, Python, Dart]
[.java, .cpp, .py, .dart]
[Java=.java, CPP=.cpp, Python=.py, Dart=.dart]
*/

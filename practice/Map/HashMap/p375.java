import java.util.*;
class HashMapDemo{
	public static void main(String s[]){
		HashSet hs=new HashSet();

		hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Badhe");
		hs.add("Rahual");

		System.out.println(hs);		//[Rahual, Ashish, Badhe, Kanha]

	
		HashMap hm=new HashMap();
	/*
		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Badhe","CarPro");
		hm.put("Rahual","BMC");

		System.out.println(hm);		//{Rahual=BMC, Ashish=Barclays, Badhe=CarPro, Kanha=Infosys}
*/

		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Kanha","CarPro");
		hm.put("Rahual","BMC");

		System.out.println(hm);		//{Rahual=BMC, Ashish=Barclays, Kanha=CarPro}
		
	}
}


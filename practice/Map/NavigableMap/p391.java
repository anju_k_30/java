import java.util.*;

class NavigableMapDemo{
	public static void main(String s[]){
		NavigableMap nm=new TreeMap();
		nm.put(30,"abc");
		nm.put(14,"pqr");
		nm.put(20,"def");
		nm.put(27,"ghi");
		nm.put(25,"jkl");
		nm.put(24,"xyz");
		nm.put(23,"mno");

		System.out.println(nm);			//{14=pqr, 20=def, 23=mno, 24=xyz, 25=jkl, 27=ghi, 30=abc}

	//1.lowerKey
	System.out.println(nm.lowerKey(23));		//20
	System.out.println(nm.lowerKey(25));		//24
	System.out.println(nm.lowerKey(9));		//null
	
	//2.lowerEntry(K)
	System.out.println(nm.lowerEntry(23));		//20=def
	
	//3.floorKey
	System.out.println(nm.floorKey(23));		//23
	
	//4.floorEntry(K)
	System.out.println(nm.floorEntry(23));		//23=mno

	//5.ceilingKey
	System.out.println(nm.ceilingKey(23));		//23
	System.out.println(nm.ceilingKey(25));		//25
	
	//6.ceilingEntry(K)
	System.out.println(nm.ceilingEntry(23));	//23=mno

	//7.higherKey
	System.out.println(nm.higherKey(27));		//30

	//8.higherEntry(K)
	System.out.println(nm.higherEntry(23));		//24=xyz


	//9.FirstEntry()
	System.out.println(nm.firstEntry());		//14=pqr

	//9.lastEntry()
	System.out.println(nm.lastEntry());		//30=abc
	
	//9.pollFirstEntry()
	System.out.println(nm.pollFirstEntry());	//14=pqr
	
	//10.pollLastEntry()
	System.out.println(nm.pollLastEntry());		//30=abc

	System.out.println(nm);				//{20=def, 23=mno, 24=xyz, 25=jkl, 27=ghi}


	//11.descendingMap()
	System.out.println(nm.descendingMap());		//{27=ghi, 25=jkl, 24=xyz, 23=mno, 20=def}


	//12.navigableKeySet();
	System.out.println(nm.navigableKeySet());	//[20, 23, 24, 25, 27]


	//13.descendingKeySet()
	System.out.println(nm.descendingKeySet());	//[27, 25, 24, 23, 20]

	//14.subMap()
	System.out.println(nm.subMap(23,true,27,false));	//{23=mno, 24=xyz, 25=jkl}

	
	//15.headMap()
	System.out.println(nm.headMap(25,true));	//{20=def, 23=mno, 24=xyz, 25=jkl}
	System.out.println(nm.headMap(25,false));	//{20=def, 23=mno, 24=xyz}
	System.out.println(nm.headMap(24,true));	//{20=def, 23=mno, 24=xyz}

	//16.tailMap()
	System.out.println(nm.headMap(30,true));	//{20=def, 23=mno, 24=xyz, 25=jkl, 27=ghi}
	System.out.println(nm.headMap(27,false));	//{20=def, 23=mno, 24=xyz, 25=jkl}
	System.out.println(nm.headMap(14,true));	//{}

	//17.subMap(K,K)
	System.out.println(nm.subMap(23,30));		//{23=mno, 24=xyz, 25=jkl, 27=ghi}

	//18.headMap(K)
	System.out.println(nm.headMap(14));		//{}
	System.out.println(nm.headMap(23));		//{20=def}

	//19.tailMap()
	System.out.println(nm.tailMap(30));		//{}
	System.out.println(nm.tailMap(25));		//{25=jkl, 27=ghi}


	}
}

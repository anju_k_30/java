import java.util.*;
import java.io.*;

class PropertiesDemo{
	public static void main(String s[])throws IOException{
		Properties obj=new Properties();
		
		FileInputStream fobj=new FileInputStream("389.properties");
		obj.load(fobj);

		String name=obj.getProperty("Ashish");
		System.out.println(name);

		obj.setProperty("Anju","abc");

		FileOutputStream opObj=new FileOutputStream("389.properties");
		obj.store(opObj,"Updated By Anjali");
	}
}

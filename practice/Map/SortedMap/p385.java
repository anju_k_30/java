import java.util.*;
class SortedMapDemo{
	public static void main(String s[]){
		SortedMap sm=new TreeMap();
		sm.put("Ind","India");
		sm.put("Pak","Pakisthan");
		sm.put("Sl","SriLanka");
		sm.put("Aus","Australia");
		sm.put("Ban","Bangladesh");

		System.out.println(sm);

	//1.subMap
	System.out.println(sm.subMap("Aus","Pak"));
	
	//2.headMap
	System.out.println(sm.headMap("Pak"));

	//3.tailMap
	System.out.println(sm.tailMap("Pak"));

	//4.firstKey
	System.out.println(sm.firstKey());

	//5.keySet
	System.out.println(sm.keySet());

	//6.values
	System.out.println(sm.values());
	
	//7.entrySet
	System.out.println(sm.entrySet());
	}
}

/*
 op
 {Aus=Australia, Ban=Bangladesh, Ind=India, Pak=Pakisthan, Sl=SriLanka}
{Aus=Australia, Ban=Bangladesh, Ind=India}
{Aus=Australia, Ban=Bangladesh, Ind=India}
{Pak=Pakisthan, Sl=SriLanka}
Aus
[Aus, Ban, Ind, Pak, Sl]
[Australia, Bangladesh, India, Pakisthan, SriLanka]
[Aus=Australia, Ban=Bangladesh, Ind=India, Pak=Pakisthan, Sl=SriLanka]
*/



//Iterating Over Map (Using EntrySet)
import java.util.*;
class SortedMapDemo{
	public static void main(String s[]){
		SortedMap sm=new TreeMap();
		sm.put("Ind","India");
		sm.put("Pak","Pakisthan");
		sm.put("Sl","SriLanka");
		sm.put("Aus","Australia");
		sm.put("Ban","Bangladesh");

		System.out.println(sm);

	Set<Map.Entry>data=sm.entrySet();
	System.out.println(data);

	Iterator<Map.Entry>itr=data.iterator();

	while(itr.hasNext()){
		//System.out.println(itr.next());
		Map.Entry abc=itr.next();
		System.out.println(abc.getKey() + ":" + abc.getValue());
	
	}
	}
}
/*
 * op
 {Aus=Australia, Ban=Bangladesh, Ind=India, Pak=Pakisthan, Sl=SriLanka}
[Aus=Australia, Ban=Bangladesh, Ind=India, Pak=Pakisthan, Sl=SriLanka]
Aus:Australia
Ban:Bangladesh
Ind:India
Pak:Pakisthan
Sl:SriLanka
*/

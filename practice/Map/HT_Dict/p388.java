import java.util.*;
class DictionaryDemo{
	public static void main(String s[]){
		Dictionary ht=new Hashtable();
		
		ht.put(10,"Sachin");
		ht.put(7,"MSD");
		ht.put(18,"Virat");
		ht.put(1,"KLRahual");
		ht.put(45,"Rohit");

		System.out.println(ht);		//{10=Sachin, 18=Virat, 7=MSD, 45=Rohit, 1=KLRahual}
		
	//1.keys
	Enumeration itr1=ht.keys();
	while(itr1.hasMoreElements())
		System.out.println(itr1.nextElement());

	//2.elements
	Enumeration itr2=ht.elements();
	while(itr2.hasMoreElements())
		System.out.println(itr2.nextElement());

	//3.get(E)
	System.out.println(ht.get(10));

	//4.remove
	ht.remove(1);
	System.out.println(ht);


	}
}

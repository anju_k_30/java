import java.util.*;
class Demo{

	String str;
	Demo(String str){
		this.str=str;
	}

	public String toString(){
		return str;
	}

	public void finalize(){
		System.out.println("Notify");
	}
}

class GCDemo{
	public static void main(String s[]){
		Demo obj1=new Demo("C2W");
		Demo obj2=new Demo("BienCaps");
		Demo obj3=new Demo("Incubator");

		WeakHashMap hm=new WeakHashMap();
		
		hm.put(obj1,2016);
		hm.put(obj2,2019);
		hm.put(obj3,2023);

		obj1=null;

		System.gc();
		System.out.println(hm);
	}
}
/*
 * op:
 C2W
BienCaps
Incubator
In main
Notify
Notify

 */

//code 1,2,9

class Core2web{
	public static void main(String []args){
		System.out.println("Before for");

//		for(int i,j;i<3;i++){		//error: variable i might not have been initialized
		
//		for(int i=0,j=0;i<1;j++){	//infinite loop i.e Inside for.......

//		for(int i,j;i<3;i++,j++){	//error: variable i might not have been initialized
						//error: variable j might not have been initialized


		for(int i=1,j;i<3;i++){
			System.out.println("Inside for");
		}
			
		System.out.println("After for");
	}
}
		


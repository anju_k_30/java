//code: 2&8

class Core2web{
	
	//public static void main(String[] s[]){	:runtime Error: Main method not found in class Core2web, please define the main method as:

	public static void main(String s[]){
		int x=2;
		while(x>1){		
			//while(true)			//Condition never fails so it goes in infinte loop

			System.out.println("Hello,Core2web!");	
			x=1;
	}
	}
}

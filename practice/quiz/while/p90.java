//code:4 &5

class Core2web{
	public static void main(String s[]){
		int var=5;
		System.out.println("Inside main");	//Inside main
		
	//	while(var>3);{				//while statement is an empty statement but then condition given with it is true so this 
							//unexpected behaviour happens
							//only prints inside main(code goes into infinite loop with no op)

		while(var-->3);{
		System.out.println("Inside while");	//Inside while
		}

	}
}

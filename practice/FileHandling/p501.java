import java.io.*;
class Demo{
	public static void main(String s[])throws IOException{
		File fobj=new File("Incubator.txt");
		fobj.createNewFile();

		//1.String getName();
		System.out.println(fobj.getName());	//Incubator.txt

		//2.String getParent();
		System.out.println(fobj.getParent());	//null

		//3.File getParentFile();
		System.out.println(fobj.getParentFile());	//null
							
		//4.String getPath();
		System.out.println(fobj.getPath());		//Incubator.txt

		//5.String getAbsolutePath();
		//calls internally pwd
		System.out.println(fobj.getAbsolutePath());	///home/anjali/core2web/java/practice/FileHandling/Incubator.txt
		
		//6.boolean canRead();
		System.out.println(fobj.canRead());		//true

		//7.boolean canWrite();
		System.out.println(fobj.canWrite());		//true
								
		//8.boolean isDirectory();
		System.out.println(fobj.isDirectory());		//false

		//9.long lastModified();
		System.out.println(fobj.lastModified());	//1705250998137

		//10.boolean delete();
		//System.out.println(fobj.delete());

		//11.String[] list();
		System.out.println(fobj.list());		//null

		File obj=new File("Demo.txt");
                obj.createNewFile();
		//12.int compareTo(File)
		System.out.println(fobj.compareTo(obj));	
		System.out.println(fobj.compareTo(fobj));	

		//13.boolean isFile()
		System.out.println(fobj.isFile());		//true

	}
}


class Demo{
	int x=10;
	private int y=20;

	void fun(){
		System.out.println(x);
		System.out.println(y);
	}
}

class User{
	public static void main(String s[]){
		Demo obj=new Demo();

		obj.fun();
		System.out.println(obj.x);
	//	System.out.println(obj.y);		//error: y has private access in Demo

		
	//	System.out.println(x);
	//	System.out.println(y);
	}
}




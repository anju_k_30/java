class Demo{
	int x=10;
	static int y=20;

	static{
		System.out.println("Static Block 1");
		Demo obj=new Demo();
		System.out.println(obj.x);

	}
	public static void main(String s[]){
		System.out.println("Main Block");
		Demo obj=new Demo();
		
		System.out.println(obj.x);
	}
	static{
		System.out.println("Static Block 2");
		System.out.println(y);
	}
}


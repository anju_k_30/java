class Demo{
	Demo(){
		System.out.println("IN CONSTRUCTOR");
	}
	void fun(){
		Demo obj=new Demo();
		System.out.println(obj);
	}
	public static void main(String s[]){
		Demo obj1=new Demo();
		System.out.println(obj1);
		
		Demo obj2=new Demo();
		System.out.println(obj2);
		
		obj1.fun();
		System.out.println(obj1);
		
	}
}
/*
 * OUTPUT:

IN CONSTRUCTOR
Demo@2f0a87b3
IN CONSTRUCTOR
Demo@319b92f3
IN CONSTRUCTOR
Demo@fcd6521
Demo@2f0a87b3
 
*/

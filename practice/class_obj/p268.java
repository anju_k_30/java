class Demo{
	int x=10;
	Demo(){			//Demo(Demo this)
		System.out.println("In Constructor");
		System.out.println(this);	//0x100
		System.out.println(x);
		System.out.println(this.x);
	}
	void fun(){		//fun(Demo this)
		System.out.println(this);	//0x100
		System.out.println(x);
		System.out.println(this.x);
	}
	public static void main(String s[]){
		Demo obj=new Demo();		//Demo(obj)
		System.out.println(obj);	//0x100
		
		obj.fun();	//fun(obj)
	}
}
/*
output:
In Constructor
Demo@1dbd16a6
10
10
Demo@1dbd16a6
Demo@1dbd16a6
10
10
*/
		

class Demo{
	int x=10;		//non-static variable/instance variable
	static int y=20;	//static variable/class variable
				
	Demo(){			//constructor
		System.out.println("Constructor");
	}
	{
		System.out.println("Instance Block 1");		//Instance block
	}
	void fun(){				//non static method/instance method
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
	}
	int z=30;	//instance variable

	void gun(){		 //non static method/instance method
		int p=30;		//local variable
		fun();
	}
	
	public static void main(String s[]){			//main method=static method
		Demo obj=new Demo();
		obj.gun();
		System.out.println("Main");
	}
	{
		System.out.println("Instance Block 2");
	}
}

/*
Instance Block 1
Instance Block 2
Constructor
10
20
30
Main
*/



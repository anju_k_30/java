class Demo{
	static{
		System.out.println("Static Block 1");
	}
	public static void main(String s[]){
		System.out.println("In Demo Main");
	}
}
class Client{
	static{
		System.out.println("Static Block 2");
	}
	public static void main(String s[]){
		System.out.println("In Client Main");
	}
	static{
		System.out.println("Static Block 3");
	}
}

/*java Client			
 
Static Block 2
Static Block 3
In Client Main


java Demo

Static Block 1
In Demo Main
*/



class Demo{
	int x=10;
	Demo(){
//		this(10);				error: recursive constructor invocation
		System.out.println("In No Args");
	}
	Demo(int x){
		this();
//		super();				error: call to super must be first statement in constructor
		System.out.println("In Para Constructor");
//		this();					error: call to this must be first statement in constructor
	}
	public static void main(String s[]){
		Demo obj=new Demo(50);
	}
}


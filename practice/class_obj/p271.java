class Demo{
	int x=10;
	Demo(){							//Demo(Demo this)
		System.out.println("In No Args Constructor");
		System.out.println(this);
		System.out.println(this.x);
		System.out.println(x);
	}
	Demo(int x){						//Demo(Demo this,int x)
		System.out.println("In Para Constructor");
		System.out.println(this);
		System.out.println(this.x);
		System.out.println(x);
	}

	public static void main(String s[]){
		Demo obj1=new Demo();				//Demo(obj1)
		Demo obj2=new Demo(50);				//Demo(obj2,50)
	}
}

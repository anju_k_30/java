class Demo{
	Demo(){
		System.out.println("No Args");
	}
	Demo(int x){
		System.out.println("In Para");
	}
	Demo(Demo xyz){
		System.out.println("In Para Demo");
	}
	public static void main(String s[]){
		Demo obj1=new Demo();
		Demo obj2=new Demo(10);
		Demo obj3=new Demo(obj2);
	}
}

		


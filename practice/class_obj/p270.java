//Method Table(containing method signature which are uniquely stored)

class Demo{
	int x=10;

	Demo(){
		System.out.println("In Constructor 1");
	}
/*	Demo(){						//error: constructor Demo() is already defined in class Demo
		System.out.println("In Constructor 2");
	}*/
}

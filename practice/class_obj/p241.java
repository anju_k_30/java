class Demo{
	int age=10;			
	int jerNo=1000;
	static int x=20;

	void fun(){
		int x=10;
	}
}
/*

class Demo {
  int age;

  int jerNo;

  static int x;

  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #7                  // Field age:I
      10: aload_0
      11: sipush        1000
      14: putfield      #13                 // Field jerNo:I
      17: return

  void fun();
    Code:
       0: bipush        10
       2: istore_1
       3: return

  static {};
    Code:
       0: bipush        20
       2: putstatic     #16                 // Field x:I
       5: return
*/

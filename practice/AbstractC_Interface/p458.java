abstract class Parent{
	abstract void marry();		// error: missing method body, or declare abstract
	
	void career(){
		System.out.println("Doctor");
	}
}
class Child extends Parent{
	void marry(){
		System.out.println("xyz");
	}
}
class Client{
	public static void main(String s[]){
		Parent obj=new Child();	
		
		//Parent obj=new Parent();	//error: Parent is abstract; cannot be instantiated
		obj.marry();
		obj.career();
	}
}

interface Demo{
	static void fun(){
		System.out.println("In Fun");
	}
}
class DemoChild implements Demo{
	void fun(){
		System.out.println("In funChild");
	}
}
class Client{
	public static void main(String s[]){
		DemoChild obj=new DemoChild();
	//	Demo obj2=new DemoChild();
		
		obj.fun();
		
	//	obj2.fun();	// error: illegal static interface method call
		
	}
}

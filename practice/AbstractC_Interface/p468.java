interface Demo{
	static void fun(){
		System.out.println("In Fun");
	}
}
class DemoChild implements Demo{
	//static is not inherited
}
class Client{
	public static void main(String s[]){
		DemoChild obj=new DemoChild();
		Demo obj2=new DemoChild();
		//obj.fun();	//cannot find symbol
		
		//obj2.fun();	// error: illegal static interface method call
		
		Demo.fun();
	}
}

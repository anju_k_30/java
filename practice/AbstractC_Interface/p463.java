interface Demo{
	void fun();		//By default==>public abstract astat
	void gun();
}
abstract class ChildDemo implements Demo{
	//void fun(){
	public void fun(){
		System.out.println("In Fun");
	}
}

class DemoChild extends ChildDemo{

	//void gun(){
	public void gun(){
		System.out.println("In Gun");
	}
}

class Client{
	public static void main(String s[]){
		Demo obj=new DemoChild();
		obj.fun();
		obj.gun();
	}
}


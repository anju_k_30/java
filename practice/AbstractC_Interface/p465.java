//Static and default:came in 1.8 version

interface Demo1{

	default void fun(){
		System.out.println("Demo Fun");
	}
}
interface Demo2{

	default void fun(){
		System.out.println("Demo Fun");
	}
}

//class DemoChild implements Demo1,Demo2{
						//error:no overriden so ambiguity
//}

class DemoChild implements Demo1,Demo2{

	public void fun(){
		System.out.println("Demo Fun Demo");
	}
}

class Client{
	public static void main(String s[]){
		DemoChild obj=new DemoChild();
		obj.fun();

		Demo1 obj1=new DemoChild();
		obj1.fun();

		Demo2 obj2=new DemoChild();
		obj2.fun();
	}
}

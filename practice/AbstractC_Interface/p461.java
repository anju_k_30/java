interface Demo{
	void fun();
	
	abstract void man();

//	void gun(){		// error: interface abstract methods cannot have body

//	}
}
class Client{
	public static void main(String s[]){
	
		//Demo obj=new Demo();		//error: Demo is abstract; cannot be instantiated

	}
}

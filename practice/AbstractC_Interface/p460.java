//SingleTon Design Pattern

class SingleTon{

	static SingleTon obj=new SingleTon();

	static SingleTon getObject(){
		return obj;
	}

	private SingleTon(){
		System.out.println("Constructor");
	}
}

class Client{

	public static void main(String s[]){
	
		SingleTon obj1=SingleTon.getObject();
		System.out.println(obj1);
	
		SingleTon obj2=SingleTon.getObject();
		System.out.println(obj2);
	
		SingleTon obj3=SingleTon.getObject();
		System.out.println(obj3);
	
	}
}

interface Demo{
	void gun();
	default void fun(){
		System.out.println("In fun Demo");
	}
}
class DemoChild implements Demo{
	public void gun(){
		System.out.println("In gun-DemoChild");
	}
}

class Client{
	public static void main(String s[]){
		Demo obj=new DemoChild();
		obj.fun();
		obj.gun();
	}
}

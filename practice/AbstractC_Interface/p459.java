abstract class Army{
	void NDA(){
		System.out.println("Protection And Training");
	}
	abstract void mission();
}

class Defence extends Army{
	void mission(){
		System.out.println("Red Operation");
	}
}

class User{
	public static void main(String s[]){
		Defence care=new Defence();
		care.NDA();
		care.mission();
	}
}

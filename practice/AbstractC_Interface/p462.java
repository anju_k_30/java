interface Demo{
	void fun();		//By default==>public abstract astat
	void gun();
}
class DemoChild implements Demo{
	//void fun(){
	public void fun(){
		System.out.println("In Fun");
	}

	//void gun(){
	public void gun(){
		System.out.println("In Gun");
	}
}

class Client{
	public static void main(String s[]){
		Demo obj=new DemoChild();
		obj.fun();
		obj.gun();
	}
}
/*
 error: gun() in DemoChild cannot implement gun() in Demo
	void gun(){
	     ^
  attempting to assign weaker access privileges; was public
p462.java:6: error: fun() in DemoChild cannot implement fun() in Demo
	void fun(){
	     ^
  attempting to assign weaker access privileges; was public
*/

interface Demo1{
	static void fun(){
		System.out.println("In Fun demo1");
	}
}

interface Demo2{
	static void fun(){
		System.out.println("In Fun demo2");
	}
}


class DemoChild implements Demo1,Demo2{
	void fun(){
		System.out.println("In funChild");
		Demo1.fun();
		Demo2.fun();
	}
}
class Client{
	public static void main(String s[]){
		DemoChild obj=new DemoChild();
//		Demo obj2=new DemoChild();	//incompatible type
		
		obj.fun();
		
//		obj2.fun();	// error: illegal static interface method call
		
	}
}

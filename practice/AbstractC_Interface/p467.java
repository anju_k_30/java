class Demo{
	static void fun(){
		System.out.println("In Fun");
	}
}
class DemoChild extends Demo{
	//static is inherited
}
class Client{
	public static void main(String s[]){
		DemoChild obj=new DemoChild();
		obj.fun();
	}
}

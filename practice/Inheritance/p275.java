class ICC{
	ICC(){					//	Method java/lang/Object."<init>":()V

		System.out.println("In ICC Constructor");
	}
}
class BCCI extends ICC{
	BCCI(){					//	Method ICC."<init>":()V

		System.out.println("In BCCI Constructor");
	}
}
class IPL extends BCCI{
	IPL(){					//	Method BCCI."<init>":()V

		System.out.println("In IPL Constructor");
	}
}

class Client{
	public static void main(String s[]){
						//	Method java/lang/Object."<init>":()V
		IPL Player=new IPL();
	}
}

class Parent{
	static{
		System.out.println("In Parent Static");
	}
}
class Child extends Parent{
	static{
		System.out.println("In Child Static Block");
	}
}
class Client{
	public static void main(String s[]){
		Child obj=new Child();
	}
}

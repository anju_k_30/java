class Parent{
	int x=10;
	static int y=20;

	static{
		System.out.println("Parent Static Block");
	}
	Parent(){
		System.out.println("In Constructor");
	}
	void Method1(){
		System.out.println(x);
		System.out.println(y);
	}
	static void Method2(){
		System.out.println(y);
	}
}

class Child extends Parent{
	static{
		System.out.println("Child Static Block");
	}
	Child(){
		System.out.println("In Child Constructor");
	}
}

class Client{
	public static void main(String s[]){
		Child obj=new Child();
		obj.Method1();
		obj.Method2();
	}
}



		


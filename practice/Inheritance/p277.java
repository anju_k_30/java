class Rice{
	static int Mem=6;				//static Variable
	int plates=6;					//instance Variable

	Rice(){
		System.out.println("Variety of Rice");	//Rice(this)
	}
	void Cooking(){					//Cooking(this)
		float quantity=2000f;			//local Variable
		System.out.println("Members are "+Mem+" and Quantity of Rice is "+quantity+" grams");
		System.out.println("Tasty Rice");
	}
}
class Biryani extends Rice{

	Biryani(){					//Biryani(Biryani this)
		System.out.println("***Veg***");
		System.out.println("Jeera Rice,Pulav,Masala Rice");
	}
	Biryani(int plates){				//Biryani(Biryani this,int)
		this();
		this.plates=plates;
		System.out.println("***Non-Veg***");
		System.out.println("Chicken Biryani,Mutton Biryani");
	}
}

class User{
	public static void main(String s[]){

		Rice dish1=new Biryani(6);		//Biryani(dish1,7)
		System.out.println();
	//	Object dish2=new Biryani();
		dish1.Cooking();			//Cooking(dish1)
	}
}

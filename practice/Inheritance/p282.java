//To obtain parent class data using super keyword.
//super() only allowed in constructor 
//super is variable which is non-static(related to parent class)

class Parent{
	int x=10;
	static int y=20;

	Parent(){
		System.out.println("Parent");
	}

}
class Child extends Parent{
	int x=100;
	static int y=200;


	Child(){
		System.out.println("Child");
		//System.out.println(super);		//error
		//System.out.println(Parent);		//error
		//System.out.println(Parent.x);		//error: non-static variable x cannot be referenced from a static context


	}

	void access(){
		System.out.println(super.x);		//Parent.x
		System.out.println(super.y);		//Parent.y
		System.out.println(x);			//SOP(this.x)
		System.out.println(y);			//Sop(this.y)
	}
}
class Client{
	public static void main(String s[]){
		Child obj=new Child();
		obj.access();
	}
}
/*
output:
Parent
Child
10
20
100
200
*/

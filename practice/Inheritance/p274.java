/*
 private class IPL{		error: modifier private not allowed here

}


protected class IPL{		error: modifier protected not allowed here

}

static class IPL{		error: modifier static not allowed here

}

*/
class IPL{			//default constructor=> IPL()
	
	IPL(){
	}
	public IPL(int x){
	}
	private IPL(float y){
	}
	protected IPL(byte x){
	}
/*	static IPL(short x){		error: modifier static not allowed here

	}
*/
}
/*
public class IPL{		error: class IPL is public, should be declared in a file named IPL.java

}
*/
public class p274{		//public constructor=>public p274()
}

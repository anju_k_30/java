class Parent{
	static int x=10;
	Parent(){
		System.out.println("In Constructor");
	}
	static{
		System.out.println("In Parent Static Block");
	}
	static void access(){
		System.out.println(x);
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In Child COnstructor");
	}

	static{
		System.out.println("In Child Static Block");
		System.out.println(x);
		access();
	}
}
class Client{
	public static void main(String s[]){
		System.out.println("In Main");
		Child obj=new Child();
	}
}


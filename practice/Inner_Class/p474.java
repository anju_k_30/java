//2.Method Local Inner Class

class Outer{

	void m1(){
		
		System.out.println("In m1-Outer");

		//Method local innerclass
			class Inner{
				void m1(){
					System.out.println("In m1-Inner");
				}
			}

	Inner obj2=new Inner();
	obj2.m1();

	}

	void m2(){
		System.out.println("In m2-Inner");
	}
}

class Client{
	public static void main(String s[]){
	
		Outer obj=new Outer();
		obj.m1();
		obj.m2();

	}
}
  

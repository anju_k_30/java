class Parent{
	int x=10;
	static int y=20;

	void m1(){
		System.out.println("In M1");
	}

	static void m2(){
		System.out.println("In M2");
	}

	Parent(){
		System.out.println("In Parent constructor");
	}

}
class Child extends Parent{
	int a=40;
	static int b=50;

	Child(){
		System.out.println("In child Constructor1");
	}

	static void m3(){
		System.out.println("In m3");
	}

	void m1(){
		System.out.println("In child m1");
	}
}

class Client{
	public static void main(String s[]){
		Parent obj1=new Parent();
		obj1.m1();
		obj1.m2();

		Child obj2=new Child();
		obj2.m1();
		obj2.m3();

		Parent obj3=new Child();
		obj3.m1();
		//obj3.m3();
	}
}



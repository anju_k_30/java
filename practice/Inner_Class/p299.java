//1.Normal Inner Class

class Outer{
	class Inner{
		void fun2(){
			System.out.println(this);
			System.out.println("Fun2-Inner");
		}
	}
	void fun1(){
		System.out.println(this);
		System.out.println("Fun1-Outer");
	}
}
class Client{
	public static void main(String s[]){
		Outer obj=new Outer();
		obj.fun1();

		Outer.Inner obj1=obj.new Inner();
		obj1.fun2();

		Outer.Inner obj2=new Outer().new Inner();
		obj2.fun2();
	}
}

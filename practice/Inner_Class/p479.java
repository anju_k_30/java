//3.Static Inner class/Static Nested class

class Outer{
	void m1(){
		System.out.println("In m1-outer");
	}
	static class Inner{
		void m1(){
			System.out.println("In m1-Inner");
		}
	}
}

class Client{
	public static void main(String s[]){
		Outer.Inner obj=new Outer.Inner();
		obj.m1();
	}
}

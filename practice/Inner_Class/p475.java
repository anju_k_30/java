
//Inner class can access outer class members
class Outer{
	int x=10;
	static int y=20;

	class Inner{

		void fun2(){
			System.out.println("Fun2-Inner");
			System.out.println(x);
			System.out.println(y);
			fun1();
		}

	}
	
	void fun1(){
		System.out.println("In fun1-Outer");
	}
}

class Client{
	public static void main(String s[]){
		Outer obj=new Outer();
		obj.fun1();

		Outer.Inner obj2=obj.new Inner();
		obj2.fun2();
	}
}
/*
 In fun1-Outer
Fun2-Inner
10
20
In fun1-Outer
*/

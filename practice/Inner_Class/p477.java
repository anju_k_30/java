
class Outer{
	int x=10;
	static int y=20;

	class Inner{
		int a=30;
		static int b=40;
		final static int q=50;
	}

}
class Client{
	public static void main(String s[]){
		Outer obj=new Outer();
		Outer.Inner obj2=obj.new Inner();
	}
}

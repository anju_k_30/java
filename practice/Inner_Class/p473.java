class Outer{
	class Inner{
		void m1(){
			System.out.println("In m1-Inner");
		}
	}

	void m2(){
		System.out.println("In m2-Inner");
	}
}

class Client{
	public static void main(String s[]){
		//Inner obj1=new Inner();	//cannot find symbol
		//obj1.m2();
		
		Outer obj=new Outer();
		obj.m2();

		Outer.Inner obj2=obj.new Inner();
		//Outer.Inner obj2=new Outer().new Inner();
		obj2.m1();
	}
}

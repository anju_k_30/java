
class Outer{
	int x=10;
	static int y=20;

	Outer(){
		System.out.println("In Outer Constructor");
	}

	class Inner{

		Inner(){
			System.out.println("In Inner constructor");
		}

		final static int a=50;
	}

}
class Client{
	public static void main(String s[]){
		
		System.out.println(Outer.y);

		Outer obj=new Outer();
		System.out.println(obj.x);
		
		//Outer.Inner obj2=obj.new Inner();
		System.out.println(obj.new Inner().a);

	}
}

//Passing and Returning Values from a method

class FunDemo{
	public static void main(String s[]){
		FunDemo obj=new FunDemo();
		
		int ret=obj.fun(10);
		System.out.println(ret);
	}

	//	void fun(int x){
		int fun(int x){
			int val=x+50;
			return val;
		}
	
}

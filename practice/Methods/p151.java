class Demo{
	void fun(int x){
		System.out.println(x);
	}
	public static void main(String s[]){
		System.out.println("In main");
		Demo obj= new Demo();
		//obj.fun();		 error: method fun in class Demo cannot be applied to given types;
		obj.fun(20);		// if no parameter to fun:error: method fun in class Demo cannot be applied to given types;

		System.out.println("End Main");
	}
}

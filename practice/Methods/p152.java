class Demo{
	public static void main(String s[]){
		Demo obj=new Demo();
		obj.fun(10);
		//obj.fun(10.5f);	error: incompatible types: possible lossy conversion from float to int
		//obj.fun(true);	error: incompatible types: boolean cannot be converted to int

		obj.gun(10);
		obj.gun(10.5f);
		obj.gun('A');
	
	}
	void fun(int x){
		System.out.println("In Fun");
		System.out.println(x);
	}

	void gun(float y){
		System.out.println("In Gun");
		System.out.println(y);
	}
}


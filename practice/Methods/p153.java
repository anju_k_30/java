class Demo{
	int fun(int x){
		return x+10;
	}
	void gun(int y){
		int x=y+10;
	}
	public static void main(String s[]){
		Demo obj=new Demo();
		System.out.println(obj.fun(10));
		//System.out.println(obj.gun(10));		error: 'void' type not allowed here

		//int a=obj.gun(10);				error: incompatible types: void cannot be converted to int
		//System.out.println(a);
	}
}



class MethodDemo{
	public static void main(String s[]){
		fun();
	//	gun();		error: non-static method gun() cannot be referenced from a static context
		
		MethodDemo obj=new MethodDemo();
		obj.gun();
	}
	static void fun(){
		System.out.println("In FUN");
	}
	void gun(){
		System.out.println("In GUN");
	}
}


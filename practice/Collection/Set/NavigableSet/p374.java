import java.util.*;
class NavigableSetDemo{
	public static void main(String s[]){
		NavigableSet ns=new TreeSet();

		ns.add("Anju");
		ns.add("Bhakti");
		ns.add("Mane");
		ns.add("Tanu");
		ns.add("Sakhu");
		ns.add("Sagya");
		ns.add("Panya");
		ns.add("Vaibhya");

		System.out.println(ns);		//[Anju, Bhakti, Mane, Panya, Sagya, Sakhu, Tanu, Vaibhya]

		NavigableSet ns1=new TreeSet();

		ns1.add(2);
		ns1.add(5);
		ns1.add(12);
		ns1.add(10);
		ns1.add(2);
		ns1.add(50);
		ns1.add(20);
		
		System.out.println(ns1);		//[2, 5, 10, 12, 20, 50]

		//1.E lower(E):returns greatest element but from the set which is less than given Element
		
		System.out.println(ns.lower("Panya"));		//mane
		System.out.println(ns1.lower(6));		//5
		
		//2.E floor(E):returns greatest element from the set which is less than or equal to given Element

		System.out.println(ns.floor("Panya"));		//panya
		System.out.println(ns1.floor(13));		//12
		
		//3.E ceiling(E):returns least element from the set which is less than or equal to given Element

		System.out.println(ns.ceiling("Panya"));	//panya
		System.out.println(ns1.ceiling(13));		//20

		//4.E higher(E):returns least element from the set which is greater than given Element

		System.out.println(ns.higher("Panya"));		//sagya
		System.out.println(ns1.higher(20));		//50

		//5.E pollFirst():removes the first lowest element in set
		
		System.out.println(ns.pollFirst());		//anju
		System.out.println(ns1.pollFirst());		//2
		System.out.println(ns);				//[Bhakti, Mane, Panya, Sagya, Sakhu, Tanu, Vaibhya]
		System.out.println(ns1);			//[5, 10, 12, 20, 50]

		//6.E pollLast():removes the first lowest element in set

		System.out.println(ns.pollLast());		//vaibhya
		System.out.println(ns1.pollLast());		//50
		System.out.println(ns);				//[Bhakti, Mane, Panya, Sagya, Sakhu, Tanu]
		System.out.println(ns1);			//[5, 10, 12, 20]

		//7.NavigableSet<E> subSet(E, boolean, E, boolean);
		System.out.println(ns.subSet("Sagya",false,"Tanu",true));	//[Sakhu, Tanu]

		//8.NavigableSet<E> headSet(E, boolean);
		System.out.println(ns.headSet("Panya",true));	//[Bhakti, Mane, Panya]	

		//9.NavigableSet<E> tailSet(E, boolean);
		System.out.println(ns.tailSet("Panya",true));	//[Panya, Sagya, Sakhu, Tanu]


	}
}

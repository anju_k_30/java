import java.util.*;
class NavigableSetDemo{
	public static void main(String s[]){
		NavigableSet ns=new TreeSet();
		ns.add(10);
		ns.add(30);
		ns.add(14);
		ns.add(27);
		ns.add(23);

		System.out.println(ns);

	//1.lower
	System.out.println(ns.lower(23));
	System.out.println(ns.lower(25));
	System.out.println(ns.lower(9));

	//2.floor
	System.out.println(ns.floor(23));

	//3.ceiling
	System.out.println(ns.ceiling(23));
	System.out.println(ns.ceiling(25));

	//4.higher
	System.out.println(ns.higher(27));

	//5.pollFirst
	System.out.println(ns.pollFirst());
	
	//6.pollLast
	System.out.println(ns.pollLast());

	System.out.println(ns);

	//7.descendingSet
	System.out.println(ns.descendingSet());

	//8.iterator
	Iterator itr=ns.iterator();
	while(itr.hasNext())
		System.out.println(itr.next());

	//9.descendingIterator
	Iterator itr1=ns.iterator();
	while(itr1.hasNext())
		System.out.println(itr1.next());

	//10.subSet
	System.out.println(ns.subSet(10,true,27,false));
	}
}
	
		

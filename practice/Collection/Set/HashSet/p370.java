import java.util.*;
class HashSetDemo{
	public static void main(String s[]){
		HashSet hs=new HashSet();

		hs.add("Kanha");
		hs.add("Rahual");

		System.out.println(hs.add("Ashish"));		//true
		
		hs.add("Badhe");
		hs.add("Rahual");
		
		System.out.println(hs.add("Kanha"));		//false

		System.out.println(hs);				//[Rahual, Ashish, Badhe, Kanha]

	}
}

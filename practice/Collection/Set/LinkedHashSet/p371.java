import java.util.*;
class LinkedHashSetDemo{
	public static void main(String s[]){
		LinkedHashSet hs=new LinkedHashSet();

		hs.add("Kanha");
		hs.add("Rahual");

		System.out.println(hs.add("Ashish"));		//true
		
		hs.add("Badhe");
		hs.add("Rahual");
		
		System.out.println(hs.add("Kanha"));		//false

		System.out.println(hs);				//[Kanha, Rahual, Ashish, Badhe]


	}
}

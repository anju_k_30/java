import java.util.*;
class TreeSetDemo{
	public static void main(String s[]){
		TreeSet ts=new TreeSet();
		ts.add("Kanha");
		ts.add("Ashish");
		ts.add("Rahual");
		ts.add("Badhe");

		ts.add(new String("Shashi"));
		ts.add(new String("Kanha"));
		ts.add(new String("Ashish"));
		ts.add(new String("Rahual"));
		ts.add(new String("Badhe"));

		System.out.println(ts);
	}
}

//internally goes same
//duplicate data doesn't get print

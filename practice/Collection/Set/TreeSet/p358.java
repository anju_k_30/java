import java.util.*;

class Myclass implements Comparable{
	
	String str=null;
	
	Myclass(String str){
		this.str=str;
	}

	public int compareTo(Object obj){
		Myclass obj1=(Myclass)obj;
		return this.str.compareTo(obj1.str);		//Business Logic remaining
	}
	
	public String toString(){
		return str;
	}
}

class TreeSetDemo{
	public static void main(String s[]){
		TreeSet ts=new TreeSet();
		
		ts.add(new Myclass("Kanha"));
		ts.add(new Myclass("Ashish"));
		ts.add(new Myclass("Rahual"));
		ts.add(new Myclass("Badhe"));

		System.out.println(ts);		//[Ashish, Badhe, Kanha, Rahual]

	}
}


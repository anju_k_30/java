import java.util.*;
class Project implements Comparable{
	String projName=null;
	int teamSize=0;
	int duration=0;

	Project(String projName,int teamSize,int Duration){
		this.projName=projName;
		this.teamSize=teamSize;
		this.duration=duration;
	}

	public String toString(){
		return projName;
	}

	public int compareTo(Object obj){
		return (this.projName).compareTo(((Project)obj).projName);
	}
}

class PQueue{
	public static void main(String s[]){
		PriorityQueue pq=new PriorityQueue();
		pq.offer(new Project("abc",15,15));
		pq.offer(new Project("pqr",20,20));
		pq.offer(new Project("tuv",10,10));
		pq.offer(new Project("xyz",30,30));

		System.out.println(pq);			//[abc, pqr, tuv, xyz]

	}
}


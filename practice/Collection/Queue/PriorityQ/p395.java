import java.util.*;
class Project{
	String projName=null;
	int teamSize=0;
	int duration=0;

	Project(String projName,int teamSize,int Duration){
		this.projName=projName;
		this.teamSize=teamSize;
		this.duration=duration;
	}

	public String toString(){
		return "{"+projName+"="+teamSize+"="+duration+"}";
	}
}

class SortByDuration implements Comparator{
	public int compare(Object obj1,Object obj2){
		return(((Project)obj1).duration)-(((Project)obj2).duration);
	}
}
class PQueue{
	public static void main(String s[]){
		PriorityQueue pq=new PriorityQueue(new SortByDuration());
		pq.offer(new Project("abc",15,15));
		pq.offer(new Project("pqr",20,20));
		pq.offer(new Project("tuv",10,10));
		pq.offer(new Project("xyz",30,30));

		System.out.println(pq);			//[{abc=15=0}, {pqr=20=0}, {tuv=10=0}, {xyz=30=0}]


	}
}


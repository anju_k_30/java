class Mythread implements Runnable{
	public void run(){
		System.out.println("In Run");
		System.out.println(Thread.currentThread().getName());
	}
}

class Demo{
	public static void main(String s[]){
		Mythread obj=new Mythread();					//new/born state of new thread
		System.out.println("Thread name= "+Thread.currentThread().getName());		//main
		Thread obj1=new Thread(obj);
		obj1.start();
		
		System.out.println(obj1.getPriority());
		Thread.currentThread().setName("C2W");
		
		Mythread obj2=new Mythread();
		Thread obj3=new Thread(obj2);
		obj3.start();
		obj3.setPriority(7);
	}
}
/*
Thread name= main
5
In Run
Thread-0
In Run
Thread-1
*/
		

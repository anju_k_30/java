import java.util.*;
class DequeDemo{
	public static void main(String s[]){
		Deque obj=new ArrayDeque();
		obj.offer(10);
		obj.offer(40);
		obj.offer(20);
		obj.offer(30);

		System.out.println(obj);

		//1.offerFirst();	//offerLast();
		obj.offerFirst(5);
		obj.offerLast(50);

		System.out.println(obj);

		//3.pollFirst();	//4.pollLast();
		obj.pollFirst();
		obj.pollLast();

		System.out.println(obj);

		//5.peekFirst();	//6.peekLast();
		System.out.println(obj.peekFirst());
		System.out.println(obj.peekLast());
		
		System.out.println(obj);

		//iterator
		Iterator itr=obj.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}

		//descendingIterator
		Iterator itr2=obj.descendingIterator();
		while(itr2.hasNext()){
			System.out.println(itr2.next());
		}
	}
}
/*
[10, 40, 20, 30]
[5, 10, 40, 20, 30, 50]
[10, 40, 20, 30]
10
30
[10, 40, 20, 30]
10
40
20
30
30
20
40
10
*/
		
		

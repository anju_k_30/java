class Mythread extends Thread{
	public void run(){
		System.out.println("Thread name= "+Thread.currentThread().getName());	//Thread 0
	}
}
class Demo{
	public static void main(String s[]){
		Mythread obj=new Mythread();					//new/born state of new thread
		System.out.println("Thread name= "+Thread.currentThread().getName());		//main
		obj.start();
		
		System.out.println(obj.getPriority());
		Thread.currentThread().setName("C2W");
		
		Mythread obj2=new Mythread();					
		obj2.start();
		obj2.setPriority(7);
	}
}
/*
 Thread name= main
5
Thread name= Thread-0
Thread name= Thread-1
/*
		

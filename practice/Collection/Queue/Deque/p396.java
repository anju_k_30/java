import java.util.*;
class DequeDemo{
	public static void main(String s[]){
		//Queue que=new LinkedList();
		Deque que=new LinkedList();

		//1.offer(E);	//2.add(E);
		que.offer(10);
		que.offer(20);
		que.add(50);
		que.offer(30);
		que.offer(40);

		System.out.println(que);

		//3.poll();
		que.poll();

		//4.remove();
		que.remove();		//throws Runtime Exception(NoSuchElement) when there is empty Queue

		System.out.println(que);

		//5.peek();
		System.out.println(que.peek());

		//6.element();
		System.out.println(que.element());	//throws Runtime Exception(NoSuchElement) when there is empty Queue

		System.out.println(que);

	}
}
/*
[10, 20, 50, 30, 40]
[50, 30, 40]
50
50
[50, 30, 40]
*/

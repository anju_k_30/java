//ArrayBlockingQueue


import java.util.*;
import java.util.concurrent.*;
class BlockingQueueDemo{
	public static void main(String s[])throws InterruptedException{		//put method throws exception

		BlockingQueue bq=new ArrayBlockingQueue(5);
		bq.offer(10);
		bq.put(20);
		bq.offer(30);
		bq.offer(40);
		bq.offer(50);

		System.out.println(bq);				//[10,20,30,40,50]

		bq.offer(60,2,TimeUnit.SECONDS);		//waits till 2 sec to print
	
		System.out.println(bq);				//[10,20,30,40,50]

		System.out.println(bq.take());			//10

		System.out.println(bq);				//[20,30,30,40,50]
		
		//util package must be included
		ArrayList al=new ArrayList();
		System.out.println("ArrayList : "+al);

		bq.drainTo(al);					//used to transfer data into another collection
		
		bq.drainTo(al,2);				//[20,30]
		System.out.println("ArrayList : "+al);		//[20,30,40,50]

		System.out.println(bq);				//[]
		
	
	}
}

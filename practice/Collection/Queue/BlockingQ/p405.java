//PriorityBlockingQueue


import java.util.concurrent.*;
class BlockingQueueDemo{
	public static void main(String s[])throws InterruptedException{		//put method throws exception

		BlockingQueue bq=new PriorityBlockingQueue();
		//BlockingQueue bq=new PriorityBlockingQueue(3);
		//BlockingQueue bq=new PriorityBlockingQueue(int,comparator);

		bq.offer("Kanha");
		bq.offer("Ashish");
		bq.offer("Rahual");

		System.out.println(bq);

		bq.put("Shashi");			//Queue gets block as size is full
		
		System.out.println(bq);		//nothing prints
	
	}
}

//BlockingQueue bq=new PriorityBlockingQueue(3,new SortByName());		//int,comparator


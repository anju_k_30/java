//LinkedBlockingQueue


import java.util.concurrent.*;
class BlockingQueueDemo{
	public static void main(String s[])throws InterruptedException{		//put method throws exception

		BlockingQueue bq=new LinkedBlockingQueue();
		//BlockingQueue bq=new LinkedBlockingQueue(3);
		
		bq.offer(10);
		bq.offer(20);
		bq.offer(30);

		System.out.println(bq);

		bq.put(40);			//Queue gets block as size is full
		
		System.out.println(bq);		//nothing prints
	
	}
}

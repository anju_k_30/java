import java.util.*;

class Nailpaint{
	String color=null;

	Nailpaint(String color){
		this.color=color;
	}

	public String toString(){
		return color;
	}
}

class SortByName implements Comparator{
	public int compare(Object obj1,Object obj2){
		return((Nailpaint)obj1).color.compareTo(((Nailpaint)obj2).color);
	}
}

class UserListSort{
	public static void main(String s[]){
		ArrayList al=new ArrayList();
		al.add(new Nailpaint("red"));
		al.add(new Nailpaint("pink"));
		al.add(new Nailpaint("orange"));
		al.add(new Nailpaint("black"));
		al.add(new Nailpaint("white"));
		
		System.out.println(al);

		Collections.sort(al,new SortByName());
		System.out.println(al);
	}
}

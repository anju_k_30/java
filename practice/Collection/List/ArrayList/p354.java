import java.util.*;
class ArrayListDemo extends ArrayList{
	public static void main(String s[]){
		//ArrayList al=new ArrayList();
		
		ArrayListDemo al=new ArrayListDemo();

		al.add(10);
		al.add("anju");
		al.add(20.0);

		System.out.println(al);			//[10, anju, 20.0]

		ArrayList al2=new ArrayList();
                al2.add("abc");
                al2.add("pqr");
                al2.add("xyz");

                System.out.println(al2);                //[abc, pqr, xyz]

                //12.boolean addAll(Collection);
                al.addAll(al2);				
                al.addAll(al);
                System.out.println(al);    	//[10, anju, 20.0, abc, pqr, xyz, 10, anju, 20.0, abc, pqr, xyz]
      
		//13.boolean addAll(int,Collection);
		al.addAll(3,al2);
		System.out.println(al);		//[10, anju, 20.0, abc, pqr, xyz, abc, pqr, xyz, 10, anju, 20.0, abc, pqr, xyz]

		//14.java.lang.Object[]to Array();
		Object arr[]=al.toArray();
//		System.out.println(arr);	//[Ljava.lang.Object;@7ad041f3
		
		//for_Each_Loop
		for(Object data:arr){
			System.out.print(data+" ");	//10 anju 20.0 abc pqr xyz abc pqr xyz 10 anju 20.0 abc pqr xyz 
		}
		System.out.println();


		//15.protected void removeRange(int,int);
		al.removeRange(3,5);			//if not inherited:error: removeRange(int,int) has protected access in ArrayList

		System.out.println(al);			//abc pqr deleted
	}
}


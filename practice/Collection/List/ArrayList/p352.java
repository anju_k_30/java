import java.util.*;
class ArrayListDemo{
	public static void main(String s[]){
		List<Integer> obj=new ArrayList<Integer>();
		obj.add(10);
		obj.add(20);
		//obj.add("Shashi");
		//obj.add(obj);

		System.out.println(obj);
	}
}

/*
 * error: incompatible types: String cannot be converted to Integer
		obj.add("Shashi");
		        ^
    error: incompatible types: List<Integer> cannot be converted to Integer
		obj.add(obj);
		^
*/


import java.util.*;
class ArrayListDemo{
	public static void main(String args[]){

		ArrayList al=new ArrayList();

		//1.add(E);
		al.add(10);
		al.add(20.5f);
		al.add("Anju");
		al.add(10);
		al.add(20.2);

		System.out.println(al);			//[10, 20.5, Anju, 10, 20.2]

		//2.int size();
		System.out.println(al.size());		//5

		//3.boolean contains(java.lang.Object);
		System.out.println(al.contains("Anju"));	//true
		System.out.println(al.contains(30));		//false

		//4.int indexOf(java.lang.Object);
		System.out.println(al.indexOf(10));		//0
		System.out.println(al.indexOf(20.5f));		//1
		System.out.println(al.indexOf("Anju"));		//2
		System.out.println(al.indexOf(10));		//0
		System.out.println(al.indexOf(20.5));		//-1

		//5.int lastIndexOf(java.lang.Object);
		System.out.println(al.lastIndexOf(10));		//3		
		System.out.println(al.lastIndexOf(20.5f));	//1
		System.out.println(al.lastIndexOf("Anju"));	//2
		System.out.println(al.lastIndexOf(10));		//3
		System.out.println(al.lastIndexOf(20.5));	//-1

		//6.E get(int);
		System.out.println(al.get(0));	//10
		System.out.println(al.get(1));	//20.5
		System.out.println(al.get(2));	//Anju
		System.out.println(al.get(3));	//10
		System.out.println(al.get(4));	//20.2

		//7.E set(int,E);
		System.out.println(al);			//[10, 20.5, 10, C2W, 20.2]
		System.out.println(al.set(3,"C2W"));	//10
		System.out.println(al);			//[10, 20.5, Anju, C2W, 20.2]

		//8.void add(int,E);
		al.add(3,"Flutter");	
		System.out.println(al);			//[10, 20.5, Anju, Flutter, C2W, 20.2]
		
		//9.E remove(int);
		System.out.println(al.remove(2));	//Anju
		System.out.println(al);			//[10, 20.5, Flutter, C2W, 20.2]

		//10.boolean remove(java.lang.Object);
		System.out.println(al.remove("C2W"));	//true
		System.out.println(al);			//[10, 20.5, Flutter, 20.2]

		//11.void clear();
		al.clear();
		System.out.println(al);			//[]






	}
}

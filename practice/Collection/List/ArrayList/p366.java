//var keyword was introduced in java 10 so it gives error in 1.8 version

import java.util.*;
class ArrayListDemo{
	public static void main(String s[]){
		ArrayList al=new ArrayList();
		
		al.add(10);
		al.add("Anju");
		al.add(new Integer(40));		//warning: [removal] Integer(int) in Integer has been deprecated and marked for removal


		for(var obj:al){
			System.out.println(obj);
		}
	}
}

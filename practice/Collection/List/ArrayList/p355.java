import java.util.*;

class CricPlayer{
	int jerNo=0;
	String name=null;
	
	CricPlayer(int jerNo,String name){
		this.jerNo=jerNo;
		this.name=name;
	}

	public String toString(){
		return jerNo+": "+name;
	}
}
class ArrayListDemo{
	public static void main(String s[]){

		ArrayList al=new ArrayList();
		al.add(new CricPlayer(18,"Virat"));
		al.add(new CricPlayer(7,"Dhoni"));
		al.add(new CricPlayer(45,"Rohit"));

		System.out.println(al);		//adress:[CricPlayer@251a69d7, CricPlayer@7344699f, CricPlayer@6b95977]
						//for user defined toString method is not called so we need to write it explicitly

		//al.add(1,"Hardik");		//[18: Virat, 7: Dhoni, 45: Rohit]
						//[18: Virat, Hardik, 7: Dhoni, 45: Rohit]
		//System.out.println(al);

		al.add(1,new CricPlayer(33,"Hardik"));
		System.out.println(al);		//[18: Virat, 33: Hardik, 7: Dhoni, 45: Rohit]

	}
}

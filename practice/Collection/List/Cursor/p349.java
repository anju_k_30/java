//ArrayList$Iter.

import java.util.*;

class CursorDemo{
	public static void main(String s[]){
		
		ArrayList al=new ArrayList();

		al.add(10);
		al.add(20.5);
		al.add(20.5f);
		al.add("C2W");

		Iterator cursor=al.iterator();
		
		System.out.println(cursor.next());
		System.out.println(cursor.next());
		System.out.println(cursor.next());
		System.out.println(cursor.next());
		//System.out.println(cursor.next());		//exception
		

		while(cursor.hasNext()){
			System.out.println(cursor.next());
			cursor.remove();
		}
		System.out.println(al);			//[]
		
		ArrayList al2=new ArrayList();
		al2.add("Ashish");
		al2.add("Kanha");
		al2.add("Rahual");
		al2.add("Badhe");
		
		Iterator cursor1=al2.iterator();

		while(cursor1.hasNext()){
			if("Kanha".equals(cursor1.next()))
				cursor1.remove();
		}
		System.out.println(al2);
		
	}

}


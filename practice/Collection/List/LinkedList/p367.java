//User Defined Linked List

import java.util.*;

class OTT{
	String pName=null;
	int subscription=0;

	OTT(String pName,int subscription){
		this.pName=pName;
		this.subscription=subscription;
	}

	public String toString(){
		return pName + ":" + subscription;
	}
}
class LinkedListDemo{
	public static void main(String s[]){
		LinkedList ll=new LinkedList();

		//void add();
		ll.add(new OTT("Hotstar",599));

		//void addFirst();
		ll.addFirst(new OTT("Netflix",899));

		//void addLast();
		ll.addLast(new OTT("JioCinema",499));

		System.out.println(ll);

		//void add(int,object);
		ll.add(2,new OTT("Planet Marathi",500));

		System.out.println(ll);

		//getFirst();
		System.out.println(ll.getFirst());

		//getLast();
		System.out.println(ll.getLast());

		//removeFirst();
		System.out.println(ll.removeFirst());
		System.out.println(ll);

		//removeLast();
		System.out.println(ll.removeLast());
		System.out.println(ll);

	}
}

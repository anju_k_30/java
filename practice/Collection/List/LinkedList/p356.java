import java.util.*;
class LinkedListDemo{
	public static void main(String s[]){
		LinkedList ll=new LinkedList();
		ll.add(10);
		ll.add(20);
		ll.addFirst(5);
		ll.addLast(25);

		System.out.println(ll);		//[5,10,20,25]
		
		//1.void add(int,E);
		ll.add(2,15);			//[5,10,15,20,25]
		System.out.println(ll);
		
		//2.E getFirst(); 
		//3.E getLast();
		System.out.println(ll.getFirst());	//5
		System.out.println(ll.getLast());	//25
		System.out.println(ll);

		//4.E removeFirst(); 
		//5.E removeLast();
		System.out.println(ll.removeFirst());	//5
		System.out.println(ll.removeLast());	//25
		System.out.println(ll);			//[10,15,20]
		
		//6.boolean contains(E);
		System.out.println(ll.contains(20));	//true
		
		//7.int size();
		System.out.println(ll.size());		//3
		
		//8.addAll(int,obj);
		LinkedList ll2=new LinkedList();
		ll2.add("Anju");
		ll2.add("Pradu");
		ll2.add("Sakshi");
		
		ll.addAll(ll2);	
		System.out.println(ll);		//[10,15,20,Anju,Pradu,Sakshi]
		
		ll.addAll(2,ll2);
		System.out.println(ll);		//[10,15,Anju,Pradu,Sakshi,20,Anju,Pradu,Sakshi]
		
		//9.remove(int);
		System.out.println(ll.remove(2));	//Anju
		System.out.println(ll.remove(ll2));	//false
		
		System.out.println(ll.get(3));		//Sakshi
		System.out.println(ll);			//[10, 15, Pradu, Sakshi, 20, Anju, Pradu, Sakshi]

		System.out.println(ll.set(3,"Mane"));	//Sakshi
		System.out.println(ll);			//[10, 15, Pradu, Mane, 20, Anju, Pradu, Sakshi]
		
		//10.public E peekFirst();
		System.out.println(ll.peekFirst());	//10
		
		//11.public E peekLast();
		System.out.println(ll.peekLast());	//Sakshi

		//12.public E pollFirst();
		//13.public E pollLast();
		System.out.println(ll.pollFirst());	//10
		System.out.println(ll);			//[15, Pradu, Mane, 20, Anju, Pradu, Sakshi]
		
		System.out.println(ll.pollLast());	//Sakshi
		System.out.println(ll);			//[15,Pradu,Mane,20,Anju,Pradu]
		
		//14.public void push(E);
		ll.push("Binecaps");
		System.out.println(ll);			//[Binecaps, 15, Pradu, Mane, 20, Anju, Pradu]
						
		ll.push("Company");
		System.out.println(ll);			//[Company, Binecaps, 15, Pradu, Mane, 20, Anju, Pradu]
		
		//15.toArray();
		Object arr[]=ll.toArray();
		for(Object data:arr){
			System.out.print(data +" ");
		}
		System.out.println();			//Company Binecaps 15 Pradu Mane 20 Anju Pradu 

		//16.clear
		ll.clear();
		System.out.println(ll);			//[]

	}
}

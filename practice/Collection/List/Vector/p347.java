import java.util.*;
class StackDemo{
	public static void main(String str[]){
		
		Stack s=new Stack();
		
		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);
		
		System.out.println(s);

		System.out.println(s.pop());
		
		System.out.println(s);

		System.out.println(s.peek());
		System.out.println(s);

		System.out.println(s.search(10));	//3:top most element is considered 1(40)

		System.out.println(s.empty());		//false


	}
}


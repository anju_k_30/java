//Multithreading in vector

import java.util.*;

class Mythread extends Thread{

	Vector v=null;

	Mythread(Vector v){
		this.v=v;
	}

	public void run(){
		System.out.println(v.hashCode());		//40061

		v.addElement(11);
		v.addElement(22);
		v.addElement(33);

		System.out.println(v);
	}
}

class VectorDemo{

	public static void main(String s[]){
		Vector v=new Vector();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		System.out.println(v);

		Mythread obj=new Mythread(v);
		obj.start();

		System.out.println(v.removeElement(30));	//true

		System.out.println(v.capacity());		//10
	}
}

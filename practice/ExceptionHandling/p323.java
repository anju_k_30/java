import java.util.Scanner;

class InvalidAgeException extends RuntimeException{
	InvalidAgeException(String str){
		super(str);
	}
}
class Demo{
	public static void main(String s[]){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter Age for voting");
		System.out.println("Note: Age above 18+");

		int data=sc.nextInt();

		if(data<18)
			throw new InvalidAgeException("Age must be above 18");

		System.out.println("Valid age");
	}
}

/*
Enter Age for voting
Note: Age above 18+
60
Valid age


Enter Age for voting
Note: Age above 18+
12
Exception in thread "main" InvalidAgeException: Age must be above 18
*/

import java.io.*;
class ExceptionDemo{
	public static void main(String s[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		String str=br.readLine();
		System.out.println(str);

		int data=0;
		try{
			data=Integer.parseInt(br.readLine());
		}
		catch(NumberFormatException obj){
			System.out.println("Please Enter Integer data");
			data=Integer.parseInt(br.readLine());
		}

		System.out.println(data);
	}
}
/*
shashi  
shashi
Bagal
Please Enter Integer data
35
35
*/

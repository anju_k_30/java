import java.util.Scanner;

class DataOverFlowException extends RuntimeException{
	DataOverFlowException(String s){
		super(s);
	}
}

class DataUnderFlowException extends RuntimeException{
	DataUnderFlowException(String s){
		super(s);
	}
}

class Demo{
	public static void main(String s[]){
		int arr[]=new int[5];

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter Integer Value");
		System.out.println("Note: 0<element<100");

		for(int i=0;i<arr.length;i++){
			int data=sc.nextInt();

			if(data<0)
				throw new DataUnderFlowException("Mitra Data 0 peksha lahan ahe");

			if(data>100)
				throw new DataOverFlowException("Mitra Data 100 peksha jastt ahe");

			arr[i]=data;
		}

		for(int i=0;i<arr.length;i++)
			System.out.println(arr[i]+" ");
	}
}
/*
Enter Integer Value
Note: 0<element<100
150
Exception in thread "main" DataOverFlowException

Enter Integer Value
Note: 0<element<100
-5
Exception in thread "main" DataUnderFlowException
	at Demo.main(p322.java:28)


#when super is written

Enter Integer Value
Note: 0<element<100
150
Exception in thread "main" DataOverFlowException: Mitra Data 100 peksha jastt ahe

*/


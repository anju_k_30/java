import java.io.*;

class Demo{
	void getdata() throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int data=0;

		try{
			data=Integer.parseInt(br.readLine());
		}
		catch(NumberFormatException obj){
			//System.out.println(obj.printStackTrace());		error: 'void' type not allowed here
		//	obj.printStackTrace();
		//	System.out.println(obj.toString());
			System.out.println(obj.getMessage());
			
		}

		System.out.println(data);
	}

	public static void main(String s[]) throws IOException {
		Demo obj=new Demo();
		obj.getdata();
	}
}
/*
//toString();
anju
java.lang.NumberFormatException: For input string: "anju"
0

//getMessage();
anju
For input string: "anju"
0

//obj.printStackTrace();
anju
java.lang.NumberFormatException: For input string: "anju"
	at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:67)
	at java.base/java.lang.Integer.parseInt(Integer.java:668)
	at java.base/java.lang.Integer.parseInt(Integer.java:784)
	at Demo.getdata(p305.java:10)
	at Demo.main(p305.java:21)
0
*/

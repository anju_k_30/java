class Demo{
	static void fun(int x){
		System.out.println(x);
		fun(++x);
	}
	public static void main(String s[]){
		try{
			fun(1);
		}
		catch(StackOverflowError obj){
			System.out.print("Exception in thread "+ Thread.currentThread().getName() +" ");
			obj.printStackTrace();
		}
	}
}

/*
Exception in thread "main" java.lang.StackOverflowError
*/

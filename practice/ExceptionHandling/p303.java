class Demo{
	void m2(){
		System.out.println("Start m2");
		System.out.println(10/0);
		System.out.println("End m2");
	}

	void m1(){
		System.out.println("Start m1");
		m2();
		System.out.println("End m1");
	}

	public static void main(String s[]){
	
		System.out.println("Start main");
		Demo obj=new Demo();
		obj.m1();
		System.out.println("End main");
	}
}
/*
Start main
Start m1
Start m2
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at Demo.m2(p303.java:4)
	at Demo.m1(p303.java:10)
	at Demo.main(p303.java:18)
*/

		

import java.io.*;
class Demo{
	public static void main(String s[]){
		System.out.println("Start Main");
		try{
			System.out.println(10/0);
		}
//		catch(ArithmeticException | IOException  obj){		error: exception IOException is never thrown in body of corresponding try statement
		catch(ArithmeticException | NullPointerException obj){
			System.out.println("Exception Occured");
		}

		System.out.println("End Main");
	}
}
		

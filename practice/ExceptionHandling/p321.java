import java.util.Scanner;
class Demo{
	public static void main(String s[]){
		Scanner sc=new Scanner(System.in);
		int x=sc.nextInt();
		try{
			if(x==0){
				throw new ArithmeticException("Divide by Zero");
			}
			System.out.println(10/x);
		}catch(ArithmeticException obj){		
			System.out.println(obj);		//internally calls toString()
		//	System.out.println(obj.getMessage());
		}

	}
}	
/*
 0
java.lang.ArithmeticException: Divide by Zero
*/

class Demo{
	public static void main(String s[]){
		System.out.println("Start main");
		
		try{
			System.out.println(10/0);
		}
		catch(ArithmeticException obj){
			System.out.println("Exception Occured");
		}
		System.out.println("End main");
	}
}

/*
 gets memory on method area

Exception table:
       from    to  target type
           8    18    21   Class java/lang/ArithmeticException
*/	

class Demo{
	void m1(){
		System.out.println("In M1");
	}
	void m2(){
		System.out.println("In M2");
	}
	public static void main(String s[]){
		
		System.out.println("Start main");

		Demo obj=new Demo();
		obj.m1();

		obj=null;
		
		obj.m2();
		
		System.out.println("End main");
	}
}
	
/*
Start main
In M1
Exception in thread "main" java.lang.NullPointerException: Cannot invoke "Demo.m2()" because "<local1>" is null
	at Demo.main(p304.java:17)
*/


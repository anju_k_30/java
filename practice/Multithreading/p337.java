class Mythread extends Thread{

	Mythread(String s){
		super(s);
	}

	Mythread(){
	
	}

	
	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}

class ThreadGroupDemo{
	public static void main(String s[]){
		Mythread obj=new Mythread("xyz");
		obj.start();
		
		Mythread obj2=new Mythread("xyz");
		obj2.start();

		Mythread obj3=new Mythread();		// if constructor not written -->
							// error: constructor Mythread in class Mythread cannot be applied to given types;
		obj3.start();
	}
}



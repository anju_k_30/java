class Mythread extends Thread{
	Mythread(String str){
		super(str);
	}

	public void run(){
		//System.out.println(getName());
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadGroupDemo{
	public static void main(String s[]){
		Mythread obj=new Mythread("xyz");
		obj.start();
		
		Mythread obj1=new Mythread("XYZ");
		obj1.start();

		//Mythread obj2=new Mythread();		//error:error: constructor Mythread in class Mythread cannot be applied to given types;

		//obj.start();
	}
}



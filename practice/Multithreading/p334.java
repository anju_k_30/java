class Mythread extends Thread{
	public void run(){
		System.out.println(getName());			//Thread-0
		System.out.println(Thread.currentThread().getThreadGroup());	//java.lang.ThreadGroup[name=main,maxpri=10]
	}
}

class ThreadGroupDemo{
	public static void main(String s[]){
		Mythread obj=new Mythread();
		obj.start();

	//	System.out.println(getName());			//error:cannot find symbol
		System.out.println(obj.getName());		//Thread-0

		obj.setName("xyz");

	}
}
		
		

class Mythread extends Thread{
	//public void run()throws InterruptedException{			//error: run() in Mythread cannot implement run() in Runnable
									//  overridden method does not throw InterruptedException
	public void run(){
		System.out.println("Thread name= "+Thread.currentThread().getName());	//Thread 0
		for(int i=0;i<10;i++){
			System.out.println("In Run");
			try{
				Thread.sleep(1000);
			}
			catch(InterruptedException obj){
			}
		}
	}
}
class Demo{
	public static void main(String s[])throws InterruptedException{
		Mythread obj=new Mythread();					//new/born state of new thread
		System.out.println("Thread name= "+Thread.currentThread().getName());		//main
		obj.start();
		
		for(int i=0;i<10;i++){
			System.out.println("In Main");
			Thread.sleep(1000);
		}
	}
}

//We can't throw InterruptedException using throws on run method(overridden) as its parent class(run method) does not throw Exception

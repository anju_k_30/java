class Mythread extends Thread{
	Mythread(ThreadGroup tg,String str){
		super(tg,str);
	}

	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo{
	public static void main(String s[]){
		
		ThreadGroup pthreadGP=new ThreadGroup("C2W");
		
		Mythread obj1=new Mythread(pthreadGP,"C");
		Mythread obj2=new Mythread(pthreadGP,"Java");
		Mythread obj3=new Mythread(pthreadGP,"Python");

		obj1.start();
		obj2.start();
		obj3.start();
	}
}


import java.util.concurrent.*;

class Mythread implements Runnable{
	int num;

	Mythread(int num){
		this.num=num;
	}

	public void run(){
		System.out.println(Thread.currentThread() + " Start Thread : "+ num);
		dailytask();
		System.out.println(Thread.currentThread() + " End Thread : "+ num);
	}

	void dailytask(){
		try{
			Thread.sleep(1000);
		}catch(InterruptedException obj){
		}
	}
}

class ThreadPoolDemo{
	public static void main(String args[]){
		ExecutorService ser=Executors.newSingleThreadExecutor();

		for(int i=0;i<=6;i++){
			Mythread obj1=new Mythread(i);
			ser.execute(obj1);
		}
		ser.shutdown();
	}
}
		

//Concurrencu Method in Thread Class
//1.Sleep(int,int)....(milisec,nanosec)

class Mythread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{
	public static void main(String s[])throws InterruptedException{
		System.out.println(Thread.currentThread());

		Mythread obj=new Mythread();
		obj.start();

		Thread.sleep(1000,500);

		Thread.currentThread().setName("C2W");
		System.out.println(Thread.currentThread());
	}
}

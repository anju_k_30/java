class Mythread extends Thread{

	public void run(){
		System.out.println(Thread.currentThread().getName());		//Thread-0
	}

}

class ThreadYeildDemo{

	public static void main(String s[]){

		Mythread obj=new Mythread();
		obj.start();

		obj.yield();	

		System.out.println(Thread.currentThread());			//Thread[main,5,main]
	}
}

/*

Thread[main,5,main]
Thread-0

*/

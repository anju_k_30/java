class Mythread extends Thread{
	public void run(){
		System.out.println("In Run");
		System.out.println(Thread.currentThread().getName());
	}

/*	public void start(){
		System.out.println("In mythread start");		//start() method should not be overridden [no error] as thread class start() method should be called to execute 										thread and not normal start() method
		run();
	}*/
}
class ThreadDemo{
	public static void main(String args[]){
		Mythread obj=new Mythread();
		obj.start();
		System.out.println(Thread.currentThread().getName());
	}
}


//2.join()


class Mythread extends Thread{
	public void run(){
		for(int i=0;i<10;i++)
			System.out.println("IN THREAD-0");
	}
}

class ThreadDemo{
	public static void main(String s[])throws InterruptedException{
		Mythread obj=new Mythread();
		obj.start();

		obj.join();
		for(int i=0;i<10;i++)
			System.out.println("IN MAIN");
	}
}
/*
 IN THREAD-0
IN THREAD-0
IN THREAD-0
IN THREAD-0
IN THREAD-0
IN THREAD-0
IN THREAD-0
IN THREAD-0
IN THREAD-0
IN THREAD-0
IN MAIN
IN MAIN
IN MAIN
IN MAIN
IN MAIN
IN MAIN
IN MAIN
IN MAIN
IN MAIN
IN MAIN
/*

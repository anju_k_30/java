//1.Creating thread using thread class(using extend keyword)

class Mythread extends Thread{

	public void run(){			//Can't reduce scope hence public is written
		for(int i=0;i<5;i++)
			System.out.println("In Run");
	}
}

class ThreadDemo{
	public static void main(String s[]){

		Mythread obj=new Mythread();
		obj.start();

		for(int i=0;i<5;i++)
			System.out.println("In Main");
	}
}


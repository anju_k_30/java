//should not be done in order to get deadlock


class Mythread extends Thread{

	static Thread nmMain=null;

	public void run(){
		try{
			nmMain.join();
		}
		catch(InterruptedException obj){
		}

		for(int i=0;i<10;i++)
			System.out.println("IN THREAD-0");
	}
}

class ThreadDemo{
	public static void main(String s[])throws InterruptedException{

		Mythread.nmMain=Thread.currentThread();

		Mythread obj=new Mythread();
		obj.start();

		//obj.join();
		obj.join(200);

		for(int i=0;i<10;i++)
			System.out.println("IN MAIN");
	}
}
//op-nothing gets printed
